# Kompressor Firmware

This project contains both secure bootloader and application code for Kompressor.

## Building

### Build dependencies

 * These packages need to be installed

```
apt install gcc-arm-none-eabi gdb-multiarch make libnewlib-arm-none-eabi kconfig-frontends
```

 * Rust cross compiler target:

```
rustup target add thumbv7m-none-eabi
rustup +nightly target add thumbv7m-none-eabi
```

### Build

```
make all
```

## Bootloaer

The bootloader has an embedded EC25519 keypair it uses to verify the app image everytime at startup. If the app image is valid, it will begin executing, if not, the bootloader will enter Download mode. New app image can be downloaded using XModem transfer.

The bootloader can be forced into download mode by asserting the Bootload pin on startup. On a raspberry pi, the Bootload pin is mapped to GPIO25.

The bootloader source code is found in `src/bootloader` and can be build with `make build/kompressor_cm3/bootloader/bootloader.bin`.

The build process embeds a ED25519 public key file to use for app signing. A new key can be generated using `./src/utils/target/debug/utils gen-key`. The keyfile use `keys/dev_key.pub` by default. This can be overwritten by defining the env var `APP_SIGN_KEY` when building the bootloader.



### Bootloader XModem Download

Install xmodem package and picocom on the raspberry pi (`apt install picocom lrzsz`)

Launch the serial terminal:
```
picocom --send-cmd "sx -vv" -b 115200 /dev/serial0
```

When prompted transfer `app_image_signed.bin`. (Ctrl-a Ctrl-s)

## App Signing

The app is signed using the Rust `utils` app (see `/src/utils`).

The following example signs the app image (app.bin) using the EC25519 `APP_SIGN_KEY`. Note: for development, you can use `keys/dev_key.priv`. The resulting signed image (`app_image_signed.bin`) can then be Downloaded to Kompressor using XModem transfer. 
```
./target/debug/utils sign-app -i build/kompressor_cm3/app/app.bin --key keys/dev_key.priv
```

This command will generate a signed app image: `app_image_signed.bin`

## Unique Secure Key

The unique pub/private keypair used for packet signing is stored in flash memory at `META_START` location

## App Image

```

0x08000000  +------------------+
            |  BOOTLOADER      |
            |                  |
0x08004000  +------------------+
            | META_START       |   <== Unique Crypto keys stored here
            |                  |
0x08004100  +------------------+   <== Bootloader Start writing here
            | APP_IMAGE_HEADER |

0x08004200  +----------------- +
            | APP_IMAGE_START  |
            |                  |
            |       ...        |
```

## Debugging

 * Install the [Cortex-Debug](https://marketplace.visualstudio.com/items?itemName=marus25.cortex-debug) VS Code extension.

 * Install [J-Link](https://www.segger.com/downloads/jlink/)




Notes:

https://wiki.st.com/stm32mpu/wiki/STM32MP15_ROM_code_secure_boot