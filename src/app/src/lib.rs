// Copyright (C) NLighten Systems, LLC (Paul Soucy) 2022
//
// This file is part of Kompressor Firmware.
//
// Kompressor Firmware is free software: you can redistribute it and/or modify it under the terms of
// the GNU General Public License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any later version.
//
// Kompressor Firmware is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with Foobar. If not, see
// <https://www.gnu.org/licenses/>.

#![no_std]
#![feature(alloc_error_handler)]
#![feature(allocator_api)]
#![feature(type_alias_impl_trait)]
#![feature(generic_associated_types)]
#![feature(core_intrinsics)]
#![allow(non_upper_case_globals)]
#![allow(non_camel_case_types)]
#![allow(non_snake_case)]

use async_runtime::SingleThreadAsyncExecutor;
use cortex_m::asm;

use kompressor_common::concentrator::{
    stream::SPIFrame, Frame, SX1250Data, SX1250Write, SX130xMemData, SX130xRegRead, SX130xRegValue,
    SX130xRegWrite, CHUNK_SIZE_MAX,
};

use log::{debug, error, info, LevelFilter};
use rscore::meta::Meta;

extern crate alloc;

use core::{mem::MaybeUninit, panic::PanicInfo};

mod allocator;
mod async_runtime;
mod concentrator;
mod data;
mod gps;
mod hal;
mod logger;
pub mod sync;

use hal::spi;

use data::StaticData;

use crate::{concentrator::SignPktReq, hal::resetSX130x};

include!(concat!(env!("OUT_DIR"), "/bindings.rs"));
include!(concat!(env!("OUT_DIR"), "/git_version.rs"));

#[panic_handler]
fn panic(panic_info: &PanicInfo<'_>) -> ! {
    log::error!("{}", panic_info);
    asm::bkpt();
    loop {}
}

#[global_allocator]
static GLOBAL: allocator::FreeRTOSAllocator = allocator::FreeRTOSAllocator;

#[alloc_error_handler]
fn on_oom(_layout: core::alloc::Layout) -> ! {
    asm::bkpt();
    loop {}
}

static LOGGER: logger::UARTLogger = logger::UARTLogger;

pub static mut DATA: MaybeUninit<StaticData> = MaybeUninit::uninit();

#[no_mangle]
pub extern "C" fn rust_init() {
    log::set_logger(&LOGGER)
        .map(|()| log::set_max_level(LevelFilter::Trace))
        .unwrap();

    info!("NLighten Secure Concentrator {}", GIT_VERSION);
    info!("Serial: {}", Meta::get().serial());

    unsafe {
        resetSX130x();
        DATA.write(StaticData::new());
    }

    freertos_rust::Task::new()
        .name("main")
        .stack_size(1100)
        .priority(freertos_rust::TaskPriority(10))
        .start(main_fn)
        .unwrap();

    freertos_rust::Task::new()
        .name("sign")
        .stack_size(800)
        .priority(freertos_rust::TaskPriority(10))
        .start(sign_fn)
        .unwrap();

    #[cfg(feature = "hwmon")]
    {
        freertos_rust::Task::new()
            .name("hwmom")
            .stack_size(422)
            .priority(freertos_rust::TaskPriority(10))
            .start(|t| loop {
                freertos_rust::CurrentTask::delay(freertos_rust::Duration::ms(5000));
                let t = freertos_rust::FreeRtosUtils::get_all_tasks(None);
                info!("{}", t);
            })
            .unwrap();
    }
}

macro_rules! branch_size {
    ($e:expr) => {{
        debug!("branch size {}:{} {} => {}",
            file!(), line!(), stringify!($e), core::mem::size_of_val(&$e) );
        $e
    }};
}

fn main_fn(_task: freertos_rust::Task) {
    let exe = SingleThreadAsyncExecutor::new();

    unsafe {
        exe.spawn(branch_size!(spi::run()));
        exe.spawn(branch_size!(concentrator::run()));
        exe.spawn(branch_size!(gps::run()));
    }

    exe.run();
}

fn sign_fn(_task: freertos_rust::Task) {
    use crate::concentrator::SignPktRes;
    use alloc::boxed::Box;
    use kompressor_common::utils::Tracer;

    let static_data = unsafe { DATA.assume_init_mut() };

    loop {
        if let Ok(req) = static_data
            .sign_queue
            .receive(freertos_rust::Duration::infinite())
        {
            let tracer = Tracer::new("sign");

            let req = unsafe { Box::from_raw(req) };

            SingleThreadAsyncExecutor::block(async {
                let req = SignPktReq::new(&req.pkt_rep());
                let sig = Meta::get().keypair().private.sign(req.hash);
                let sign_res = SignPktRes {
                    id: req.id,
                    sig: sig,
                };
                static_data
                    .sign_pkt
                    .send(sign_res, freertos_rust::Duration::ms(500));
            });
        }
    }
}
