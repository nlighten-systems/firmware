// Copyright (C) NLighten Systems, LLC (Paul Soucy) 2022
//
// This file is part of Kompressor Firmware.
//
// Kompressor Firmware is free software: you can redistribute it and/or modify it under the terms of
// the GNU General Public License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any later version.
//
// Kompressor Firmware is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with Foobar. If not, see
// <https://www.gnu.org/licenses/>.

pub struct Checksum {
    ch_a: u8,
    ch_b: u8,
}

impl Checksum {
    pub fn new() -> Self {
        Checksum { ch_a: 0, ch_b: 0 }
    }

    pub fn update(&mut self, buf: &[u8]) {
        for data in buf {
            self.ch_a = self.ch_a.wrapping_add(*data);
            self.ch_b = self.ch_b.wrapping_add(self.ch_a);
        }
    }

    pub fn finalize(&self) -> [u8; 2] {
        [self.ch_a, self.ch_b]
    }

    pub fn verify(&self, buf: &[u8]) -> bool {
        self.ch_a == buf[0] && self.ch_b == buf[1]
    }
}
