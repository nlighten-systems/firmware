// Copyright (C) NLighten Systems, LLC (Paul Soucy) 2022
//
// This file is part of Kompressor Firmware.
//
// Kompressor Firmware is free software: you can redistribute it and/or modify it under the terms of
// the GNU General Public License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any later version.
//
// Kompressor Firmware is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with Foobar. If not, see
// <https://www.gnu.org/licenses/>.

use kompressor_common::pps_clock::PPSClock;
use kompressor_common::{concentrator::stream::SPIFrame, gps::GPSTime};

use kompressor_common::concentrator::*;

use rscore::lorapkt::{self, Register};

use crate::async_runtime::Mutex;
use crate::concentrator::{signpkt::LoRaPacketOwned, SignPktReq, SignPktRes};
use crate::sync::Mailbox;

pub struct StaticData {
    pub host_rx: Mailbox<SPIFrame>,
    pub host_tx: Mailbox<SPIFrame>,
    pub sx130x_write_op: Option<SX130xMemWrite>,
    pub radio_ctx: lorapkt::Context,
    pub gps_time: Option<GPSTime>,
    pub next_gps_time: Option<GPSTime>,
    pub lora_pkt_builder: lorapkt::DefaultBuilder,
    pub sx130x_pps: PPSClock,
    pub sign_queue: freertos_rust::Queue<*mut LoRaPacketOwned>,
    pub sign_pkt: freertos_rust::Queue<SignPktRes>,
    pub smcu_reg: Register<0x0000, 1>,
}

impl StaticData {
    pub fn new() -> Self {
        Self {
            host_rx: Mailbox::new(),
            host_tx: Mailbox::new(),
            sx130x_write_op: None,
            radio_ctx: lorapkt::Context::new(),
            gps_time: None,
            next_gps_time: None,
            lora_pkt_builder: lorapkt::DefaultBuilder::new(),
            sx130x_pps: PPSClock::new(),
            sign_queue: freertos_rust::Queue::new(10).unwrap(),
            sign_pkt: freertos_rust::Queue::new(1).unwrap(),
            smcu_reg: Register::new(),
        }
    }
}
