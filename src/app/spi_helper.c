
#include "main.h"
#include "string.h"
#include "stdbool.h"
#include "stdatomic.h"


#define FRAME_SIZE 20


QueueHandle_t gSPI_HOST_TX;
QueueHandle_t gSPI_HOST_RX;

static uint8_t gSPI_Buf[FRAME_SIZE];

void reset_host_spi();

void spi_init()
{
    gSPI_HOST_TX = xQueueCreate(5, FRAME_SIZE);
    gSPI_HOST_RX = xQueueCreate(10, FRAME_SIZE);
    reset_host_spi();
}

void reset_host_spi()
{
    memset(gSPI_Buf, 0, FRAME_SIZE);
    HAL_SPI_TransmitReceive_DMA(&hspi2, gSPI_Buf, gSPI_Buf, FRAME_SIZE);
}

void HAL_SPI_AbortCpltCallback(SPI_HandleTypeDef* hspi)
{
    if (hspi == &hspi2) {
        //HAL_SPIEx_FlushRxFifo(&hspi2);
    }
}



void HAL_SPI_TxRxCpltCallback(SPI_HandleTypeDef* hspi)
{
    static uint8_t tx_buf[FRAME_SIZE];
    

	BaseType_t xHigherPriorityTaskWoken = pdFALSE;
    atomic_uintptr_t expected;

	if(hspi == &hspi2) {

        xQueueSendFromISR(gSPI_HOST_RX, gSPI_Buf, &xHigherPriorityTaskWoken);
        
        if (xQueueReceiveFromISR(gSPI_HOST_TX, gSPI_Buf, &xHigherPriorityTaskWoken) != pdTRUE) {
            memset(gSPI_Buf, 0, FRAME_SIZE);
        }
        HAL_SPI_TransmitReceive_DMA(&hspi2, gSPI_Buf, gSPI_Buf, FRAME_SIZE);
        
	}
    portYIELD_FROM_ISR( xHigherPriorityTaskWoken );
	
}

void HAL_SPI_TxCpltCallback(SPI_HandleTypeDef* hspi)
{
    if (hspi == &hspi1) {
        //SpiTransferComplete(hspi);
    }
}

void HAL_SPI_RxCpltCallback(SPI_HandleTypeDef* hspi)
{
    if (hspi == &hspi1) {
        //SpiTransferComplete(hspi);
    }
}

void HAL_SPI_ErrorCallback(SPI_HandleTypeDef* hspi)
{
    if (hspi == &hspi2) {
        //if we get a SPI error (overrun??) just try again. IDK.
        reset_host_spi();
    }
}

void spi1_nss_set()
{
    HAL_GPIO_WritePin(SPI1_NSS_GPIO_Port, SPI1_NSS_Pin, RESET);
}

void spi1_nss_clear()
{
    HAL_GPIO_WritePin(SPI1_NSS_GPIO_Port, SPI1_NSS_Pin, SET);
}

void SpiSlaveNSS()
{
    // SPI_HandleTypeDef *hspi = &hspi2;

    // unsigned int expected = STATE_DMA_READY;

    // if (atomic_compare_exchange_strong(&gSpiSlaveState, &expected, STATE_TRANSFER)) {
    //     CLEAR_BIT(hspi->Instance->CR1, SPI_CR1_SSI);
    // }

    
}
