// Copyright (C) NLighten Systems, LLC (Paul Soucy) 2022
//
// This file is part of Kompressor Firmware.
//
// Kompressor Firmware is free software: you can redistribute it and/or modify it under the terms of
// the GNU General Public License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any later version.
//
// Kompressor Firmware is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with Foobar. If not, see
// <https://www.gnu.org/licenses/>.

#![no_std]

#![feature(alloc_error_handler)]
#![feature(allocator_api)]
#![feature(core_intrinsics)]
#![allow(non_upper_case_globals)]
#![allow(non_camel_case_types)]
#![allow(non_snake_case)]

mod protocol;
mod gps;


use rscore::{
    io::{self, Write},
    //sha512::Sha512, ed25519::{self, KeyPair, Signature},
    keys::AppKeys,
    lorapkt::{self, Context, LoRaPacket, LoraPacketRep},
    ublox::{self, GnassTimebase, UbxPkt},
    ufmt::{uWrite, uwrite, Formatter},
    utils::{Time, UTCTime},
};

use freertos_rust::*;

use kompressor_common::{
    gps::GPSTime,
    pps_clock::PPSClock,
    prelude::Sha512,
    spiproto::{self},
};

use serde::{
    ser::{Serialize, SerializeMap, SerializeSeq},
    Serializer,
};

use log::{error, info, Level, LevelFilter, Metadata, Record, SetLoggerError};

use cortex_m::asm;

extern crate alloc;

include!(concat!(env!("OUT_DIR"), "/bindings.rs"));

include!(concat!(env!("OUT_DIR"), "/git_version.rs"));

use core::{
    alloc::{AllocError, GlobalAlloc, Layout},
    convert::Infallible,
    mem::MaybeUninit,
};

#[cfg(not(test))]
use core::panic::PanicInfo;

#[cfg(not(test))]
#[panic_handler]
fn panic(_panic: &PanicInfo<'_>) -> ! {
    asm::bkpt();
    loop {}
}

struct MyAllocator;

unsafe impl GlobalAlloc for MyAllocator {
    unsafe fn alloc(&self, layout: Layout) -> *mut u8 {
        use core::fmt::Write;
        let debug_log = &huart1 as *const UART_HandleType;
        let mut uart = UART { handle: debug_log };

        let size = layout.size();
        //info!("alloc {}", size);
        write!(uart, "alloc {}\r\n", size);
        let res = pvPortMalloc(size as u32);
        return res as *mut u8;
    }

    unsafe fn dealloc(&self, ptr: *mut u8, layout: Layout) {
        vPortFree(ptr as *mut cty::c_void)
    }
}

extern "C" {
    pub fn pvPortMalloc(xWantedSize: u32) -> *mut cty::c_void;
    pub fn vPortFree(pv: *mut cty::c_void);
}

#[global_allocator]
static GLOBAL: MyAllocator = MyAllocator;

#[alloc_error_handler]
fn on_oom(_layout: core::alloc::Layout) -> ! {
    asm::bkpt();
    loop {}
}

#[allow(non_camel_case_types)]
pub enum SX1250OpCode {
    CALIBRATE = 0x89,
    CALIBRATE_IMAGE = 0x98,
    CLR_IRQ_STATUS = 0x02,
    STOP_TIMER_ON_PREAMBLE = 0x9F,
    SET_RFSWITCHMODE = 0x9D,
    GET_IRQ_STATUS = 0x12,
    GET_RX_BUFFER_STATUS = 0x13,
    GET_PACKET_STATUS = 0x14,
    READ_BUFFER = 0x1E,
    READ_REGISTER = 0x1D,
    SET_DIO_IRQ_PARAMS = 0x08,
    SET_MODULATION_PARAMS = 0x8B,
    SET_PA_CONFIG = 0x95,
    SET_PACKET_PARAMS = 0x8C,
    SET_PACKET_TYPE = 0x8A,
    SET_RF_FREQUENCY = 0x86,
    SET_BUFFER_BASE_ADDRESS = 0x8F,
    SET_SLEEP = 0x84,
    SET_STANDBY = 0x80,
    SET_RX = 0x82,
    SET_TX = 0x83,
    SET_TX_PARAMS = 0x8E,
    WRITE_BUFFER = 0x0E,
    WRITE_REGISTER = 0x0D,
    SET_TXCONTINUOUSWAVE = 0xD1,
    SET_TXCONTINUOUSPREAMBLE = 0xD2,
    GET_STATUS = 0xC0,
    SET_REGULATORMODE = 0x96,
    SET_FS = 0xC1,
    GET_DEVICE_ERRORS = 0x17,
}

const OPCODE_SET_RF_FREQUENCY: u8 = 0x86;
const SX130X_FIFO: u16 = 0x4000;
const SX130X_OTP_RD_DATA_RD_DATA: u16 = 0x6181;

//static mut RADIO_CTX: core::mem::MaybeUninit<Context> = core::mem::MaybeUninit::uninit();
//static mut LORAPKT_BUILDER: core::mem::MaybeUninit<lorapkt::DefaultBuilder> =
//    core::mem::MaybeUninit::uninit();
static mut STATIC_DATA: core::mem::MaybeUninit<StaticData> = core::mem::MaybeUninit::uninit();

struct StaticData {
    radio_ctx: Context,
    lora_pkt_builder: lorapkt::DefaultBuilder,
    sx130x_pps: PPSClock,
    gps_time: Option<GPSTime>,
    next_gps_time: Option<GPSTime>,
    gps_task: freertos_rust::Task,
    encode_queue: freertos_rust::Queue<*mut SigningData>,
    encode_task: freertos_rust::Task,
    sign_queue: freertos_rust::Queue<*mut (u32, [u8; Sha512::SHA512_HASH_SIZE])>,
    sign_task: freertos_rust::Task,
    uart: freertos_rust::Mutex<*const UART_HandleType>,
}

struct UARTLogger;

static LOGGER: UARTLogger = UARTLogger;

impl log::Log for UARTLogger {
    fn enabled(&self, metadata: &Metadata) -> bool {
        metadata.level() <= Level::Trace
    }

    fn log(&self, record: &Record) {
        if self.enabled(record.metadata()) {
            use core::fmt::Write;

            let uart_lock = unsafe { STATIC_DATA.assume_init_ref() }
                .uart
                .lock(Duration::infinite())
                .unwrap();
            let mut uart = UART { handle: *uart_lock };

            write!(uart, "{}", record.level());
            write!(uart, " ");
            write!(uart, "{}", record.args());
            write!(uart, "\r\n");
        }
    }

    fn flush(&self) {}
}

#[no_mangle]
pub extern "C" fn rust_init() {
    unsafe {
        STATIC_DATA.write(StaticData {
            radio_ctx: Context::new(),
            lora_pkt_builder: lorapkt::DefaultBuilder::new(),
            sx130x_pps: PPSClock::new(),
            gps_time: None,
            next_gps_time: None,
            gps_task: freertos_rust::Task::new()
                .name("gps")
                .stack_size(512)
                .priority(freertos_rust::TaskPriority(2))
                .start(gps_fn)
                .unwrap(),
            encode_queue: freertos_rust::Queue::new(32).unwrap(),
            sign_queue: freertos_rust::Queue::new(32).unwrap(),
            encode_task: freertos_rust::Task::new()
                .name("encode")
                .stack_size(256)
                .priority(freertos_rust::TaskPriority(2))
                .start(encode_fn)
                .unwrap(),
            sign_task: freertos_rust::Task::new()
                .name("sign")
                .stack_size(524)
                .priority(freertos_rust::TaskPriority(1))
                .start(signing_fn)
                .unwrap(),
            uart: freertos_rust::Mutex::new(&huart1 as *const UART_HandleType).unwrap(),
        });

        log::set_logger(&LOGGER)
            .map(|()| log::set_max_level(LevelFilter::Trace))
            .unwrap();

        info!("NLighten Secure Concentrator v{}", GIT_VERSION);

        //RADIO_CTX.write(Context::new());

        //LORAPKT_BUILDER.write(lorapkt::DefaultBuilder::new());

        #[cfg(feature = "hwmon")]
        {
            freertos_rust::Task::new()
                .name("hwmom")
                .stack_size(210)
                .priority(freertos_rust::TaskPriority(1))
                .start(|t| {
                    let mut dbg_serial = UART { handle: &huart1 };

                    loop {
                        use core::fmt::Write;
                        CurrentTask::delay(Duration::ms(5000));
                        let t = FreeRtosUtils::get_all_tasks(None);
                        write!(&mut dbg_serial, "{}", t);
                    }
                })
                .unwrap();
        }
    }
}

#[no_mangle]
pub extern "C" fn ctx_sx1250_write(cmd: &spi_cmd_w_reg_1250_t) {
    match cmd.op_code {
        OPCODE_SET_RF_FREQUENCY => {
            let buf = &cmd.buf[..cmd.buf_size as usize];
            let radio_ctx = unsafe { &mut STATIC_DATA.assume_init_mut().radio_ctx };
            radio_ctx.rf_chain[(cmd.radio - 1) as usize]
                .freq
                .write(0, buf);
        }
        _ => {}
    }
}

#[no_mangle]
pub extern "C" fn ctx_sx130x_write(cmd: &spi_cmd_w_reg_130x_t) {
    let buf = &cmd.buf[..cmd.buf_size as usize];
    let radio_ctx = unsafe { &mut STATIC_DATA.assume_init_mut().radio_ctx };
    radio_ctx.sx130x_mem_0.write(cmd.address, buf);
    radio_ctx.sx130x_mem_1.write(cmd.address, buf);
}

#[no_mangle]
pub extern "C" fn ctx_sx130x_read(cmd: &spi_cmd_r_reg_130x_t) {
    unsafe {
        let mut dbg_serial = UART { handle: &huart1 };
        let buf = &cmd.buf[..cmd.buf_size as usize];
        let static_data = STATIC_DATA.assume_init_mut();

        match cmd.address {
            SX130X_FIFO => {
                //info!("raw data: {:?}", buf);

                for b in buf {
                    if let Some(pkt) = static_data.lora_pkt_builder.push(*b) {
                        #[cfg(feature = "debug")]
                        let tracer = Tracer::new("pkt received");

                        static_data.radio_ctx.gps_time = None;
                        if let Some(ref gps_time) = static_data.gps_time {
                            if let Ok(mut nano_diff) = static_data.sx130x_pps.to_ns(pkt.timestamp())
                            {
                                if let Ok(sub_clocks) =
                                    static_data.radio_ctx.fine_timestamp_sub_clocks(&pkt)
                                {
                                    // convert 32 Mhz clocks to nanoseconds
                                    nano_diff +=
                                        unsafe { core::intrinsics::roundf32(sub_clocks * 31.25) }
                                            as i32;
                                }

                                static_data.radio_ctx.gps_time =
                                    Some(gps_time.clone().saturating_add_nanos(nano_diff));
                            }
                        }

                        let sd = |pkt: &LoRaPacket| -> Result<Box<SigningData>, AllocError> {
                            Ok(Box::try_new(SigningData::try_new(
                                &pkt,
                                &static_data.radio_ctx,
                            )?)?)
                        };

                        match sd(&pkt) {
                            Ok(sd) => {
                                let _ = static_data
                                    .encode_queue
                                    .send(Box::into_raw(sd), Duration::zero());
                            }
                            Err(_) => {
                                let _ = uwrite!(dbg_serial, "could not queue signing: oom\r\n");
                            }
                        }
                    }
                }
            }

            SX130X_OTP_RD_DATA_RD_DATA => {
                let radio_ctx = &mut static_data.radio_ctx;
                radio_ctx.eui.set(radio_ctx.sx130x_mem_1[0], buf[0]);
            }
            _ => {}
        }
    }
}

unsafe fn read_fifo_buffer() {
    let mut cmd = spi_cmd_r_reg_130x_t {
        address: 0x58c8,
        buf_size: 2,
        buf: unsafe { core::mem::MaybeUninit::uninit().assume_init() },
    };

    spi_master_read_reg_130x(&mut cmd);
    let num_bytes_1 = u16::from_be_bytes([cmd.buf[0], cmd.buf[1]]);
    spi_master_read_reg_130x(&mut cmd);
    let num_bytes_2 = u16::from_be_bytes([cmd.buf[0], cmd.buf[1]]);

    let mut num_bytes = num_bytes_1.max(num_bytes_2);
    while num_bytes > 0 {
        let mut cmd = spi_cmd_r_reg_130x_t {
            address: 0x4000,
            buf_size: num_bytes.min(8) as u8,
            buf: unsafe { core::mem::MaybeUninit::uninit().assume_init() },
        };
        spi_master_read_reg_130x(&mut cmd);
        num_bytes -= cmd.buf_size as u16;
    }
}

unsafe fn read_last_pps() -> u32 {
    let mut cmd = spi_cmd_r_reg_130x_t {
        address: 0x6101,
        buf_size: 8,
        buf: unsafe { core::mem::MaybeUninit::uninit().assume_init() },
    };

    spi_master_read_reg_130x(&mut cmd);

    u32::from_be_bytes([cmd.buf[0], cmd.buf[1], cmd.buf[2], cmd.buf[3]])
}

#[no_mangle]
pub extern "C" fn ext_cfg_done() {
    // turn on auto read
    freertos_rust::Task::new()
        .name("auto")
        .stack_size(156)
        .priority(freertos_rust::TaskPriority(3))
        .start(|t| unsafe {
            let mut timer = TickTimer::new(300);

            loop {
                CurrentTask::delay(Duration::ms(10));
                read_fifo_buffer();

                if timer.tick() {
                    let pps_time = read_last_pps();
                    unsafe {
                        STATIC_DATA.assume_init_mut().sx130x_pps.last_pps(pps_time);
                    }
                }
            }
        })
        .unwrap();
}

fn encode_fn(task: Task) {
    unsafe {
        //let mut dbg_serial = core::cell::RefCell::new(unsafe { UART { handle: &huart1 } });
        let static_data = STATIC_DATA.assume_init_ref();

        loop {
            let pkt = Box::from_raw(
                static_data
                    .encode_queue
                    .receive(Duration::infinite())
                    .unwrap(),
            );

            #[cfg(feature = "debug")]
            let trace = Tracer::new("encode");

            fn encode_pkt(pkt: &SigningData) -> Result<(u32), serde_cbor::Error> {
                let pkt_rep = pkt.lora_pkt();
                let pkt_id = pkt_rep.pkt.key();

                let mut writer = LoraPacketRepStream::new(0);
                let mut serializer = serde_cbor::Serializer::new(&mut writer);
                let mut out_map = serializer.serialize_map(Some(2))?;
                out_map.serialize_entry("key", &pkt_id)?;
                out_map.serialize_entry("pkt", &pkt_rep)?;
                writer.flush();
                Ok(pkt_id)
            }

            match encode_pkt(&pkt) {
                Ok(pkt_id) => {
                    use kompressor_common::lora::RxPktRep;

                    if let Ok(hash) = Box::try_new((pkt_id, pkt.lora_pkt().as_hash())) {
                        let _ = static_data
                            .sign_queue
                            .send(Box::into_raw(hash), Duration::zero());
                    }
                }
                Err(e) => {
                    error!("error sending packet: {}", e);
                    // let _ = uwrite!(
                    //     dbg_serial.borrow_mut(),
                    //     "error sending packet: {}",
                    //     e.to_string().as_str()
                    // );
                }
            }
        }
    }
}

fn signing_fn(task: Task) {
    unsafe {
        //let mut dbg_serial = core::cell::RefCell::new(unsafe { UART { handle: &huart1 } });
        let static_data = STATIC_DATA.assume_init_ref();

        loop {
            let pkt_hash = Box::from_raw(
                static_data
                    .sign_queue
                    .receive(Duration::infinite())
                    .unwrap(),
            );

            #[cfg(feature = "debug")]
            let trace = Tracer::new("sign");

            fn sign_pkt(
                bundle: &Box<(u32, [u8; Sha512::SHA512_HASH_SIZE])>,
            ) -> Result<(), serde_cbor::Error> {
                let (pkt_id, pkt_hash) = (bundle.0, bundle.1);

                let sig = AppKeys::load().keypair().private.sign(pkt_hash);

                let mut writer = LoraPacketRepStream::new(1);
                let mut serializer = serde_cbor::Serializer::new(&mut writer);
                let mut out_map = serializer.serialize_map(Some(2))?;
                out_map.serialize_entry("key", &pkt_id)?;
                out_map.serialize_entry("sig", &sig[..])?;
                writer.flush();
                Ok(())
            }

            if let Err(e) = sign_pkt(&pkt_hash) {
                error!("error singing packet: {}", e);
                // let _ = uwrite!(
                //     dbg_serial.borrow_mut(),
                //     "error signing packet: {}",
                //     e.to_string().as_str()
                // );
            }
        }
    }
}

struct SigningData {
    raw_data: Box<[u8]>,
    ctx: Context,
}

impl SigningData {
    fn try_new(pkt: &LoRaPacket, ctx: &Context) -> Result<Self, AllocError> {
        fn from(slice: &[u8]) -> Result<Box<[u8]>, AllocError> {
            let len = slice.len();
            let layout = match Layout::array::<u8>(len) {
                Ok(l) => l,
                Err(_) => return Err(AllocError),
            };

            unsafe {
                let ptr = alloc::alloc::alloc(layout);
                ptr::copy_nonoverlapping(slice.as_ptr(), ptr, len);
                let slice_ptr = core::ptr::slice_from_raw_parts_mut(ptr, len);
                Ok(Box::from_raw(slice_ptr))
            }
        }

        Ok(Self {
            raw_data: from(pkt.raw_buf())?,
            ctx: ctx.clone(),
        })
    }

    fn lora_pkt<'a>(&'a self) -> LoraPacketRep<'a> {
        LoraPacketRep {
            pkt: LoRaPacket::from(self.raw_data.as_ref()),
            ctx: &self.ctx,
        }
    }
}

struct LoraPacketRepStream {
    stream_idx: u16,
    buf: [u8; spiproto::Packet::PAYLOAD_SIZE],
    i: usize,
}

impl LoraPacketRepStream {
    fn new(stream_idx: u16) -> Self {
        Self {
            stream_idx,
            buf: unsafe { core::mem::MaybeUninit::uninit().assume_init() },
            i: 0,
        }
    }

    fn flush(&mut self) {
        if self.i > 0 {
            let mut pkt = spiproto::Packet::default();
            let pkt_hdr = spiproto::PktHeader {
                dest: spiproto::Dest::Host,
                addr: self.stream_idx,
                len: self.i as u8,
                write: true,
                read: false,
            };
            pkt_hdr.write_header(&mut pkt);
            pkt.write_payload(&self.buf);

            unsafe {
                xQueueGenericSend(
                    spiSlaveTxQueue,
                    pkt.as_ptr() as *const cty::c_void,
                    1000,
                    queueSEND_TO_BACK,
                )
            };

            self.i = 0;
        }
    }

    fn finish(mut self) {
        self.flush();
    }
}

impl serde_cbor::ser::Write for LoraPacketRepStream {
    type Error = serde_cbor::Error;

    fn write_all(&mut self, mut buf: &[u8]) -> Result<(), Self::Error> {
        loop {
            if buf.is_empty() {
                break;
            }
            if self.i == spiproto::Packet::PAYLOAD_SIZE {
                self.flush();
            }

            self.buf[self.i] = buf[0];
            self.i += 1;
            buf = &buf[1..];
        }

        Ok(())
    }
}

#[repr(C)]
struct UART_HandleType {}

type StreamBufferHandle_t = *mut cty::c_void;
type TickType_t = u32;
const queueSEND_TO_BACK: cty::c_long = 0;
const queueSEND_TO_FRONT: cty::c_long = 1;
const queueOVERWRITE: cty::c_long = 2;

extern "C" {
    fn HAL_UART_Receive(
        handle: *const UART_HandleType,
        buf: *mut u8,
        size: u16,
        timeout: u32,
    ) -> cty::c_uint;
    fn HAL_UART_Receive_IT(handle: *const UART_HandleType, buf: *mut u8, size: u16) -> cty::c_uint;
    fn HAL_UART_Transmit(
        handle: *const UART_HandleType,
        buf: *const u8,
        size: u16,
        timeout: u32,
    ) -> cty::c_uint;
    fn resetGPS();
    fn xStreamBufferReceive(
        xStreamBuffer: StreamBufferHandle_t,
        RxData: *mut u8,
        xBufferLengthBytes: cty::size_t,
        xTicksToWait: TickType_t,
    ) -> cty::size_t;
    fn xTaskGetTickCount() -> TickType_t;
    fn xQueueGenericSend(
        xQueue: *const cty::c_void,
        pvItemToQueue: *const cty::c_void,
        xTicksToWait: TickType_t,
        xCopyPosition: cty::c_long,
    ) -> cty::c_long;
    fn spi_master_read_reg_130x(cmd: *mut spi_cmd_r_reg_130x_t);

    static huart1: UART_HandleType;
    static huart2: UART_HandleType;
    static mut gGPSRxBuf: [u8; 1];
    //static gpsRxQueue: StreamBufferHandle_t;
    static spiSlaveTxQueue: *const cty::c_void;
}

struct TickTimer {
    start: TickType_t,
    period: TickType_t,
}

impl TickTimer {
    fn new(period: TickType_t) -> Self {
        TickTimer {
            start: unsafe { xTaskGetTickCount() },
            period: period,
        }
    }

    fn tick(&mut self) -> bool {
        let now = unsafe { xTaskGetTickCount() };
        let diff = now.wrapping_sub(self.start);
        if diff >= self.period {
            self.start = now;
            return true;
        } else {
            return false;
        }
    }
}

const HAL_MAX_DELAY: u32 = 0xFFFFFFFF;
const HAL_OK: u32 = 0;

struct UART {
    handle: *const UART_HandleType,
}

impl io::Write for UART {
    fn write(&mut self, buf: &[u8]) -> io::Result<usize> {
        unsafe {
            if HAL_UART_Transmit(self.handle, buf.as_ptr(), buf.len() as u16, 100) == 0 {
                Ok(buf.len())
            } else {
                Err(io::Error::from(io::ErrorKind::Unknown))
            }
        }
    }
}

impl uWrite for UART {
    type Error = Infallible;

    fn write_str(&mut self, s: &str) -> Result<(), Self::Error> {
        self.write_all(s.as_bytes());
        Ok(())
    }
}

impl core::fmt::Write for UART {
    fn write_str(&mut self, s: &str) -> fmt::Result {
        self.write_all(s.as_bytes());
        Ok(())
    }
}

const UBX_CLASS_CFG: u8 = 0x06;
const UBX_CFG_PRT: u8 = 0x00;

const COM_PORT_UART1: u8 = 1;

