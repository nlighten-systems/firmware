include stm32l1.mk
-include .config

.PHONY: clean modules signapp dockerimage buildenv pushdockerimage menuconfig addlicense

APP_SIGN_KEY := keys/dev_key

BUILD_DIR := build

PREFIX = arm-none-eabi-
# The gcc compiler bin path can be either defined in make command via GCC_PATH variable (> make GCC_PATH=xxx)
# either it can be added to the PATH environment variable.
ifdef GCC_PATH
CC = $(GCC_PATH)/$(PREFIX)gcc
LD = $(GCC_PATH)/$(PREFIX)ld
CXX = $(GCC_PATH)/$(PREFIX)g++
AS = $(GCC_PATH)/$(PREFIX)gcc -x assembler-with-cpp
CP = $(GCC_PATH)/$(PREFIX)objcopy
SZ = $(GCC_PATH)/$(PREFIX)size
else
CC = $(PREFIX)gcc
CXX = $(PREFIX)g++
LD = $(PREFIX)ld
AS = $(PREFIX)gcc -x assembler-with-cpp
CP = $(PREFIX)objcopy
SZ = $(PREFIX)size
endif
HEX = $(CP) -O ihex
BIN = $(CP) -O binary -S
AR ?= ar

all: modules

clean:
	rm -rf $(BUILD_DIR)
	rm -rf target
	rm -rf flash_target.bin app_image_signed.bin hardware_key_bootloader.bin fullimage.bin

include build.mk
include scripts/stm32l.mk

$(BUILD_DIR)/config/build_config.h: .config target/debug/build_util
	mkdir -p $(BUILD_DIR)/config
	target/debug/build_util > $@ < $<


# Cortext M3 == thumbv7m-none-eabi
ARCH := cm3
BOARD := kompressor
TARGET := $(BOARD)_$(ARCH)

C_FLAGS := $(STM32L1_C_FLAGS)

$(eval $(call newmodule))
MODULE := stm32l1xx_hal_driver_bootloader
INCLUDES := src/bootloader/stm32_boilerplate/inc \
src/CMSIS/Include \
src/CMSIS/Device/ST/STM32L1xx/Include \
src/ST/STM32L1xx_HAL_Driver/Inc \

SRC_FILES := \
src/ST/STM32L1xx_HAL_Driver/Src/stm32l1xx_hal.c \
src/ST/STM32L1xx_HAL_Driver/Src/stm32l1xx_hal_cortex.c \
src/ST/STM32L1xx_HAL_Driver/Src/stm32l1xx_hal_dma.c \
src/ST/STM32L1xx_HAL_Driver/Src/stm32l1xx_hal_exti.c \
src/ST/STM32L1xx_HAL_Driver/Src/stm32l1xx_hal_flash.c \
src/ST/STM32L1xx_HAL_Driver/Src/stm32l1xx_hal_flash_ex.c \
src/ST/STM32L1xx_HAL_Driver/Src/stm32l1xx_hal_flash_ramfunc.c \
src/ST/STM32L1xx_HAL_Driver/Src/stm32l1xx_hal_gpio.c \
src/ST/STM32L1xx_HAL_Driver/Src/stm32l1xx_hal_pwr.c \
src/ST/STM32L1xx_HAL_Driver/Src/stm32l1xx_hal_pwr_ex.c \
src/ST/STM32L1xx_HAL_Driver/Src/stm32l1xx_hal_rcc.c \
src/ST/STM32L1xx_HAL_Driver/Src/stm32l1xx_hal_rcc_ex.c \
src/ST/STM32L1xx_HAL_Driver/Src/stm32l1xx_hal_spi.c \
src/ST/STM32L1xx_HAL_Driver/Src/stm32l1xx_hal_tim.c \
src/ST/STM32L1xx_HAL_Driver/Src/stm32l1xx_hal_tim_ex.c \
src/ST/STM32L1xx_HAL_Driver/Src/stm32l1xx_hal_uart.c

$(eval $(call build_staticlib))


###################### FreeRTOS #############################

$(eval $(call newmodule))
MODULE := freertos
INCLUDES := src/app/stm32_boilerplate/inc \
src/ST/STM32L1xx_HAL_Driver/Inc \
src/freertos/include \
src/freertos/portable/GCC/ARM_CM3 \
src/CMSIS/Include 

SRC_FILES := \
src/freertos/croutine.c \
src/freertos/event_groups.c \
src/freertos/list.c \
src/freertos/queue.c \
src/freertos/stream_buffer.c \
src/freertos/tasks.c \
src/freertos/timers.c \
src/freertos/shim.c \
src/freertos/portable/GCC/ARM_CM3/port.c \
src/freertos/portable/MemMang/heap_4.c

$(eval $(call build_staticlib))

$(eval $(call newmodule))
MODULE := bootloader_rs
SRC_FILES := target/thumbv7m-none-eabi/release/libbootloader_rs.a
$(eval $(call extern_staticlib))

$(eval $(call newmodule))
MODULE := app_rs
SRC_FILES := target/thumbv7m-none-eabi/release/libapp_rs.a
$(eval $(call extern_staticlib))

################## Bootloader ########################

$(eval $(call newmodule))
MODULE := bootloader
INCLUDES := src/config \
src/bootloader/stm32_boilerplate/inc \
src/CMSIS/Include \
src/CMSIS/Device/ST/STM32L1xx/Include \
src/ST/STM32L1xx_HAL_Driver/Inc

SRC_FILES := \
  src/startup.s \
  src/bootloader/stm32_boilerplate/main.c \
  src/bootloader/stm32_boilerplate/stm32l1xx_hal_msp.c \
  src/bootloader/stm32_boilerplate/stm32l1xx_it.c \
  src/bootloader/stm32_boilerplate/system_stm32l1xx.c \
  src/bootloader/appstart.c

LOCAL_OBJS += $(BUILD_DIR)/bootloader_pubkey.o

LINK_DEPEND := bootloader_rs stm32l1xx_hal_driver_bootloader
LOCAL_LD := $(CC)
LD_FLAGS := -Tsrc/bootloader/stm32_boilerplate/stm32l152cb.ld $(STM32L1_CPU_FLAGS) -specs=nano.specs -Wl,-Map=$(BUILD_DIR)/$(MODULE).map,--cref -Wl,--gc-sections -Wl,--print-memory-usage
$(eval $(call build_exe))

###################### App ################################

$(BUILD_DIR)/$(TARGET)/app/app_stm32l152cb.ld: src/app/stm32_boilerplate/app_stm32l152cb.ld.in
	cpp -Isrc/config $< | grep -v '^#' > $@

$(BUILD_DIR)/$(TARGET)/app/app.elf: $(BUILD_DIR)/$(TARGET)/app/app_stm32l152cb.ld


$(eval $(call newmodule))
MODULE := app
INCLUDES := $(BUILD_DIR)/config \
src/config \
src/app/stm32_boilerplate/inc \
src/app \
$(stm32-include-y) \
src/freertos/include \
src/freertos/portable/GCC/ARM_CM3 \
src/freertos/CMSIS_RTOS

SRC_FILES := $(stm32-y) \
  src/app/stm32_boilerplate/freertos.c \
  src/app/stm32_boilerplate/main.c \
  src/app/stm32_boilerplate/startup_stm32l152cbtx.s \
  src/app/stm32_boilerplate/stm32l1xx_hal_msp.c \
  src/app/stm32_boilerplate/stm32l1xx_hal_timebase_tim.c \
  src/app/stm32_boilerplate/stm32l1xx_it.c \
  src/app/stm32_boilerplate/sysmem.c \
  src/app/stm32_boilerplate/system_stm32l1xx.c \
  src/app/hal.c \
  src/app/gps.c \
  src/app/spi_helper.c

COMPILE_DEPEND := $(BUILD_DIR)/config/build_config.h
LINK_DEPEND := app_rs freertos
LOCAL_LD := $(CC)
LD_FLAGS := -T$(BUILD_DIR)/$(TARGET)/app/app_stm32l152cb.ld $(STM32L1_CPU_FLAGS) --specs=nosys.specs -Wl,-Map=$(BUILD_DIR)/$(MODULE).map,--cref -Wl,--gc-sections -Wl,--print-memory-usage -Wl,--allow-multiple-definition
$(eval $(call build_exe))



###################### App debug ################################

$(BUILD_DIR)/$(TARGET)/app_debug/app_stm32l152cb_debug.ld: src/app/stm32_boilerplate/app_stm32l152cb_debug.ld.in
	cpp -Isrc/config $< | grep -v '^#' > $@

$(BUILD_DIR)/$(TARGET)/app_debug/app_debug.elf: $(BUILD_DIR)/$(TARGET)/app_debug/app_stm32l152cb_debug.ld

$(eval $(call newmodule))
MODULE := app_debug
INCLUDES := $(BUILD_DIR)/config \
src/config \
src/app/stm32_boilerplate/inc \
src/app \
$(stm32-include-y) \
src/freertos/include \
src/freertos/portable/GCC/ARM_CM3 \
src/freertos/CMSIS_RTOS

SRC_FILES := $(stm32-y) \
  src/app/stm32_boilerplate/freertos.c \
  src/app/stm32_boilerplate/main.c \
  src/app/stm32_boilerplate/startup_stm32l152cbtx.s \
  src/app/stm32_boilerplate/stm32l1xx_hal_msp.c \
  src/app/stm32_boilerplate/stm32l1xx_hal_timebase_tim.c \
  src/app/stm32_boilerplate/stm32l1xx_it.c \
  src/app/stm32_boilerplate/sysmem.c \
  src/app/stm32_boilerplate/system_stm32l1xx.c \
  src/app/hal.c \
  src/app/gps.c \
  src/app/spi_helper.c

COMPILE_DEPEND := $(BUILD_DIR)/config/build_config.h
LINK_DEPEND := app_rs freertos
LOCAL_LD := $(CC)
LD_FLAGS := -T$(BUILD_DIR)/$(TARGET)/app_debug/app_stm32l152cb_debug.ld $(STM32L1_CPU_FLAGS) --specs=nosys.specs -Wl,-Map=$(BUILD_DIR)/$(MODULE).map,--cref -Wl,--gc-sections -Wl,--print-memory-usage -Wl,--allow-multiple-definition
$(eval $(call build_exe))

RSCORE_SOURCES := $(shell find src/rscore -name '*.rs' -o -name 'Cargo.toml')
APP_SOURCES := $(shell find src/app -name '*.rs' -o -name 'Cargo.toml')
BOOTLOADER_SOURCES := $(shell find src/bootloader -name '*.rs' -o -name 'Cargo.toml')
FREERTOS_RS_SOURCES := $(shell find src/freertos-rust -name '*.rs' -o -name 'Cargo.toml')
UTILS_SOURCES := $(shell find src/utils -name '*.rs' -o -name 'Cargo.toml')


modules: $(ALL_EXE) $(ALL_LIBS) target/debug/utils

$(BUILD_DIR)/bootloader_pubkey.o: $(APP_SIGN_KEY).pub | $(LOCAL_BUILD_DIR)
	@echo "app sign key: $(APP_SIGN_KEY)"
	cp $(APP_SIGN_KEY).pub $(BUILD_DIR)/app_key.pub
	$(AS) -I $(BUILD_DIR) -o $@ -c src/bootloader/app_key.s

flash:
	st-flash --reset write $(BUILD_DIR)/bootloader/bootloader.bin 0x08000000

flash_bootloader_jlink: build/kompressor_cm3/bootloader/bootloader.bin
	cp build/kompressor_cm3/bootloader/bootloader.bin flash_target.bin
	JLinkExe -device STM32L152CB -commanderscript flash_jlink.txt

flash_meta_jlink: hardware_key.priv
	JLinkExe -device STM32L152CB -commanderscript flash_meta_jlink.txt

flash_full_image: fullimage.bin
	cp fullimage.bin flash_target.bin
	JLinkExe -device STM32L152CB -commanderscript flash_jlink.txt

openocd:
	openocd -f openocd.cfg

telnet:
	telnet localhost 4444

gdb: $(BUILD_DIR)/bootloader/bootloader.elf
	$(GDB) --eval-command="target remote localhost:3333" $<

dockerimage: docker_image/Dockerfile
	cd docker_image && docker build -t kompressor_build .

pushdockerimage: docker_image/Dockerfile
	docker login registry.gitlab.com
	cd docker_image && docker build -t registry.gitlab.com/nlighten-systems/firmware .
	docker push registry.gitlab.com/nlighten-systems/firmware

buildenv:
	docker run --rm -it --user=$(shell id -u):$(shell id -g) -v $(abspath .):/workdir kompressor_build /bin/bash

target/thumbv7em-none-eabihf/release/libbootloader_rs.a: src/bootloader/src/lib.rs
	rustc --version && cargo --version
	cargo +nightly build --release --target=thumbv7em-none-eabihf 

target/thumbv7m-none-eabi/release/libapp_rs.a: $(APP_SOURCES) $(FREERTOS_RS_SOURCES) $(RSCORE_SOURCES)
	rustc --version && cargo --version
	cargo +nightly build --release --target=thumbv7m-none-eabi --package app

target/thumbv7m-none-eabi/release/libbootloader_rs.a: $(BOOTLOADER_SOURCES) $(RSCORE_SOURCES)
	rustc --version && cargo --version
	cargo +nightly build --release --target=thumbv7m-none-eabi --package bootloader

target/debug/utils: $(UTILS_SOURCES) $(RSCORE_SOURCES)
	rustc --version && cargo --version
	cargo +nightly build --package utils

target/debug/build_util: $(UTILS_SOURCES) $(RSCORE_SOURCES)
	rustc --version && cargo --version
	cargo build --package build_util

app_image_signed.bin: build/kompressor_cm3/app/app.bin target/debug/utils
	./target/debug/utils sign-app -i build/kompressor_cm3/app/app.bin --key $(APP_SIGN_KEY).priv

hardware_key.priv: target/debug/utils
	./target/debug/utils gen-key --name hardware_key

hardware_key_bootloader.bin: target/debug/utils build/kompressor_cm3/bootloader/bootloader.bin hardware_key.priv
	./target/debug/utils meta -b build/kompressor_cm3/bootloader/bootloader.bin -n hardware_key --serial 1540097

fullimage.bin: hardware_key_bootloader.bin app_image_signed.bin
	cat hardware_key_bootloader.bin > fullimage.bin
	cat app_image_signed.bin >> fullimage.bin

tests:
	cargo +nightly test --package=rscore --features="std"

menuconfig:
	kconfig-mconf KConfig

addlicense:
	licensesnip src/app/src
	licensesnip src/bootloader/src
	licensesnip src/kompressor_common/src
	licensesnip src/rscore/src
	licensesnip src/utils/src
	licensesnip src/app/hal.c
	licensesnip src/app/gps.c
	licensesnip src/app/radios.c

