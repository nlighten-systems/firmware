// Copyright (C) NLighten Systems, LLC (Paul Soucy) 2022
//
// This file is part of Kompressor Firmware.
//
// Kompressor Firmware is free software: you can redistribute it and/or modify it under the terms of
// the GNU General Public License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any later version.
//
// Kompressor Firmware is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with Foobar. If not, see
// <https://www.gnu.org/licenses/>.

#include "main.h"
#include <string.h>
#include <stdio.h>

extern UART_HandleTypeDef huart1;
extern UART_HandleTypeDef huart2;

extern StreamBufferHandle_t gpsRxQueue;

uint8_t gGPSRxBuf[1];


void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart)
{
  if(huart == &huart2) {
    BaseType_t xHigherPriorityTaskWoken = pdFALSE;
    xStreamBufferSendFromISR(gpsRxQueue, &gGPSRxBuf[0], 1, &xHigherPriorityTaskWoken);
    HAL_UART_Receive_IT(&huart2, &gGPSRxBuf[0], 1);
    portYIELD_FROM_ISR( xHigherPriorityTaskWoken );
  }
}