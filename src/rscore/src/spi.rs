// Copyright (C) NLighten Systems, LLC (Paul Soucy) 2022
//
// This file is part of Kompressor Firmware.
//
// Kompressor Firmware is free software: you can redistribute it and/or modify it under the terms of
// the GNU General Public License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any later version.
//
// Kompressor Firmware is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with Foobar. If not, see
// <https://www.gnu.org/licenses/>.

use crate::checksum::Checksum;

pub const PAYLOAD_SIZE: usize = 16;

const SYNC_WORD_1: u8 = 0xB5;
const SYNC_WORD_2: u8 = 0x4E;

pub struct SpiPkt {
    payload: [u8; PAYLOAD_SIZE],
}

impl SpiPkt {
    pub fn write(&self, buf: &mut [u8]) {
        let mut checksum = Checksum::new();

        buf[0] = SYNC_WORD_1;
        buf[1] = SYNC_WORD_2;

        checksum.update(&self.payload);
        for i in 0..PAYLOAD_SIZE {
            buf[2 + i] = self.payload[i];
        }

        let checksum = checksum.finalize();
        buf[2 + PAYLOAD_SIZE] = checksum[0];
        buf[2 + 1 + PAYLOAD_SIZE] = checksum[1];
    }
}
