// Copyright (C) NLighten Systems, LLC (Paul Soucy) 2022
//
// This file is part of Kompressor Firmware.
//
// Kompressor Firmware is free software: you can redistribute it and/or modify it under the terms of
// the GNU General Public License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any later version.
//
// Kompressor Firmware is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with Foobar. If not, see
// <https://www.gnu.org/licenses/>.

use core::ops::{Deref, DerefMut};

#[cfg(feature = "serde")]
use serde::ser::SerializeMap;
use ufmt::{uWrite, uwrite, Formatter};

use adler::Adler32;

use kompressor_common::prelude::*;
use kompressor_common::{
    gps::{GPSTime, WGS84Position},
    lora::{Bandwidth, CodingRate, Datarate, MacAddress, RxPktRep, SpreadingFactor},
};

use int_enum::{IntEnum, IntEnumError};

#[derive(Clone, Copy)]
pub struct Register<const START: u16, const N: usize> {
    value: [u8; N],
}

impl<const START: u16, const N: usize> Register<START, N> {
    pub fn new() -> Self {
        Self { value: [0u8; N] }
    }

    pub fn start(&self) -> u16 {
        START
    }

    pub fn contains(&self, addr: u16) -> bool {
        START <= addr && addr < (START + N as u16)
    }

    pub fn write(&mut self, addr: u16, buf: &[u8]) -> bool {
        let contains = self.contains(addr);
        if contains {
            let addr_rel = (addr - START) as usize;
            let len = buf.len().min(N - addr_rel);
            for i in 0..len {
                self.value[addr_rel + i] = buf[i];
            }
        }
        contains
    }
}

impl<const START: u16, const N: usize> Deref for Register<START, N> {
    type Target = [u8; N];

    fn deref(&self) -> &Self::Target {
        &self.value
    }
}

impl<const START: u16, const N: usize> DerefMut for Register<START, N> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.value
    }
}

#[derive(Clone, Copy)]
pub struct RFChain {
    pub freq: Register<0, 4>,
}

impl RFChain {
    pub fn new(freq: u32) -> Self {
        let mut thiz = RFChain {
            freq: Register::new(),
        };
        thiz.set_freq_hz(freq);
        thiz
    }

    pub fn set_freq_hz(&mut self, freq: u32) {
        let reg = (freq as u64 * (1 << 25) / 32_000_000) as u32;
        *self.freq = reg.to_be_bytes();
    }

    pub fn freq_hz(&self) -> u32 {
        let reg = u32::from_be_bytes(*self.freq);
        (reg as u64 * 32_000_000 / (1 << 25)) as u32
    }
}

#[derive(Clone, Copy, Default)]
pub struct EUI([u8; 8]);

impl EUI {
    pub fn set(&mut self, i: u8, v: u8) {
        if let Some(elem) = self.0.get_mut(i as usize) {
            *elem = v;
        }
    }

    pub fn value(&self) -> u64 {
        u64::from_be_bytes(self.0)
    }

    pub fn as_mac(&self) -> MacAddress {
        self.0.into()
    }
}

#[derive(Clone)]
pub struct Context {
    pub rf_chain: [RFChain; 2],
    pub sx130x_mem_0: Register<0x5800, 16>,
    pub sx130x_mem_1: Register<0x6180, 1>,
    pub gps_time: Option<GPSTime>,
    pub position: Option<WGS84Position>,
    pub eui: EUI,
}

impl Context {
    pub fn new() -> Self {
        Self {
            rf_chain: [RFChain {
                freq: Register::new(),
            }; 2],
            sx130x_mem_0: Register::new(),
            sx130x_mem_1: Register::new(),
            gps_time: None,
            position: None,
            eui: EUI::default(),
        }
    }

    pub fn if_chain_freq_hz(&self, ifchain_idx: usize) -> i32 {
        let mem_idx = ifchain_idx * 2;
        let mut value = (0x1F00 & ((self.sx130x_mem_0[mem_idx] as i16) << 8))
            | (0x00FF & ((self.sx130x_mem_0[mem_idx + 1] as i16) << 0));
        value = value << 3;
        value = value >> 3;
        (value as i32 * 15625) / 32
    }

    pub fn if_chain_set_freq_hz(&mut self, ifchain_idx: usize, freq: i32) {
        let reg = (freq * 32) / 15625;
        let mem_idx = ifchain_idx * 2;
        self.sx130x_mem_0[mem_idx] = (0x1F & (reg >> 8)) as u8;
        self.sx130x_mem_0[mem_idx + 1] = (0xFF & (reg >> 0)) as u8;
    }

    pub fn freq_hz(&self, ifchain_idx: usize) -> u32 {
        let rf_chain = match ifchain_idx {
            0..=3 => &self.rf_chain[0],
            4..=7 => &self.rf_chain[1],
            _ => panic!("unknown ifchain_idx"),
        };
        let relitive_freq = self.if_chain_freq_hz(ifchain_idx);
        ((rf_chain.freq_hz() as i32) + relitive_freq) as u32
    }

    pub fn service_bandwith(&self) -> Bandwidth {
        //TODO: get actual bandwidth
        Bandwidth::BW500
    }

    pub fn fine_timestamp_sub_clocks(&self, pkt: &LoRaPacket) -> Result<f32, Error> {
        let mut ftime = [0_i32; 64];
        let ts_metrics_nb = pkt.num_ts();

        let ts_metrics_nb_clipped = match pkt.spreading_factor() {
            Ok(SpreadingFactor::SF12) => ts_metrics_nb.min(4),
            Ok(SpreadingFactor::SF11) => ts_metrics_nb.min(8),
            Ok(SpreadingFactor::SF10) => ts_metrics_nb.min(16),
            Ok(_) => ts_metrics_nb.min(32),
            Err(_) => return Err(Error::BadPkt),
        };

        let ts_metrics = pkt.timestamp_metrics();
        ftime[0] = ts_metrics[0] as i32;
        let mut ftime_sum = ftime[0];
        for i in 1..(2 * ts_metrics_nb_clipped) {
            ftime[i] = ftime[i - 1] + ts_metrics[i] as i32;
            ftime_sum += ftime[i];
        }

        let ftime_mean = ftime_sum as f32 / (2 * ts_metrics_nb_clipped) as f32;

        Ok(ftime_mean)
    }

    pub fn sx130x_write_reg(&mut self, addr: u16, buf: &[u8]) {
        self.sx130x_mem_0.write(addr, buf);
        self.sx130x_mem_1.write(addr, buf);
    }
}

#[derive(Debug, PartialEq, Eq)]
pub enum Error {
    SyncWord,
    Incomplete,
    ChecksumError,
    BadPkt,
}

//#[derive(Clone, Copy)]
pub struct LoRaPacket<'a> {
    buf: &'a [u8],
}

const SX130X_PKT_SYNC: [u8; 2] = [0xA5, 0xC0];
const SX1302_PKT_HEAD_METADATA: usize = 7;

impl<'a> LoRaPacket<'a> {
    pub fn raw_buf(&self) -> &'a [u8] {
        self.buf
    }

    pub fn payload_len(&self) -> usize {
        self.buf[0] as usize
    }

    pub fn payload(&self) -> &[u8] {
        &self.buf[SX1302_PKT_HEAD_METADATA..(SX1302_PKT_HEAD_METADATA + self.payload_len())]
    }

    pub fn num_ts(&self) -> usize {
        self.buf[self.payload_len() + SX1302_PKT_HEAD_METADATA + 12] as usize
    }

    pub fn timestamp_metrics(&self) -> &[i8] {
        let start = self.payload_len() + SX1302_PKT_HEAD_METADATA + 13;
        let end = start + (self.num_ts() * 2);
        unsafe { core::mem::transmute(&self.buf[start..end]) }
    }

    pub fn modem_id(&self) -> u8 {
        self.buf[3]
    }

    pub fn rx_channel(&self) -> usize {
        self.buf[1] as usize
    }

    pub fn crc_enable(&self) -> bool {
        0x01 & self.buf[2] > 0
    }

    pub fn coding_rate(&self) -> Result<CodingRate, IntEnumError<CodingRate>> {
        let value: u8 = 0x07 & (self.buf[2] >> 1);
        CodingRate::from_int(value)
    }

    pub fn spreading_factor(&self) -> Result<SpreadingFactor, IntEnumError<SpreadingFactor>> {
        let value: u8 = 0x0F & (self.buf[2] >> 4);
        SpreadingFactor::from_int(value)
    }

    pub fn payload_crc_error(&self) -> bool {
        0x01 & self.buf[7 + self.payload_len()] > 0
    }

    pub fn sync_error(&self) -> bool {
        0x04 & self.buf[7 + self.payload_len()] > 0
    }

    pub fn header_error(&self) -> bool {
        0x08 & self.buf[7 + self.payload_len()] > 0
    }

    pub fn timing_set(&self) -> bool {
        0x10 & self.buf[7 + self.payload_len()] > 0
    }

    /**
     * average RSSI of the channel in dB
     */
    pub fn rssi_channel(&self) -> u8 {
        self.buf[9 + self.payload_len()]
    }

    /**
     * average RSSI of the signal in dB
     */
    pub fn rssi_signal(&self) -> u8 {
        self.buf[10 + self.payload_len()]
    }

    pub fn rx_crc16(&self) -> u16 {
        u16::from_le_bytes([
            self.buf[17 + self.payload_len()],
            self.buf[18 + self.payload_len()],
        ])
    }

    /**
     * snr is in steps of 0.25dB
     */
    pub fn snr(&self) -> i8 {
        i8::from_le_bytes([self.buf[8 + self.payload_len()]])
    }

    pub fn frequency_offset_error(&self) -> i32 {
        let mut value: i32 = ((0x0F & self.buf[6] as i32) << 16)
            | ((self.buf[5] as i32) << 8)
            | ((self.buf[4] as i32) << 0);

        if value > (1 << 19) {
            /* Handle signed value on 20bits */
            value = value - (1 << 20);
        }

        value
    }

    pub fn timestamp(&self) -> u32 {
        u32::from_le_bytes([
            self.buf[13 + self.payload_len()],
            self.buf[14 + self.payload_len()],
            self.buf[15 + self.payload_len()],
            self.buf[16 + self.payload_len()],
        ])
    }

    pub fn freq_hz(&self, ctx: &Context) -> u32 {
        ctx.freq_hz(self.rx_channel())
    }

    /// get a key that uniquely identifies this packet
    pub fn key(&self) -> u32 {
        let mut hasher = Adler32::new();
        hasher.write_slice(&self.buf);
        hasher.checksum()
    }

    pub fn fmt<W>(&self, ctx: &Context, formatter: &mut Formatter<W>)
    where
        W: uWrite,
    {
        let bandwith = match self.rx_channel() {
            0..=7 => Bandwidth::BW125,
            _ => ctx.service_bandwith(),
        };
        let datarate = Datarate(self.spreading_factor().unwrap(), bandwith);

        let _ = uwrite!(formatter, "freq: {} {}", self.freq_hz(ctx), datarate);
    }
}

impl<'a> From<&'a [u8]> for LoRaPacket<'a> {
    fn from(buf: &'a [u8]) -> Self {
        Self { buf: buf }
    }
}

pub struct LoraPacketRep<'a> {
    pub pkt: LoRaPacket<'a>,
    pub ctx: &'a Context,
}

impl<'a> LoraPacketRep<'a> {
    pub fn rssi_channel(&self) -> u8 {
        self.pkt.rssi_channel()
    }

    pub fn crc_enable(&self) -> bool {
        self.pkt.crc_enable()
    }

    pub fn payload_crc_error(&self) -> bool {
        self.pkt.payload_crc_error()
    }
}

impl<'a> RxPktRep for LoraPacketRep<'a> {
    fn gps_time(&self) -> Option<GPSTime> {
        self.ctx.gps_time
    }

    fn freq(&self) -> u32 {
        self.pkt.freq_hz(self.ctx)
    }

    fn datarate(&self) -> Datarate {
        let bandwith = match self.pkt.rx_channel() {
            0..=7 => Bandwidth::BW125,
            _ => self.ctx.service_bandwith(),
        };
        Datarate(self.pkt.spreading_factor().unwrap(), bandwith)
    }

    fn snr(&self) -> i32 {
        // Convert to units of 0.01dB
        self.pkt.snr() as i32 * 25
    }

    fn rssi(&self) -> u8 {
        self.pkt.rssi_signal()
    }

    fn tmst(&self) -> u32 {
        self.pkt.timestamp()
    }

    fn gatwy_id(&self) -> MacAddress {
        self.ctx.eui.as_mac()
    }

    fn pos(&self) -> Option<WGS84Position> {
        self.ctx.position
    }

    fn payload(&self) -> &[u8] {
        self.pkt.payload()
    }
}

#[cfg(feature = "serde")]
impl<'a> serde::Serialize for LoraPacketRep<'a> {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: serde::Serializer,
    {
        let mut m = serializer.serialize_map(None)?;

        m.serialize_entry("freq", &self.freq())?;
        m.serialize_entry("datarate", &self.datarate())?;
        m.serialize_entry("snr", &self.snr())?;
        m.serialize_entry("rssi", &self.rssi())?;
        m.serialize_entry("rssic", &self.rssi_channel())?;
        m.serialize_entry("tmst", &self.tmst())?;
        m.serialize_entry("gatwy_id", &self.gatwy_id())?;
        m.serialize_entry("crc_en", &self.crc_enable())?;
        m.serialize_entry("crc_err", &self.payload_crc_error())?;

        if let Some(ref time) = self.gps_time() {
            m.serialize_entry("gps_time", time)?;
        }

        if let Some(ref position) = self.ctx.position {
            m.serialize_entry("pos", position)?;
        }

        m.serialize_entry("payload", self.pkt.payload())?;
        m.end()
    }
}

struct Checksum {
    checksum: u8,
}

impl Checksum {
    fn new() -> Self {
        Checksum { checksum: 0 }
    }

    fn update(&mut self, b: u8) {
        self.checksum = self.checksum.wrapping_add(b);
    }

    fn finalize(&self) -> u8 {
        self.checksum
    }
}

pub type DefaultBuilder = Builder<{ 256 + 20 + 2 * 3 }>;

pub struct Builder<const N: usize> {
    buf: [u8; N],
    payload_len: u8,
    num_ts: u8,
    checksum: Checksum,
    i: u16,
    state: u16,
}

impl<const N: usize> Builder<N> {
    pub fn new() -> Self {
        Self {
            buf: unsafe { core::mem::MaybeUninit::uninit().assume_init() },
            payload_len: unsafe { core::mem::MaybeUninit::uninit().assume_init() },
            num_ts: unsafe { core::mem::MaybeUninit::uninit().assume_init() },
            checksum: unsafe { core::mem::MaybeUninit::uninit().assume_init() },
            i: unsafe { core::mem::MaybeUninit::uninit().assume_init() },
            state: 0,
        }
    }

    pub fn push(&mut self, b: u8) -> Option<LoRaPacket> {
        match self.state {
            0 => {
                if b == SX130X_PKT_SYNC[0] {
                    self.state = 1;
                    self.checksum = Checksum::new();
                    self.checksum.update(b);
                }
            }
            1 => {
                if b == SX130X_PKT_SYNC[1] {
                    self.state = 2;
                    self.checksum.update(b);
                } else {
                    self.state = 0;
                }
            }
            2 => {
                self.payload_len = b;
                self.checksum.update(b);
                self.state = 3;
                self.i = 0;
                self.buf[0] = b;
            }
            3 => {
                self.checksum.update(b);
                self.i += 1;
                if (self.i as usize) < N {
                    self.buf[self.i as usize] = b;
                }

                if self.i == 6 + self.payload_len as u16 + 13 {
                    self.num_ts = b;
                }
                if self.i == 6 + self.payload_len as u16 + 13 + (2 * self.num_ts as u16) {
                    self.state = 4;
                }
            }
            4 => {
                self.state = 0;
                if self.i as usize > N {
                    return None;
                }
                let checksum_r = b;
                if self.checksum.finalize() == checksum_r {
                    return Some(LoRaPacket {
                        buf: &self.buf[..(self.i as usize + 1)],
                    });
                }
            }
            _ => panic!(),
        }

        None
    }
}

#[cfg(feature = "std")]
#[cfg(test)]
mod test {
    use super::*;

    use serde_json::json;

    use core::convert::Infallible;
    use std::num::ParseIntError;

    fn decode_hex(s: &str) -> Result<Vec<u8>, ParseIntError> {
        (0..s.len())
            .step_by(2)
            .map(|i| u8::from_str_radix(&s[i..i + 2], 16))
            .collect()
    }

    fn default_ctx() -> Context {
        let mut ctx = Context::new();
        // ctx.time.year = 2022;
        // ctx.time.month = 4;
        // ctx.time.day = 7;
        // ctx.time.hour = 13;
        // ctx.time.minute = 4;
        // ctx.time.second = 53;
        ctx.rf_chain[0] = RFChain::new(904300000);
        ctx.rf_chain[1] = RFChain::new(905000000);
        ctx.if_chain_set_freq_hz(0, -400000);
        ctx.if_chain_set_freq_hz(1, -200000);
        ctx.if_chain_set_freq_hz(2, 0);
        ctx.if_chain_set_freq_hz(3, 200000);
        ctx.if_chain_set_freq_hz(4, -300000);
        ctx.if_chain_set_freq_hz(5, -100000);
        ctx.if_chain_set_freq_hz(6, 100000);
        ctx.if_chain_set_freq_hz(7, 300000);

        ctx
        /*
        Context {
            rf_chain: [RFChain::new(904300000), RFChain::new(905000000)],
            if_chain: [
                IFChain{rf_chain: 0, freq_hz: -400000},
                IFChain{rf_chain: 0, freq_hz: -200000},
                IFChain{rf_chain: 0, freq_hz: 0},
                IFChain{rf_chain: 0, freq_hz:  200000},
                IFChain{rf_chain: 1, freq_hz: -300000},
                IFChain{rf_chain: 1, freq_hz: -100000},
                IFChain{rf_chain: 1, freq_hz:  100000},
                IFChain{rf_chain: 1, freq_hz:  300000},
            ]
        }
        */
    }

    struct StrBuf {
        buf: String,
    }

    impl StrBuf {
        fn new() -> Self {
            Self { buf: String::new() }
        }
    }

    impl uWrite for StrBuf {
        type Error = Infallible;

        fn write_str(&mut self, s: &str) -> Result<(), Self::Error> {
            self.buf.push_str(s);
            Ok(())
        }
    }

    // #[test]
    // fn test_ref() {
    //     let ctx = default_ctx();

    //     //let buf = &decode_hex("a5c00b04730840b90068656c6c6f20776f726c6400e4787111338cea572b009e00eb").unwrap();
    //     let buf = &decode_hex("a5c03403930b10c30c40001a08018544eef700205008ebe8b55acda285ea6038be8fefc3f6f30b30cea18a90141006b22c17abdbd380bb025ab17619de01d7867c51426c207cf133e40050a5c03400930810fb0f4030da0001852762f700109300eb78d593cda2804f6030beefe3cff6560b30ce31ea1c141006126c17ab885380bb0259b97229de0031bfbf00024c227cf1b3e400f5").unwrap();
    //     let mut builder = DefaultBuilder::new();
    //     for b in buf {
    //         if let Some(ref pkt) = builder.push(*b) {
    //             let pkt_rep = LoraPacketRep {
    //                 pkt: pkt,
    //                 ctx: &ctx,
    //             };

    //             let json_str = serde_json::to_string(&pkt_rep).unwrap();
    //             println!("pkt: {} => {}", pkt.key(), json_str);
    //             //assert_eq!("", json_str);
    //         }
    //     }
    // }

    #[test]
    fn test_builder() {
        let buf = &decode_hex("a5c00002600880590300db76715f22dc6159160000009a").unwrap();
        //let buf: &[u8] = &decode_hex("a5c00004600800f10200d7756e3f21e4fafb4380b60030").unwrap();
        let (last, rest) = buf.split_last().unwrap();
        let mut builder = DefaultBuilder::new();
        for b in rest {
            assert!(builder.push(*b).is_none());
        }
        let pkt = builder.push(*last);
        assert!(pkt.is_some());
    }

    #[test]
    fn test_rx_channel() {
        let buf = &decode_hex("a5c00002600880590300db76715f22dc6159160000009a").unwrap();
        let (last, rest) = buf.split_last().unwrap();
        let mut builder = DefaultBuilder::new();
        for b in rest {
            assert!(builder.push(*b).is_none());
        }
        let pkt = builder.push(*last).unwrap();
        let rx_channel = pkt.rx_channel();
        assert!(rx_channel <= 8);
    }

    #[test]
    fn test_modem_id() {
        let buf = &decode_hex("a5c00002600880590300db76715f22dc6159160000009a").unwrap();
        let (last, rest) = buf.split_last().unwrap();
        let mut builder = DefaultBuilder::new();
        for b in rest {
            assert!(builder.push(*b).is_none());
        }
        let pkt = builder.push(*last).unwrap();
        let modem_id = pkt.modem_id();
        assert!(modem_id <= 16);
    }

    #[test]
    fn test_sf() {
        let buf = &decode_hex("a5c00002600880590300db76715f22dc6159160000009a").unwrap();
        let (last, rest) = buf.split_last().unwrap();
        let mut builder = DefaultBuilder::new();
        for b in rest {
            assert!(builder.push(*b).is_none());
        }
        let pkt = builder.push(*last).unwrap();
        let sf = pkt.spreading_factor();
        assert!(sf.is_ok());
    }

    #[test]
    fn test_sf_11() {
        let buf =
            &decode_hex("a5c00802b300f4ca0c686565cfb110749b01b878693f2674da719a3ad80027").unwrap();
        let (last, rest) = buf.split_last().unwrap();
        let mut builder = DefaultBuilder::new();
        for b in rest {
            assert!(builder.push(*b).is_none());
        }
        let pkt = builder.push(*last).unwrap();
        let sf = pkt.spreading_factor();
        assert!(sf.is_ok());
        if let Ok(sf) = sf {
            assert_eq!(sf, SpreadingFactor::SF11);
        }
    }

    const HELLO_WORLD_PKT: &'static str =
        "a5c00b04730840b90068656c6c6f20776f726c6400e4787111338cea572b009e00eb";

    #[test]
    fn test_payload() {
        let buf = &decode_hex(HELLO_WORLD_PKT).unwrap();
        let (last, rest) = buf.split_last().unwrap();
        let mut builder = DefaultBuilder::new();
        for b in rest {
            assert!(builder.push(*b).is_none());
        }
        let pkt = builder.push(*last).unwrap();
        let payload = pkt.payload();
        let str_payload = String::from_utf8_lossy(payload);
        assert_eq!(b"hello world", payload);
    }

    const HELLO_WORLD_FINETIME_PKT: [u8; 54] = [
        165, 192, 11, 7, 115, 8, 128, 126, 0, 104, 101, 108, 108, 111, 32, 119, 111, 114, 108, 100,
        0, 231, 117, 111, 33, 34, 243, 88, 222, 47, 0, 158, 10, 0, 0, 0, 8, 8, 0, 248, 0, 248, 248,
        248, 0, 0, 8, 0, 0, 16, 248, 0, 0, 90,
    ];

    #[test]
    fn test_fine_timestamp_metrics() {
        let buf = &HELLO_WORLD_FINETIME_PKT;

        let (last, rest) = buf.split_last().unwrap();
        let mut builder = DefaultBuilder::new();
        for b in rest {
            assert!(builder.push(*b).is_none());
        }
        let pkt = builder.push(*last).unwrap();
        let metrics = pkt.timestamp_metrics();
        assert!(metrics.len() > 0);

        let ctx = default_ctx();
        let sub_clocks = ctx.fine_timestamp_sub_clocks(&pkt);
        assert_eq!(Ok(-0.8_f32), sub_clocks);
    }
}
