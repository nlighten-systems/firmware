// Copyright (C) NLighten Systems, LLC (Paul Soucy) 2022
//
// This file is part of Kompressor Firmware.
//
// Kompressor Firmware is free software: you can redistribute it and/or modify it under the terms of
// the GNU General Public License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any later version.
//
// Kompressor Firmware is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with Foobar. If not, see
// <https://www.gnu.org/licenses/>.

use core::{
    cell::UnsafeCell,
    future::Future,
    pin::Pin,
    sync::atomic::{compiler_fence, Ordering},
    task::{Context, Poll, Waker},
};

pub struct Mailbox<T> {
    inner: UnsafeCell<Inner<T>>,
}

struct Inner<T> {
    cell: Option<T>,
    waker: Option<Waker>,
}

impl<T> Inner<T> {
    fn notify(&mut self) {
        if let Some(w) = self.waker.take() {
            w.wake();
        }
    }
}

unsafe impl<T> Sync for Mailbox<T> {}

impl<T> Mailbox<T>
where
    T: Copy,
{
    pub const fn new() -> Self {
        Self {
            inner: UnsafeCell::new(Inner {
                cell: None,
                waker: None,
            }),
        }
    }

    pub fn send<'a, 'b>(&'a self, mail: &'a T) -> impl Future<Output = ()> + 'a {
        pub struct MailSender<'a, 'b, T> {
            mailbox: &'a Mailbox<T>,
            mail: &'b T,
        }

        impl<'a, 'b, T> Future for MailSender<'a, 'b, T>
        where
            T: Copy,
        {
            type Output = ();

            fn poll(self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<Self::Output> {
                compiler_fence(Ordering::Acquire);
                let mut inner = unsafe { &mut *self.mailbox.inner.get() };
                match inner.cell {
                    None => {
                        inner.cell.replace(*self.mail);
                        inner.notify();
                        compiler_fence(Ordering::Release);
                        Poll::Ready(())
                    }

                    Some(_) => {
                        inner.waker = Some(cx.waker().clone());
                        Poll::Pending
                    }
                }
            }
        }

        MailSender {
            mailbox: self,
            mail: mail,
        }
    }

    pub fn take<'a>(&'a self) -> impl Future<Output = T> + 'a {
        pub struct MailTaker<'a, T> {
            mailbox: &'a Mailbox<T>,
        }

        impl<'a, T> Future for MailTaker<'a, T> {
            type Output = T;

            fn poll(self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<Self::Output> {
                compiler_fence(Ordering::Acquire);
                let mut inner = unsafe { &mut *self.mailbox.inner.get() };
                match inner.cell.take() {
                    None => {
                        inner.waker = Some(cx.waker().clone());
                        Poll::Pending
                    }

                    Some(v) => {
                        inner.notify();
                        compiler_fence(Ordering::Acquire);
                        Poll::Ready(v)
                    }
                }
            }
        }

        MailTaker { mailbox: self }
    }

    pub fn try_take(&self) -> Option<T> {
        let inner = unsafe { &mut *self.inner.get() };
        let retval = inner.cell.take();
        inner.notify();
        return retval;
    }
}
