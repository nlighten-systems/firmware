// Copyright (C) NLighten Systems, LLC (Paul Soucy) 2022
//
// This file is part of Kompressor Firmware.
//
// Kompressor Firmware is free software: you can redistribute it and/or modify it under the terms of
// the GNU General Public License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any later version.
//
// Kompressor Firmware is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with Foobar. If not, see
// <https://www.gnu.org/licenses/>.

// inspired by : https://github.com/awelkie/xmodem.rs/blob/master/src/lib.rs
//

use crate::io::*;

const SOH: u8 = 0x01;
#[allow(dead_code)]
const STX: u8 = 0x02;
const EOT: u8 = 0x04;
const ACK: u8 = 0x06;
const NAK: u8 = 0x15;
const CAN: u8 = 0x18;
const CRC: u8 = 0x43;

pub struct XModem {}

impl XModem {
    pub fn new() -> Self {
        XModem {}
    }

    pub fn receve<D, W>(&mut self, dev: &mut D, out: &mut W) -> Result<()>
    where
        D: Read + Write,
        W: Write,
    {
        let mut data = [0_u8; 128];
        let mut num_errors = 0;
        write_byte(dev, CRC)?;
        let mut packet_num: u8 = 1;
        loop {
            match read_byte(dev) {
                Ok(SOH) => {
                    let p1 = read_byte(dev)?;
                    let p2 = read_byte(dev)?;
                    let expected_packet = p1 == packet_num && (255 - p1) == p2;
                    dev.read_exact(&mut data)?;
                    let recevied_checksum =
                        ((read_byte(dev)? as u16) << 8) + read_byte(dev)? as u16;
                    let success = calc_crc(&data) == recevied_checksum && expected_packet;
                    if !success {
                        write_byte(dev, NAK)?;
                        num_errors += 1;
                    } else {
                        out.write_all(&data)?;
                        write_byte(dev, ACK)?;
                        packet_num = packet_num.wrapping_add(1);
                    }
                }
                Ok(EOT) => {
                    write_byte(dev, ACK)?;
                    break;
                }
                Ok(CAN) => {
                    return Err(ErrorKind::Cancel.into());
                }
                Ok(_) => {
                    num_errors += 1;
                }
                Err(Error {
                    kind: ErrorKind::TimedOut,
                }) => {
                    write_byte(dev, CRC)?;
                }
                Err(e) => return Err(e),
            }
            if num_errors > 5 {
                return Err(ErrorKind::Unknown.into());
            }
        }
        Ok(())
    }
}

fn calc_crc(data: &[u8]) -> u16 {
    crc16::State::<crc16::XMODEM>::calculate(data)
}

fn write_byte(writer: &mut impl Write, byte: u8) -> Result<()> {
    let buf = [byte];
    writer.write_all(&buf)
}

fn read_byte(reader: &mut impl Read) -> Result<u8> {
    let mut buf = [0_u8];
    reader.read_exact(&mut buf)?;
    Ok(buf[0])
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_recev() {
        let xmodem = XModem::new();
    }
}
