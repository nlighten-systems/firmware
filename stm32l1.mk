

#TARGET := STM32L1
STM32L1_CPU_FLAGS := -mcpu=cortex-m3 -mthumb -mfloat-abi=soft
STM32L1_C_FLAGS := -DUSE_HAL_DRIVER -DSTM32L152xB -g $(STM32L1_CPU_FLAGS) -Os -fdata-sections -ffunction-sections

