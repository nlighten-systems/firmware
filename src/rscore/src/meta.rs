// Copyright (C) NLighten Systems, LLC (Paul Soucy) 2022
//
// This file is part of Kompressor Firmware.
//
// Kompressor Firmware is free software: you can redistribute it and/or modify it under the terms of
// the GNU General Public License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any later version.
//
// Kompressor Firmware is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with Foobar. If not, see
// <https://www.gnu.org/licenses/>.

// use crate::ed25519::{
//     KeyPair, PublicKey, ED25519_PRIVATE_KEY_SIZE, ED25519_PUBLIC_KEY_SIZE, PrivateKey
// };

use core::slice;

use crate::serialnum::SerialNum;
use kompressor_common::crypto::ed25519::KeyPair;

pub struct Meta([u8; 256]);

impl Meta {
    pub fn get() -> &'static Meta {
        let p = crate::META_START as *const Meta;
        unsafe { &*p }
    }

    pub fn keypair(&self) -> &'static KeyPair {
        let p = &self.0 as *const u8 as *const KeyPair;
        unsafe { &*p }
    }

    pub fn serial(&self) -> SerialNum {
        let p = &self.0 as *const u8;
        unsafe {
            let p = p.offset(32 + 64);
            SerialNum::from_bytes(slice::from_raw_parts(p, 4).try_into().unwrap())
        }
    }
}
