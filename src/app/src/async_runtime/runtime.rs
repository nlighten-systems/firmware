// Copyright (C) NLighten Systems, LLC (Paul Soucy) 2022
//
// This file is part of Kompressor Firmware.
//
// Kompressor Firmware is free software: you can redistribute it and/or modify it under the terms of
// the GNU General Public License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any later version.
//
// Kompressor Firmware is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with Foobar. If not, see
// <https://www.gnu.org/licenses/>.

use alloc::alloc;
use core::{
    alloc::Layout,
    cell::UnsafeCell,
    future::Future,
    pin::Pin,
    sync::atomic::{AtomicBool, Ordering},
    task::{Context, Poll, RawWaker, RawWakerVTable, Waker},
};
use heapless::Vec;
use log::debug;

use cortex_m::asm;

use super::NTASKS;

pub struct SingleThreadAsyncExecutor {
    tasks: UnsafeCell<Vec<&'static Task, NTASKS>>,
}

impl SingleThreadAsyncExecutor {
    pub fn new() -> Self {
        Self {
            tasks: UnsafeCell::new(Vec::new()),
        }
    }

    pub fn spawn(&self, f: impl Future<Output = ()> + 'static) {
        unsafe {
            if (*self.tasks.get()).push(Task::new(f)).is_err() {
                // Out of memory
                panic!();
            }
        }
    }

    pub fn run(&self) -> ! {
        unsafe {
            let tasks = &*self.tasks.get();

            loop {
                for i in 0..NTASKS {
                    if let Some(task) = tasks.get(i) {
                        if task.ready.load(Ordering::Acquire) {
                            // we are about to service the task so switch the `ready` flag to `false`
                            task.ready.store(false, Ordering::Release);

                            // NOTE we never deallocate tasks so `&ready` is always pointing to
                            // allocated memory (`&'static AtomicBool`)
                            let waker = unsafe {
                                Waker::from_raw(RawWaker::new(
                                    &task.ready as *const _ as *const _,
                                    &VTABLE,
                                ))
                            };
                            let mut cx = Context::from_waker(&waker);

                            let task_fn = Pin::new_unchecked(&mut *task.f.get());
                            let _ = task_fn.poll(&mut cx);
                        }
                    }
                }
            }
        }
    }

    pub fn block(mut f: impl Future<Output = ()>) {
        let ready = AtomicBool::new(false);

        loop {
            // we are about to service the task so switch the `ready` flag to `false`
            ready.store(false, Ordering::Release);

            let waker =
                unsafe { Waker::from_raw(RawWaker::new(&ready as *const _ as *const _, &VTABLE)) };
            let mut cx = Context::from_waker(&waker);

            let task_fn = unsafe { Pin::new_unchecked(&mut f) };
            if let Poll::Ready(_) = task_fn.poll(&mut cx) {
                break;
            }
        }
    }
}

type Task = Node<dyn Future<Output = ()> + 'static>;

struct Node<F>
where
    F: ?Sized,
{
    ready: AtomicBool,
    f: UnsafeCell<F>,
}

impl Task {
    fn new(f: impl Future<Output = ()> + 'static) -> &'static mut Self {
        alloc_init(Node {
            ready: AtomicBool::new(true),
            f: UnsafeCell::new(f),
        })
    }
}

fn alloc_init<T>(val: T) -> &'static mut T {
    unsafe {
        let layout = Layout::new::<T>();
        debug!("layout: {:?}", layout);
        let ptr = alloc::alloc(layout) as *mut T;
        ptr.write(val);
        &mut *ptr
    }
}

static VTABLE: RawWakerVTable = {
    unsafe fn clone(p: *const ()) -> RawWaker {
        RawWaker::new(p, &VTABLE)
    }
    unsafe fn wake(p: *const ()) {
        wake_by_ref(p)
    }
    unsafe fn wake_by_ref(p: *const ()) {
        (*(p as *const AtomicBool)).store(true, Ordering::Release)
    }
    unsafe fn drop(_: *const ()) {
        // no-op
    }

    RawWakerVTable::new(clone, wake, wake_by_ref, drop)
};

pub async fn r#yield() {
    struct Yield {
        yielded: bool,
    }

    impl Future for Yield {
        type Output = ();

        fn poll(mut self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<Self::Output> {
            if self.yielded {
                Poll::Ready(())
            } else {
                self.yielded = true;
                // wake ourselves
                cx.waker().wake_by_ref();
                //asm::sev();
                Poll::Pending
            }
        }
    }

    Yield { yielded: false }.await
}
