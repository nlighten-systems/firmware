// There are different ways to embed binary files into the app image
// this approch uses the .incbin assembly trick. See https://www.devever.net/~hl/incbin

.section .rodata
.global app_key
.align 4

app_key:
.incbin "app_key.pub"
