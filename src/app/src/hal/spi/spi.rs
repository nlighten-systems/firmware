// Copyright (C) NLighten Systems, LLC (Paul Soucy) 2022
//
// This file is part of Kompressor Firmware.
//
// Kompressor Firmware is free software: you can redistribute it and/or modify it under the terms of
// the GNU General Public License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any later version.
//
// Kompressor Firmware is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with Foobar. If not, see
// <https://www.gnu.org/licenses/>.

use core::{
    future::Future,
    marker::PhantomData,
    pin::Pin,
    sync::atomic::{compiler_fence, AtomicBool, Ordering},
    task::{Context, Poll},
};

use crate::hal::*;

pub fn spi1_write<'a>(data: &'a [u8]) {
    unsafe {
        HAL_SPI_Transmit(&hspi1, data.as_ptr(), data.len() as u16, HAL_MAX_DELAY);
    }
}

pub fn spi1_read<'a>(data: &mut [u8]) {
    unsafe {
        HAL_SPI_Receive(&hspi1, data.as_mut_ptr(), data.len() as u16, HAL_MAX_DELAY);
    }
}

#[inline]
pub fn spi1_cs_set() {
    unsafe { spi1_nss_set() }
}

#[inline]
pub fn spi1_cs_clear() {
    unsafe { spi1_nss_clear() }
}

static mut SPI_TRANSACTION: SpiTransaction = SpiTransaction {
    complete: AtomicBool::new(false),
};

struct SpiTransaction {
    complete: AtomicBool,
}

impl SpiTransaction {
    fn reset(&mut self) {
        self.complete.store(false, Ordering::Relaxed);
    }
}

pub struct SpiWrite<'a> {
    phantom: PhantomData<&'a [u8]>,
}

impl<'a> Future for SpiWrite<'a> {
    type Output = ();

    fn poll(self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<Self::Output> {
        unsafe {
            match SPI_TRANSACTION.complete.load(Ordering::Acquire) {
                true => Poll::Ready(()),

                false => {
                    cx.waker().wake_by_ref();
                    Poll::Pending
                }
            }
        }
    }
}

pub struct SpiRead<'a> {
    phantom: PhantomData<&'a [u8]>,
}

impl<'a> Future for SpiRead<'a> {
    type Output = ();

    fn poll(self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<Self::Output> {
        unsafe {
            match SPI_TRANSACTION.complete.load(Ordering::Acquire) {
                true => Poll::Ready(()),

                false => {
                    cx.waker().wake_by_ref();
                    Poll::Pending
                }
            }
        }
    }
}

#[no_mangle]
extern "C" fn SpiTransferComplete(_hspi: *const SPI_HandleTypeDef) {
    unsafe {
        SPI_TRANSACTION.complete.store(true, Ordering::Release);
    }
}

extern "C" {
    fn spi1_nss_set();
    fn spi1_nss_clear();
    pub fn xQueueGenericSend(
        handle: *const cty::c_void,
        item: *const cty::c_void,
        ticks: u32,
        e: u32,
    ) -> u32;
    pub fn xQueueReceive(handle: *const cty::c_void, item: *mut cty::c_void, ticks: u32) -> u32;
}
