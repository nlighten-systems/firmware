// Copyright (C) NLighten Systems, LLC (Paul Soucy) 2022
//
// This file is part of Kompressor Firmware.
//
// Kompressor Firmware is free software: you can redistribute it and/or modify it under the terms of
// the GNU General Public License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any later version.
//
// Kompressor Firmware is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with Foobar. If not, see
// <https://www.gnu.org/licenses/>.

use core::fmt::Display;
use core::future::Future;

use int_enum::{IntEnum, IntEnumError};
use ufmt::{derive::uDebug, uDisplay, uwrite};

use crate::checksum::Checksum;
use crate::io::AsyncWriter;
use crate::{
    io::{self, Write},
    utils::UTCTime,
};
use kompressor_common::gps::WGS84Position;

pub enum Error {
    SyncWord,
    Checksum,
}

const UBX_SYNCH_1: u8 = 0xB5;
const UBX_SYNCH_2: u8 = 0x62;

pub struct UbxPkt<'a> {
    cls: u8,
    id: u8,
    payload: &'a [u8],
}

impl<'a> UbxPkt<'a> {
    pub fn new(cls: u8, id: u8, payload: &'a [u8]) -> Self {
        UbxPkt {
            cls: cls,
            id: id,
            payload: payload,
        }
    }

    pub fn from_bytes(buf: &'a [u8]) -> Result<Self, Error> {
        if buf[0..2] != [UBX_SYNCH_1, UBX_SYNCH_2] {
            return Err(Error::SyncWord);
        }

        let len = u16::from_le_bytes([buf[4], buf[5]]);

        let mut checksum = Checksum::new();
        checksum.update(&buf[2..6 + len as usize]);

        if !checksum.verify(&buf[6 + len as usize..6 + len as usize + 2]) {
            return Err(Error::Checksum);
        }

        Ok(UbxPkt {
            cls: buf[2],
            id: buf[3],
            payload: &buf[6..6 + len as usize],
        })
    }

    pub fn write<W: Write>(&self, pipe: &mut W) -> io::Result<()> {
        let mut checksum = Checksum::new();
        let len: u16 = self.payload.len() as u16;

        pipe.write_all(&[UBX_SYNCH_1, UBX_SYNCH_2])?;

        let header = &[self.cls, self.id, (0x00FF & len) as u8, (len >> 8) as u8];
        checksum.update(header);
        pipe.write_all(header)?;

        // Write payload.
        checksum.update(self.payload);
        pipe.write_all(self.payload)?;

        // Write checksum
        pipe.write_all(&checksum.finalize())?;

        Ok(())
    }

    pub async fn write_async<W>(&self, pipe: &mut W) -> io::Result<()>
    where
        W: AsyncWriter,
    {
        let mut checksum = Checksum::new();
        let len: u16 = self.payload.len() as u16;

        pipe.write(&[UBX_SYNCH_1, UBX_SYNCH_2]).await?;

        let header = &[self.cls, self.id, (0x00FF & len) as u8, (len >> 8) as u8];
        checksum.update(header);
        pipe.write(header).await?;

        // Write payload.
        checksum.update(self.payload);
        pipe.write(self.payload).await?;

        // Write checksum
        pipe.write(&checksum.finalize()).await?;

        Ok(())
    }
}

impl<'a> Display for UbxPkt<'a> {
    fn fmt(&self, f: &mut core::fmt::Formatter<'_>) -> core::fmt::Result {
        write!(f, "[{:#04x} {:#04x} {:?}]", self.cls, self.id, self.payload)
    }
}

impl<'a> uDisplay for UbxPkt<'a> {
    fn fmt<W>(&self, fmt: &mut ufmt::Formatter<'_, W>) -> Result<(), W::Error>
    where
        W: ufmt::uWrite + ?Sized,
    {
        uwrite!(fmt, "[{},{}, {:?}]", self.cls, self.id, self.payload)?;
        Ok(())
    }
}

#[derive(Debug)]
pub enum TypeError {
    InvalidType,
}

#[derive(uDebug, Debug, PartialEq, Eq, PartialOrd, Ord)]
pub enum FixType {
    NoFix,
    DeadReckoning,
    Fix2D,
    Fix3D,
    GNASSAndDeadReckoning,
    TimeOnly,
}

pub struct UbxNavPVT<'a> {
    payload: &'a [u8],
}

impl<'a> UbxNavPVT<'a> {
    pub fn fix_type(&self) -> Result<FixType, TypeError> {
        let fix = u8::from_le_bytes([self.payload[20]]);
        match fix {
            0 => Ok(FixType::NoFix),
            1 => Ok(FixType::DeadReckoning),
            2 => Ok(FixType::Fix2D),
            3 => Ok(FixType::Fix3D),
            4 => Ok(FixType::GNASSAndDeadReckoning),
            5 => Ok(FixType::TimeOnly),
            _ => Err(TypeError::InvalidType),
        }
    }

    pub fn content_valid(&self) -> u8 {
        u8::from_le_bytes([self.payload[1]])
    }

    pub fn num_sv(&self) -> u8 {
        u8::from_le_bytes([self.payload[23]])
    }

    /// longitude (deg) 1e-7 scale
    fn lon(&self) -> i32 {
        i32::from_le_bytes([
            self.payload[24],
            self.payload[25],
            self.payload[26],
            self.payload[27],
        ])
    }

    /// latitude (deg) 1e-7 scale
    fn lat(&self) -> i32 {
        i32::from_le_bytes([
            self.payload[28],
            self.payload[29],
            self.payload[30],
            self.payload[31],
        ])
    }

    /// Horizontal accuracy estimate (mm)
    fn hacc(&self) -> u32 {
        u32::from_le_bytes([
            self.payload[40],
            self.payload[41],
            self.payload[42],
            self.payload[43],
        ])
    }

    /// Height above ellipsoid (mm)
    fn height(&self) -> i32 {
        i32::from_le_bytes([
            self.payload[32],
            self.payload[33],
            self.payload[34],
            self.payload[35],
        ])
    }

    /// Vertical accuracy estimate (mm)
    fn vacc(&self) -> u32 {
        u32::from_le_bytes([
            self.payload[44],
            self.payload[45],
            self.payload[46],
            self.payload[47],
        ])
    }

    pub fn position(&self) -> Option<WGS84Position> {
        let content_valid = self.content_valid();
        let extra_pvt = (0x01 & content_valid) > 0;

        match self.fix_type() {
            Ok(FixType::Fix2D) | Ok(FixType::Fix3D) => Some(WGS84Position {
                lon: self.lon(),
                lat: self.lat(),
                height: self.height(),
                hacc: self.hacc(),
                vacc: if extra_pvt { Some(self.vacc()) } else { None },
            }),

            _ => None,
        }
    }
}

impl<'a> TryFrom<&UbxPkt<'a>> for UbxNavPVT<'a> {
    type Error = TypeError;

    fn try_from(value: &UbxPkt<'a>) -> Result<Self, Self::Error> {
        if value.cls == 0x01 && value.id == 0x07 {
            Ok(UbxNavPVT {
                payload: value.payload,
            })
        } else {
            Err(Self::Error::InvalidType)
        }
    }
}

impl<'a> uDisplay for UbxNavPVT<'a> {
    fn fmt<W>(&self, formatter: &mut ufmt::Formatter<'_, W>) -> Result<(), W::Error>
    where
        W: ufmt::uWrite + ?Sized,
    {
        let time = self as &dyn UTCTime;
        let fix = match self.fix_type() {
            Ok(fix) => fix,
            Err(_) => FixType::NoFix,
        };
        let num_sv = self.num_sv();
        match self.position() {
            Some(pos) => uwrite!(
                formatter,
                "pos: {}\r\ntime: {}\r\nfix: {:?}\r\nnum_sv: {}",
                pos,
                time,
                fix,
                num_sv
            ),
            None => uwrite!(
                formatter,
                "pos: None\r\ntime: {}\r\nfix: {:?}\r\nnum_sv: {}",
                time,
                fix,
                num_sv
            ),
        }
    }
}

impl<'a> Display for UbxNavPVT<'a> {
    fn fmt(&self, f: &mut core::fmt::Formatter<'_>) -> core::fmt::Result {
        let pos = self.position();
        let time = self as &dyn UTCTime;
        let fix = match self.fix_type() {
            Ok(fix) => fix,
            Err(_) => FixType::NoFix,
        };
        let num_sv = self.num_sv();

        match pos {
            Some(pos) => {
                write!(f, "{:?} pos: {} time: {} sv: {}", fix, pos, time, num_sv)
            }
            None => {
                write!(f, "{:?} time: {}, sv: {}", fix, time, num_sv)
            }
        }
    }
}

impl<'a> UTCTime for UbxNavPVT<'a> {
    fn year(&self) -> u16 {
        u16::from_le_bytes([self.payload[4], self.payload[5]])
    }

    fn month(&self) -> u8 {
        u8::from_le_bytes([self.payload[6]])
    }

    fn day(&self) -> u8 {
        u8::from_le_bytes([self.payload[7]])
    }

    fn hour(&self) -> u8 {
        u8::from_le_bytes([self.payload[8]])
    }

    fn minute(&self) -> u8 {
        u8::from_le_bytes([self.payload[9]])
    }

    fn second(&self) -> u8 {
        u8::from_le_bytes([self.payload[10]])
    }

    fn nano(&self) -> i32 {
        i32::from_le_bytes([
            self.payload[16],
            self.payload[17],
            self.payload[18],
            self.payload[19],
        ])
    }
}

#[repr(u8)]
#[derive(Debug, IntEnum, Copy, Clone, PartialEq, Eq)]
pub enum GnassTimebase {
    Gps = 0,
    Glonass = 1,
    BeiDou = 2,
    Galielo = 3,
    NavIC = 4,
    Unknown = 15,
}

#[derive(Debug)]
pub struct UbxTimTp<'a> {
    payload: &'a [u8],
}

impl<'a> UbxTimTp<'a> {
    pub const LEN: usize = 16;

    pub fn week(&self) -> u16 {
        u16::from_le_bytes([self.payload[12], self.payload[13]])
    }

    /// Time of Week (ms)
    pub fn tow_ms(&self) -> u32 {
        u32::from_le_bytes([
            self.payload[0],
            self.payload[1],
            self.payload[2],
            self.payload[3],
        ])
    }

    /// submillisecond part of tow_ms (2^-32 ms)
    pub fn tow_sub_ms(&self) -> u32 {
        u32::from_le_bytes([
            self.payload[4],
            self.payload[5],
            self.payload[6],
            self.payload[7],
        ])
    }

    pub fn flags(&self) -> u8 {
        u8::from_le_bytes([self.payload[14]])
    }

    pub const FLAG_TIMEBASE: u8 = (1 << 0);
    pub const FLAG_TIMEBASE_GNSS: u8 = 0;
    pub const FLAG_TIMEBASE_UTC: u8 = 1;

    pub fn flag_timebase_is_gnss(&self) -> bool {
        (self.flags() & UbxTimTp::FLAG_TIMEBASE) == UbxTimTp::FLAG_TIMEBASE_GNSS
    }

    pub fn refinfo(&self) -> u8 {
        u8::from_le_bytes([self.payload[15]])
    }

    pub fn ref_timebase_gnss(&self) -> Result<GnassTimebase, IntEnumError<GnassTimebase>> {
        let raw = self.refinfo() & 0x0F;
        GnassTimebase::from_int(raw)
    }
}

impl<'a> TryFrom<&UbxPkt<'a>> for UbxTimTp<'a> {
    type Error = TypeError;

    fn try_from(value: &UbxPkt<'a>) -> Result<Self, Self::Error> {
        if value.cls == 0x0D && value.id == 0x01 {
            Ok(UbxTimTp {
                payload: value.payload,
            })
        } else {
            Err(Self::Error::InvalidType)
        }
    }
}

impl<'a> Display for UbxTimTp<'a> {
    fn fmt(&self, f: &mut core::fmt::Formatter<'_>) -> core::fmt::Result {
        write!(f, "{}.{}", self.week(), self.tow_ms())
    }
}

pub struct UbxCfgTP5v1<'a> {
    payload: &'a [u8],
}

impl<'a> UbxCfgTP5v1<'a> {
    pub const LEN: usize = 32;

    pub fn version(&self) -> u8 {
        u8::from_le_bytes([self.payload[1]])
    }

    pub fn ant_cable_delay(&self) -> i16 {
        i16::from_le_bytes([self.payload[4], self.payload[5]])
    }

    pub fn freq_period(&self) -> u32 {
        u32::from_le_bytes([
            self.payload[8],
            self.payload[9],
            self.payload[10],
            self.payload[11],
        ])
    }

    pub fn freq_period_lock(&self) -> u32 {
        u32::from_le_bytes([
            self.payload[12],
            self.payload[13],
            self.payload[14],
            self.payload[15],
        ])
    }

    pub fn pulse_len_ratio(&self) -> u32 {
        u32::from_le_bytes([
            self.payload[16],
            self.payload[17],
            self.payload[18],
            self.payload[19],
        ])
    }

    pub fn pulse_len_ratio_lock(&self) -> u32 {
        u32::from_le_bytes([
            self.payload[20],
            self.payload[21],
            self.payload[22],
            self.payload[23],
        ])
    }

    pub fn flags(&self) -> u32 {
        u32::from_le_bytes([
            self.payload[28],
            self.payload[29],
            self.payload[30],
            self.payload[31],
        ])
    }
}

impl<'a> TryFrom<&UbxPkt<'a>> for UbxCfgTP5v1<'a> {
    type Error = TypeError;

    fn try_from(value: &UbxPkt<'a>) -> Result<Self, Self::Error> {
        let item = UbxCfgTP5v1 {
            payload: value.payload,
        };
        if value.cls == 0x06 && value.id == 0x31 && item.version() == 1 {
            Ok(item)
        } else {
            Err(Self::Error::InvalidType)
        }
    }
}

impl<'a> Display for UbxCfgTP5v1<'a> {
    fn fmt(&self, f: &mut core::fmt::Formatter<'_>) -> core::fmt::Result {
        write!(
            f,
            "period: {}, len: {} flags: {:x}",
            self.freq_period(),
            self.pulse_len_ratio(),
            self.flags()
        )
    }
}

pub struct Builder<const N: usize> {
    buf: [u8; N],
    cls: u8,
    id: u8,
    len: u16,
    i: u16,
    checksum: Checksum,
    read_ch_a: u8,
    state: u16,
}

impl<const N: usize> Builder<N> {
    pub fn new() -> Self {
        Self {
            buf: [0_u8; N],
            cls: 0,
            id: 0,
            len: 0,
            i: 0,
            checksum: Checksum::new(),
            read_ch_a: 0,
            state: 0,
        }
    }

    pub fn push(&mut self, b: u8) -> Option<UbxPkt> {
        match self.state {
            0 => {
                if b == UBX_SYNCH_1 {
                    self.state = 1;
                }
            }
            1 => {
                if b == UBX_SYNCH_2 {
                    self.state = 2;
                } else {
                    self.state = 0;
                }
            }
            2 => {
                self.cls = b;
                self.checksum = Checksum::new();
                self.checksum.update(&[b]);
                self.state = 3;
            }
            3 => {
                self.id = b;
                self.checksum.update(&[b]);
                self.state = 4;
            }
            4 => {
                self.len = b as u16;
                self.checksum.update(&[b]);
                self.state = 5;
            }
            5 => {
                self.len |= (b as u16) << 8;
                self.checksum.update(&[b]);
                self.i = 0;
                self.state = 6;
            }
            6 => {
                self.checksum.update(&[b]);
                if (self.i as usize) < N {
                    self.buf[self.i as usize] = b;
                }
                self.i += 1;
                if self.i == self.len {
                    self.state = 7;
                }
            }
            7 => {
                self.read_ch_a = b;
                self.state = 8;
            }
            8 => {
                self.state = 0;
                if self.i as usize > N {
                    return None;
                }
                if self.checksum.finalize() == [self.read_ch_a, b] {
                    return Some(UbxPkt {
                        cls: self.cls,
                        id: self.id,
                        payload: &self.buf[..self.len as usize],
                    });
                }
            }
            _ => {
                panic!()
            }
        }
        None
    }
}

#[cfg(test)]
mod test {
    use ufmt::Formatter;

    use super::*;

    #[test]
    fn test_parse() {
        let buf = [
            0xB5, 0x62, /* UBX Sync Chars */
            0x06, 0x01, /* CFG-MSG Class/ID */
            0x08, 0x00, /* Payload length */
            0x01, 0x20, 0x00, 0x01, 0x01, 0x00, 0x00,
            0x00, /* Enable NAV-TIMEGPS output on serial */
            0x32, 0x94,
        ];

        let pkt = UbxPkt::from_bytes(&buf);
        assert!(pkt.is_ok());
        if let Ok(pkt) = pkt {
            assert_eq!(pkt.cls, 0x06);
            assert_eq!(pkt.id, 0x01);
        }
    }

    struct ArrayBuf<const N: usize> {
        buf: [u8; N],
        size: usize,
    }

    impl<const N: usize> ArrayBuf<N> {
        fn new() -> Self {
            ArrayBuf {
                buf: [0_u8; N],
                size: 0,
            }
        }
    }

    impl<const N: usize> Write for ArrayBuf<N> {
        fn write(&mut self, buf: &[u8]) -> io::Result<usize> {
            let num_bytes = buf.len().min(N - self.size);
            for b in &buf[..num_bytes] {
                self.buf[self.size] = *b;
                self.size += 1;
            }
            Ok(num_bytes)
        }
    }

    #[test]
    fn test_write() {
        let payload = [0x01, 0x20, 0x00, 0x01, 0x01, 0x00, 0x00, 0x00];
        let pkt = UbxPkt::new(0x06, 0x01, &payload);

        let mut buf = ArrayBuf::<32>::new();

        assert!(pkt.write(&mut buf).is_ok());

        let expected_raw = [
            0xB5, 0x62, /* UBX Sync Chars */
            0x06, 0x01, /* CFG-MSG Class/ID */
            0x08, 0x00, /* Payload length */
            0x01, 0x20, 0x00, 0x01, 0x01, 0x00, 0x00,
            0x00, /* Enable NAV-TIMEGPS output on serial */
            0x32, 0x94,
        ];

        assert_eq!(&expected_raw, &buf.buf[..expected_raw.len()]);
    }

    #[test]
    fn test_builder_init() {
        let mut builder = Builder::<32>::new();
        assert_eq!(0, builder.state); // start at initial state
    }

    #[test]
    fn test_builder_sync1() {
        let mut builder = Builder::<32>::new();

        builder.push(0x03); // ignore non-sync word
        assert_eq!(0, builder.state);

        builder.push(0xB5);
        assert_eq!(1, builder.state); // sync word advances state

        builder.push(0x04);
        assert_eq!(0, builder.state); // non-sync word 2 resets state
    }

    #[test]
    fn test_builder() {
        let mut builder = Builder::<32>::new();

        for b in [
            0xB5, 0x62, 0x06, 0x01, 0x08, 0x00, 0x01, 0x20, 0x00, 0x01, 0x01, 0x00, 0x00, 0x00,
            0x32,
        ] {
            assert!(builder.push(b).is_none());
        }
        let pkt = builder.push(0x94);
        assert!(pkt.is_some());
        if let Some(pkt) = pkt {
            assert_eq!(0x06, pkt.cls);
            assert_eq!(0x01, pkt.id);
            assert_eq!(
                [0x01, 0x20, 0x00, 0x01, 0x01, 0x00, 0x00, 0x00],
                pkt.payload
            );
        }
        assert_eq!(0, builder.state);
    }

    #[test]
    fn test_cfg_pps() {
        let pkt = UbxPkt {
            cls: 0x06,
            id: 0x31,
            payload: &[
                0, 1, 0, 0, 50, 0, 0, 0, 64, 66, 15, 0, 64, 66, 15, 0, 0, 0, 0, 0, 160, 134, 1, 0,
                0, 0, 0, 0, 119, 0, 0, 0,
            ],
        };
        let pkt_cfg_pps = UbxCfgTP5v1::try_from(&pkt).unwrap();
        assert_eq!(1, pkt_cfg_pps.version());
        assert_eq!(50, pkt_cfg_pps.ant_cable_delay());
        assert_eq!(1_000_000, pkt_cfg_pps.freq_period());
        assert_eq!(1_000_000, pkt_cfg_pps.freq_period_lock());
        assert_eq!(0, pkt_cfg_pps.pulse_len_ratio());
        assert_eq!(100_000, pkt_cfg_pps.pulse_len_ratio_lock());
        assert_eq!(119, pkt_cfg_pps.flags());
    }

    #[test]
    fn test_tp() {
        let pkt = UbxPkt {
            cls: 0x0D,
            id: 0x01,
            payload: &[80, 137, 253, 15, 0, 0, 0, 0, 81, 28, 0, 0, 164, 8, 11, 63],
        };
        let pkt_tp = UbxTimTp::try_from(&pkt).unwrap();
        assert_eq!(2212, pkt_tp.week());
        assert_eq!(268274000, pkt_tp.tow_ms());
        assert_eq!(0, pkt_tp.tow_sub_ms());

        assert_eq!(11, pkt_tp.flags());
        assert!(!pkt_tp.flag_timebase_is_gnss());
        assert_eq!(63, pkt_tp.refinfo());

        assert_eq!(Ok(GnassTimebase::Unknown), pkt_tp.ref_timebase_gnss());
    }

    #[cfg(feature = "std")]
    mod std {

        use super::*;
        pub struct MyString {
            str: String,
        }

        impl MyString {
            pub fn new() -> Self {
                MyString { str: String::new() }
            }
        }

        impl ufmt::uWrite for MyString {
            type Error = &'static str;

            fn write_str(&mut self, s: &str) -> Result<(), Self::Error> {
                self.str.push_str(s);
                Ok(())
            }
        }

        #[test]
        fn test_ubx_nav_pvt() {
            let ubx = UbxPkt {
                cls: 0x01,
                id: 0x07,
                payload: &[
                    184, 141, 14, 1, 230, 7, 4, 10, 4, 55, 13, 55, 12, 0, 0, 0, 238, 64, 4, 0, 3,
                    1, 10, 11, 7, 85, 114, 213, 65, 152, 91, 25, 46, 237, 0, 0, 125, 110, 1, 0,
                    119, 21, 0, 0, 241, 23, 0, 0, 26, 0, 0, 0, 248, 255, 255, 255, 240, 255, 255,
                    255, 27, 0, 0, 0, 205, 54, 239, 1, 206, 1, 0, 0, 37, 92, 117, 0, 176, 0, 0, 0,
                    224, 74, 35, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                ],
            };

            let pvt = UbxNavPVT::try_from(&ubx);
            assert!(pvt.is_ok());

            let mut buf = MyString::new();
            let mut formatter = Formatter::new(&mut buf);
            if let Ok(pvt) = pvt {
                let time: &dyn UTCTime = &pvt;
                uwrite!(&mut formatter, "{}", time);
            }
        }
    }
}
