

LOCAL_CPP_FILES = \
	$(sort $(strip \
	$(filter %.cpp,$(SRC_FILES)) \
	$(filter %.cxx,$(SRC_FILES)) \
	$(filter %.cc,$(SRC_FILES)))) \

LOCAL_C_FILES = \
	$(sort $(strip $(filter %.c,$(SRC_FILES))))

LOCAL_AS_FILES = \
	$(sort $(strip $(filter %.s,$(SRC_FILES))))

LOCAL_RUST_FILES = \
	$(sort $(strip $(filter %.rs,$(SRC_FILES))))

OBJ = $(LOCAL_BUILD_DIR)/$(addsuffix .o,$(notdir $(SRC)))
DEP = $(LOCAL_BUILD_DIR)/$(addsuffix .d,$(notdir $(SRC)))

define include_flags
$(addprefix -I,$(INCLUDES))
endef

define link_dependencies
$(foreach mod,$(LOCAL_LINK_DEPEND),$$($(mod)_ARTIFACT))
endef

define compile_rust
$(eval LOCAL_OBJS += $(OBJ))
$(OBJ): $(SRC) | $(LOCAL_BUILD_DIR)
	@echo "building $(OBJ)"
	rustc --emit=obj -g --target=thumbv7em-none-eabihf -C panic=abort -o $(OBJ) $(SRC)
endef

define compile_c
$(eval LOCAL_OBJS += $(OBJ))
$(OBJ): $(SRC) $(LOCAL_COMPILE_DEPEND) | $(LOCAL_BUILD_DIR)
	@echo "building $(OBJ)"
	$(CC) -c $(LOCAL_C_FLAGS) -o $(OBJ) $(SRC)

-include $(DEP)
endef

define compile_cpp
$(eval LOCAL_OBJS += $(OBJ))
$(OBJ): $(SRC) $(LOCAL_COMPILE_DEPEND) | $(LOCAL_BUILD_DIR)
	@echo "building $(OBJ)"
	$(CXX) -c $(LOCAL_CXX_FLAGS) -o $(OBJ) $(SRC)

-include $(DEP)
endef


define compile_as
$(eval LOCAL_OBJS += $(OBJ))
$(OBJ): $(SRC) $(LOCAL_COMPILE_DEPEND) | $(LOCAL_BUILD_DIR)
	@echo "building $(OBJ)"
	$(AS) -c $(LOCAL_AS_FLAGS) -o $(OBJ) $(SRC)

-include $(DEP)
endef

define link
$(LOCAL_ARTIFACT): $(LOCAL_OBJS) $(link_dependencies)
	$(LOCAL_LD) $(LOCAL_LD_FLAGS) -o $(LOCAL_ARTIFACT) $(LOCAL_OBJS) $(link_dependencies)
	$(SZ) $(LOCAL_ARTIFACT)
endef

define archive
$(LOCAL_ARTIFACT): $(LOCAL_OBJS) $(link_dependencies)
	$(AR) rcs $(LOCAL_ARTIFACT) $(LOCAL_OBJS)

endef

define binary_output
$(LOCAL_BUILD_DIR)/$(MODULE).hex: $(LOCAL_ARTIFACT) | $(LOCAL_BUILD_DIR)
	$(HEX) $(LOCAL_ARTIFACT) $(LOCAL_BUILD_DIR)/$(MODULE).hex
	
$(LOCAL_BUILD_DIR)/$(MODULE).bin: $(LOCAL_ARTIFACT) | $(LOCAL_BUILD_DIR)
	$(BIN) $(LOCAL_ARTIFACT) $(LOCAL_BUILD_DIR)/$(MODULE).bin

endef

define compile
$(value LOCAL_BUILD_DIR):
	mkdir -p $(value LOCAL_BUILD_DIR)

$(foreach SRC,$(LOCAL_AS_FILES),$(call compile_as))
$(foreach SRC,$(LOCAL_C_FILES),$(call compile_c))
$(foreach SRC,$(LOCAL_CPP_FILES),$(call compile_cpp))
$(foreach SRC,$(LOCAL_RUST_FILES),$(call compile_rust))
endef

define newmodule
MODULE :=
SRC_FILES :=
INCLUDES :=
LINK_DEPEND :=
COMPILE_DEPEND :=
LOCAL_OBJS := 
endef

define module
ALL_MODULES += $(MODULE)
LOCAL_C_FLAGS := $(include_flags) -MMD $(C_FLAGS)
LOCAL_CXX_FLAGS := $(include_flags) -MMD $(CXX_FLAGS)
LOCAL_AS_FLAGS := $(include_flags) -MMD $(AS_FLAGS)
LOCAL_LD ?= $(CXX)
LOCAL_LD_FLAGS := $(LD_FLAGS)
LOCAL_SRCS := $(LOCAL_C_FILES) $(LOCAL_CPP_FILES)
FORMAT_FILES += $(LOCAL_C_FILES) $(LOCAL_CPP_FILES)
LOCAL_BUILD_DIR := $(BUILD_DIR)/$(TARGET)/$(MODULE)
LOCAL_LINK_DEPEND := $(LINK_DEPEND)
LOCAL_COMPILE_DEPEND := $(COMPILE_DEPEND)

endef

define extern_staticlib
$(eval $(call module))
$(eval LOCAL_ARTIFACT := $(SRC_FILES))
$(eval $(MODULE)_ARTIFACT := $(LOCAL_ARTIFACT))
endef

define build_staticlib
$(eval $(call module))
$(eval $(MODULE)_TYPE := staticlib)
$(eval LOCAL_ARTIFACT := $(LOCAL_BUILD_DIR)/$(MODULE).a)
$(eval ALL_LIBS += $(LOCAL_ARTIFACT))
$(eval $(MODULE)_ARTIFACT := $(LOCAL_ARTIFACT))
$(eval $(call compile))
$(eval $(call archive))
endef

define build_exe
$(eval $(call module))
$(eval $(MODULE)_TYPE := exe)
$(eval LOCAL_ARTIFACT := $(LOCAL_BUILD_DIR)/$(MODULE).elf)
$(eval ALL_EXE += $(LOCAL_ARTIFACT))
$(eval $(MODULE)_ARTIFACT := $(LOCAL_ARTIFACT))
$(eval $(MODULE): $(LOCAL_ARTIFACT))
$(call compile)
$(call link)
$(call binary_output)

endef

define build_sharedlib
$(eval $(MODULE)_TYPE := sharedlib)
$(eval $(call module))
$(eval LOCAL_C_FLAGS += -fpic)
$(eval LOCAL_CXX_FLAGS += -fpic)
$(eval LOCAL_LD_FLAGS += -shared)
$(eval LOCAL_ARTIFACT := $(LOCAL_BUILD_DIR)/$(MODULE).so)
$(eval ALL_LIBS += $(LOCAL_ARTIFACT))
$(eval $(MODULE)_ARTIFACT := $(LOCAL_ARTIFACT))
$(eval $(MODULE): $(LOCAL_ARTIFACT))
$(call compile)
$(call link)
endef

