#pragma once

#define META_START          0x08004000
#define APP_IMAGE_HEADER    0x08004100
#define APP_IMAGE_START     0x08004200

#define APP_MAX_SIZE (((128-16) * 1024) - 256)
