// Copyright (C) NLighten Systems, LLC (Paul Soucy) 2022
//
// This file is part of Kompressor Firmware.
//
// Kompressor Firmware is free software: you can redistribute it and/or modify it under the terms of
// the GNU General Public License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any later version.
//
// Kompressor Firmware is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with Foobar. If not, see
// <https://www.gnu.org/licenses/>.

use core::{
    alloc::{AllocError, Layout},
    ops::Deref,
    ptr,
};

use alloc::boxed::Box;
use kompressor_common::prelude::*;
use kompressor_common::{
    concentrator::{Frame, StreamData},
    Error,
};
use log::debug;

use super::StreamAsyncWriter;

use rscore::{
    io::AsyncWriter,
    lorapkt::{Context, LoRaPacket, LoraPacketRep},
};

pub struct LoRaPacketOwned {
    buf: Box<[u8]>,
    ctx: Context,
}

impl LoRaPacketOwned {
    pub fn try_new(pkt: &LoRaPacket, ctx: &Context) -> Result<Self, AllocError> {
        let slice = pkt.raw_buf();
        let len = slice.len();
        let layout = match Layout::array::<u8>(len) {
            Ok(l) => l,
            Err(_) => return Err(AllocError),
        };

        unsafe {
            let ptr = alloc::alloc::alloc(layout);
            ptr::copy_nonoverlapping(slice.as_ptr(), ptr, len);
            let slice_ptr = core::ptr::slice_from_raw_parts_mut(ptr, len);
            Ok(LoRaPacketOwned {
                buf: Box::from_raw(slice_ptr),
                ctx: ctx.clone(),
            })
        }
    }

    pub fn try_new_box(pkt: &LoRaPacket, ctx: &Context) -> Result<Box<Self>, AllocError> {
        Ok(Box::try_new(Self::try_new(pkt, ctx)?)?)
    }
}

impl LoRaPacketOwned {
    pub fn pkt(&self) -> LoRaPacket {
        LoRaPacket::from(self.buf.as_ref())
    }

    pub fn pkt_rep(&self) -> LoraPacketRep {
        LoraPacketRep {
            pkt: self.pkt(),
            ctx: &self.ctx,
        }
    }
}

#[derive(Debug, Clone, Copy)]
pub struct SignPktReq {
    pub id: u32,
    pub hash: [u8; Sha512::SHA512_HASH_SIZE],
}

impl SignPktReq {
    pub fn new(pkt: &LoraPacketRep) -> Self {
        use kompressor_common::lora::RxPktRep;
        Self {
            id: pkt.pkt.key(),
            hash: pkt.as_hash(),
        }
    }

    pub fn try_new_box(pkt: &LoraPacketRep) -> Result<Box<Self>, AllocError> {
        Ok(Box::try_new(Self::new(pkt))?)
    }
}

#[derive(Debug, Clone, Copy)]
pub struct SignPktRes {
    pub id: u32,
    pub sig: Signature,
}

struct ArrayWriter<const N: usize> {
    buf: [u8; N],
    i: usize,
}

impl<const N: usize> ArrayWriter<N> {
    pub const fn new() -> Self {
        Self {
            buf: [0_u8; N],
            i: 0,
        }
        //Self { i: 0}
    }

    fn buf(&self) -> &[u8] {
        &self.buf[..self.i]
        //unsafe { &ARRAY[..self.i] }
    }

    fn clear(&mut self) {
        self.i = 0;
    }
}

impl<const N: usize> serde_cbor::ser::Write for ArrayWriter<N> {
    type Error = serde_cbor::Error;

    fn write_all(&mut self, buf: &[u8]) -> Result<(), Self::Error> {
        //unsafe {
        if self.i + buf.len() >= N {
            //if self.i + ARRAY.len() >= N {
            return Err(serde_cbor::Error::message(""));
        }

        for b in buf {
            self.buf[self.i] = *b;
            //ARRAY[self.i] = *b;
            self.i += 1;
        }

        Ok(())
        //}
    }
}

//static mut ARRAY: [u8 ; 306] = [0_u8 ; 306];

fn serialize<const N: usize>(
    pkt_rep: &LoraPacketRep<'_>,
) -> Result<ArrayWriter<N>, serde_cbor::Error> {
    use serde::ser::SerializeMap;
    use serde::Serializer;

    let pkt_id = pkt_rep.pkt.key();

    let mut serializer = serde_cbor::Serializer::new(ArrayWriter::<N>::new());
    let mut out_map = serializer.serialize_map(Some(2))?;
    out_map.serialize_entry("key", &pkt_id);
    out_map.serialize_entry("pkt", &pkt_rep);
    Ok(serializer.into_inner())
}

pub async fn write_cbor<'a>(pkt_rep: &'a LoraPacketRep<'a>) -> Result<(), serde_cbor::Error> {
    //let mut buf = serialize(pkt_rep)?;

    //let mut writer = StreamAsyncWriter::new(0);
    //writer.write(serializer.into_inner().buf()).await;
    //writer.flush().await;

    let data = unsafe { crate::DATA.assume_init_mut() };

    let array = serialize::<306>(pkt_rep)?;
    let mut buf = array.buf();

    while buf.len() > 0 {
        let n = buf.len().min(15);
        crate::spi::host_send(
            data,
            &Frame::StreamData(StreamData {
                idx: 0,
                data: &buf[..n],
            }),
        )
        .await;
        buf = &buf[n..];
    }

    Ok(())
}

fn serialize_signpkt<const N: usize>(
    pkt_id: u32,
    sig: &Signature,
) -> Result<ArrayWriter<N>, serde_cbor::Error> {
    use serde::ser::SerializeMap;
    use serde::Serializer;

    let mut serializer = serde_cbor::Serializer::new(ArrayWriter::<N>::new());
    let mut out_map = serializer.serialize_map(Some(2))?;
    out_map.serialize_entry("key", &pkt_id);
    out_map.serialize_entry("sig", &sig[..]);
    Ok(serializer.into_inner())
}

pub async fn write_cbor_signpkt(pkt_id: u32, sig: &Signature) -> Result<(), serde_cbor::Error> {
    let data = unsafe { crate::DATA.assume_init_mut() };

    let array = serialize_signpkt::<306>(pkt_id, sig)?;
    let mut buf = array.buf();

    while buf.len() > 0 {
        let n = buf.len().min(15);
        crate::spi::host_send(
            data,
            &Frame::StreamData(StreamData {
                idx: 0,
                data: &buf[..n],
            }),
        )
        .await;
        buf = &buf[n..];
    }

    Ok(())
}
