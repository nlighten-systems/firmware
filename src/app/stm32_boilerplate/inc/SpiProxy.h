#pragma once

#include "stdint.h"
#include "stdbool.h"

typedef enum {
	SPI_CMD_W_REG_130x,
	SPI_CMD_R_REG_130x,
	SPI_CMD_RMW_REG_130x,
	SPI_CMD_R_REG_1250,
	SPI_CMD_W_REG_1250,
} spi_cmd_type_t;

typedef struct {
	uint16_t address;
	uint8_t value;
} spi_cmd_w_reg_130x_t;

typedef struct {
	uint16_t address;
	uint8_t value;
} spi_cmd_r_reg_130x_t;

typedef struct {
	uint16_t address;
	uint8_t value;
	uint8_t mask;
} spi_cmd_rmw_reg_130x_t;

typedef struct {
	uint8_t radio;
	uint8_t op_code;
	uint8_t buf[4];
	uint8_t buf_len;
} spi_cmd_w_reg_1250_t;

typedef struct {
	uint8_t radio;
	uint8_t op_code;
	uint8_t buf_len;
	uint8_t buf[4];
} spi_cmd_r_reg_1250_t;

typedef struct {
	spi_cmd_type_t type;

	union {
		spi_cmd_w_reg_130x_t w_reg_130x;
		spi_cmd_r_reg_130x_t r_reg_130x;
		spi_cmd_rmw_reg_130x_t rmw_reg_130x;
		spi_cmd_r_reg_1250_t r_reg_1250;
		spi_cmd_w_reg_1250_t w_reg_1250;
	};

} spi_cmd_t;

void processSPIMaster(spi_cmd_t* cmd, bool waitForComplete);


