// Copyright (C) NLighten Systems, LLC (Paul Soucy) 2022
//
// This file is part of Kompressor Firmware.
//
// Kompressor Firmware is free software: you can redistribute it and/or modify it under the terms of
// the GNU General Public License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any later version.
//
// Kompressor Firmware is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with Foobar. If not, see
// <https://www.gnu.org/licenses/>.

mod spi;

use freertos_rust::PhantomData;
pub use spi::*;

use core::{
    future::Future,
    pin::Pin,
    sync::atomic::{AtomicBool, Ordering},
    task::{Context, Poll, Waker},
};

use crate::{
    async_runtime,
    data::StaticData,
    hal::{hspi2, HAL_SPI_Abort_IT},
};

use futures::{pin_mut, FutureExt};

use kompressor_common::concentrator::{
    stream::{SPIFrame, Window},
    Frame,
};

use log::{error, info};

use super::TickTimer;

pub async fn host_send<'a>(data: &StaticData, m: &Frame<'a>) {
    let mut frame = SPIFrame::new();
    m.write_frame(&mut frame);
    data.host_tx.send(&frame).await;
}

static mut SPI_SLAVE_TX_RX: SpiSlaveTxRx = SpiSlaveTxRx {
    ready: AtomicBool::new(false),
    waker: None,
};

#[no_mangle]
pub extern "C" fn SpiSlaveTxRx_complete() {
    unsafe {
        SPI_SLAVE_TX_RX.ready.store(true, Ordering::Release);
        // if let Some(waker) = SPI_SLAVE_TX_RX.waker.take() {
        //     waker.wake()
        // }
    }
}

pub struct SpiSlaveTxRx {
    ready: AtomicBool,
    waker: Option<Waker>,
}

pub fn slave_tx_rx(tx: *const u8, rx: *mut u8) -> &'static mut SpiSlaveTxRx {
    unsafe {
        //HAL_SPI_TransmitReceive_DMA(&hspi2, tx, rx, SPIFrame::FRAME_SIZE as u16);
        SPI_SLAVE_TX_RX.ready.store(false, Ordering::Release);
        SpiSlaveTxRx(tx, rx);
        &mut SPI_SLAVE_TX_RX
    }
}

impl Future for SpiSlaveTxRx {
    type Output = ();

    fn poll(self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<Self::Output> {
        if self.ready.load(Ordering::Acquire) {
            Poll::Ready(())
        } else {
            //self.waker = Some(cx.waker().clone());
            cx.waker().wake_by_ref();
            Poll::Pending
        }
    }
}

const BUF_SIZE: usize = 32;

pub async fn run() {
    unsafe {
        spi_init();
    }
    //let mut tx_frame: SPIFrame = unsafe { MaybeUninit::uninit().assume_init() };
    //let mut rx_frame: SPIFrame = unsafe { MaybeUninit::uninit().assume_init() };

    //let mut tx_window: Window<SPIFrame, 1> = Window::new(0);
    //let mut rx_window: Window<SPIFrame, 1> = Window::new(0);

    //let mut tx_window = TxWindow::new();
    //let mut rx_window = RxWindow::new();

    loop {
        read_spi_host().await;

        async_runtime::r#yield().await;
    }
}

async fn read_spi_host() {
    let mut rx_frame = SPIFrame::new();

    let data = unsafe { crate::DATA.assume_init_mut() };

    if let Some(mut tx) = data.host_tx.try_take() {
        tx.set_checksum(tx.compute_checksum());
        unsafe {
            while spi::xQueueGenericSend(gSPI_HOST_TX, &tx as *const _ as *const _, 0, 0) == 0 {}
        }
    }

    unsafe {
        if spi::xQueueReceive(gSPI_HOST_RX, &mut rx_frame as *mut _ as *mut _, 0) > 0 {
            //info!("received pkt: {:?}", rx_frame);
            if rx_frame.get_type() > 0 && rx_frame.verify_checksum() {
                data.host_rx.send(&rx_frame).await;
            }
        }
    }

    //debug!("rx_frame: {:?}", rx_frame);
}

extern "C" {
    fn spi_init();
    //static mut gSpiSlaveTxBuf: *const u8;
    //static mut gSpiSlaveRxBuf: *mut u8;
    //static mut gSpiSlaveReady: u8;
    static mut gSpiSlaveRx: [u8; SPIFrame::FRAME_SIZE];
    static mut gSpiSlaveTx: [u8; SPIFrame::FRAME_SIZE];
    static mut gSPI_HOST_TX: *const cty::c_void;
    static mut gSPI_HOST_RX: *const cty::c_void;
    fn SpiSlaveTxRx(pTxData: *const u8, pRxData: *mut u8);
}
