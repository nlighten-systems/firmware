// Copyright (C) NLighten Systems, LLC (Paul Soucy) 2022
//
// This file is part of Kompressor Firmware.
//
// Kompressor Firmware is free software: you can redistribute it and/or modify it under the terms of
// the GNU General Public License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any later version.
//
// Kompressor Firmware is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with Foobar. If not, see
// <https://www.gnu.org/licenses/>.

use core::alloc::{GlobalAlloc, Layout};

use log::info;

extern "C" {
    pub fn pvPortMalloc(xWantedSize: u32) -> *mut cty::c_void;
    pub fn vPortFree(pv: *mut cty::c_void);
}

pub struct FreeRTOSAllocator;

unsafe impl GlobalAlloc for FreeRTOSAllocator {
    unsafe fn alloc(&self, layout: Layout) -> *mut u8 {
        let size = layout.size();
        //info!("alloc: {}", size);
        let res = pvPortMalloc(size as u32);
        return res as *mut u8;
    }

    unsafe fn dealloc(&self, ptr: *mut u8, _layout: Layout) {
        vPortFree(ptr as *mut cty::c_void)
    }
}
