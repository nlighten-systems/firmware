// Copyright (C) NLighten Systems, LLC (Paul Soucy) 2022
//
// This file is part of Kompressor Firmware.
//
// Kompressor Firmware is free software: you can redistribute it and/or modify it under the terms of
// the GNU General Public License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any later version.
//
// Kompressor Firmware is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with Foobar. If not, see
// <https://www.gnu.org/licenses/>.

#![feature(type_alias_impl_trait)]
#![feature(generic_associated_types)]
#![cfg_attr(not(any(feature = "std", test)), no_std)]

include!(concat!(env!("OUT_DIR"), "/bindings.rs"));

pub mod prelude {
    pub use kompressor_common::prelude::*;
}

pub mod ed25519 {
    pub use kompressor_common::crypto::ed25519::*;
}

pub use ufmt;

mod appimage;
pub mod checksum;
pub mod io;
pub mod lorapkt;
pub mod meta;
pub mod serialnum;
pub mod spi;
pub mod ublox;
pub mod utils;
pub mod xmodem;

pub use appimage::AppMetadata;
