use std::io::{self, prelude::*};
use std::io::{BufReader, BufWriter};

use lazy_static::lazy_static;
use regex::Regex;

fn main() {
    let mut input = io::stdin();
    let mut output = io::stdout();

    if let Err(_) = to_c_header(&mut input, &mut output) {
        std::process::exit(1);
    }
}

fn to_c_header<I, O>(input: &mut I, output: &mut O) -> io::Result<()>
where
    I: Read,
    O: Write,
{
    lazy_static! {
        static ref COMMENT: Regex = Regex::new(r"^\s*#").unwrap();
        static ref DEFINE: Regex = Regex::new(r"([A-Za-z_]+)=([A-Za-z_]+)").unwrap();
    }

    let mut writer = BufWriter::new(output);
    writeln!(writer, "/* Auto-generated config file */")?;

    for line in BufReader::new(input).lines() {
        let line = line?;
        if !COMMENT.is_match(&line) {
            if let Some(cap) = DEFINE.captures(&line) {
                let var = &cap[1];
                let _value = &cap[2];

                writeln!(writer, "#define {}", var)?;
            }
        }
    }

    Ok(())
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test() {
        let mut data = r#"
        #
        # Automatically generated file; DO NOT EDIT.
        # Configuration
        #
        CONFIG_COM_SPI=y
        # CONFIG_COM_USB is not set
        
        "#
        .as_bytes();

        let mut output = BufWriter::new(Vec::new());

        let r = to_c_header(&mut data, &mut output);
        assert!(r.is_ok());

        let output = String::from_utf8(output.into_inner().unwrap()).unwrap();
        let expected = "/* Auto-generated config file */\n#define CONFIG_COM_SPI\n";
        assert_eq!(expected, output);
    }
}
