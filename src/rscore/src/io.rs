// Copyright (C) NLighten Systems, LLC (Paul Soucy) 2022
//
// This file is part of Kompressor Firmware.
//
// Kompressor Firmware is free software: you can redistribute it and/or modify it under the terms of
// the GNU General Public License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any later version.
//
// Kompressor Firmware is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with Foobar. If not, see
// <https://www.gnu.org/licenses/>.

use core::future::Future;

pub enum ErrorKind {
    Unknown,
    Cancel,
    TimedOut,
}

pub struct Error {
    pub kind: ErrorKind,
}

impl From<ErrorKind> for Error {
    fn from(k: ErrorKind) -> Self {
        Error { kind: k }
    }
}

pub type Result<T> = core::result::Result<T, Error>;

pub trait Read {
    fn read(&mut self, buf: &mut [u8]) -> Result<usize>;
    fn read_exact(&mut self, buf: &mut [u8]) -> Result<()> {
        default_read_exact(self, buf)
    }
}

pub(crate) fn default_read_exact<R>(this: &mut R, mut buf: &mut [u8]) -> Result<()>
where
    R: Read + ?Sized,
{
    while !buf.is_empty() {
        match this.read(buf) {
            Ok(0) => break,
            Ok(n) => {
                //let tmp = buf;
                //buf = &mut tmp[n..];
                buf = &mut buf[n..];
            }
            Err(e) => return Err(e),
        }
    }
    Ok(())
}

pub trait Write {
    fn write(&mut self, buf: &[u8]) -> Result<usize>;
    fn write_all(&mut self, buf: &[u8]) -> Result<()> {
        default_write_all(self, buf)
    }
}

pub(crate) fn default_write_all<W>(this: &mut W, mut buf: &[u8]) -> Result<()>
where
    W: Write + ?Sized,
{
    while !buf.is_empty() {
        match this.write(buf) {
            Ok(0) => break,
            Ok(n) => {
                buf = &buf[n..];
            }
            Err(e) => return Err(e),
        }
    }
    Ok(())
}

pub trait AsyncWriter {
    type Output<'b>: Future<Output = Result<()>>
    where
        Self: 'b;

    fn write<'a>(&'a mut self, buf: &'a [u8]) -> Self::Output<'a>;
}
