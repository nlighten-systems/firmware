// Copyright (C) NLighten Systems, LLC (Paul Soucy) 2022
//
// This file is part of Kompressor Firmware.
//
// Kompressor Firmware is free software: you can redistribute it and/or modify it under the terms of
// the GNU General Public License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any later version.
//
// Kompressor Firmware is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with Foobar. If not, see
// <https://www.gnu.org/licenses/>.

use rscore::xmodem::XModem;

use core::fmt::Write;

const FLASH_TYPEPROGRAM_BYTE: u32 = 0x00000000;
const FLASH_TYPEPROGRAM_WORD: u32 = 0x02;

const FLASH_TYPEERASE_PAGES: u32 = 0x00000000;

const PAGE_SIZE: u32 = 256;

#[allow(non_snake_case)]
#[repr(C)]
struct FLASH_EraseInitTypeDef {
    TypeErase: u32,
    PageAddress: u32,
    NbPages: u32,
}

extern "C" {
    fn HAL_FLASH_Unlock() -> u32;
    fn HAL_FLASH_Lock() -> u32;
    fn HAL_FLASHEx_Erase(pEraseInit: *const FLASH_EraseInitTypeDef, sectorError: *mut u32) -> u32;
    fn HAL_FLASH_Program(typeprog: u32, address: u32, data: u32) -> u32;

}
struct FlashWriter {
    address: u32,
}

impl FlashWriter {
    fn new() -> Self {
        unsafe {
            HAL_FLASH_Unlock();
            FlashWriter {
                address: crate::APP_IMAGE_HEADER,
            }
        }
    }

    fn erase_page(&self) -> core::result::Result<(), u32> {
        let erase_op = FLASH_EraseInitTypeDef {
            TypeErase: FLASH_TYPEERASE_PAGES,
            PageAddress: self.address,
            NbPages: 1,
        };

        let mut pageError = 0;
        unsafe {
            let ret = HAL_FLASHEx_Erase(&erase_op, &mut pageError);
            if ret == 0 {
                Ok(())
            } else {
                Err(ret)
            }
        }
    }
}

impl Drop for FlashWriter {
    fn drop(&mut self) {
        unsafe {
            HAL_FLASH_Lock();
        }
    }
}

impl rscore::io::Write for FlashWriter {
    fn write(&mut self, buf: &[u8]) -> rscore::io::Result<usize> {
        use core::ptr;

        if self.address % PAGE_SIZE == 0 {
            if let Err(e) = self.erase_page() {
                return Err(rscore::io::Error::from(rscore::io::ErrorKind::Unknown));
            }
        }

        unsafe {
            let num_bytes = buf.len().min(4);
            let mut word = ptr::read_volatile(self.address as *const u32);
            let word_ptr = ptr::addr_of_mut!(word) as *mut u8;

            for i in 0..num_bytes {
                *(word_ptr.add(i)) = buf[i];
            }

            HAL_FLASH_Program(FLASH_TYPEPROGRAM_WORD, self.address, word);

            self.address += num_bytes as u32;
            Ok(num_bytes)
        }
    }
}

pub fn download_app() {
    let _ = crate::SERIAL.write_str("Ready for XModem transfer\r\n");

    let mut out = FlashWriter::new();
    let mut xmodem = XModem::new();
    match xmodem.receve(&mut crate::SERIAL, &mut out) {
        Ok(()) => {}
        Err(_e) => {}
    }
}
