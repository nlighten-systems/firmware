// Copyright (C) NLighten Systems, LLC (Paul Soucy) 2022
//
// This file is part of Kompressor Firmware.
//
// Kompressor Firmware is free software: you can redistribute it and/or modify it under the terms of
// the GNU General Public License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any later version.
//
// Kompressor Firmware is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with Foobar. If not, see
// <https://www.gnu.org/licenses/>.

#![allow(improper_ctypes)]
#![allow(dead_code)]

#[repr(C)]
pub struct OpaqeStruct {
    var: i32,
}

pub type UART_HandleTypeDef = OpaqeStruct;
pub type SPI_HandleTypeDef = OpaqeStruct;

pub const HAL_MAX_DELAY: u32 = 0xFFFFFFFF;

extern "C" {

    pub fn HAL_UART_Receive(
        handle: *const UART_HandleTypeDef,
        buf: *mut u8,
        size: u16,
        timeout: u32,
    ) -> cty::c_uint;

    pub fn HAL_UART_Receive_IT(
        handle: *const UART_HandleTypeDef,
        buf: *mut u8,
        size: u16,
    ) -> cty::c_uint;

    pub fn HAL_UART_Transmit(
        handle: *const UART_HandleTypeDef,
        buf: *const u8,
        size: u16,
        timeout: u32,
    ) -> cty::c_uint;

    pub fn HAL_UART_Transmit_IT(
        handle: *const UART_HandleTypeDef,
        pData: *const u8,
        size: u16,
    ) -> cty::c_uint;

    pub fn HAL_SPI_Transmit(
        hspi: *const SPI_HandleTypeDef,
        pData: *const u8,
        size: u16,
        timeout: u32,
    ) -> cty::c_uint;

    pub fn HAL_SPI_Receive(
        hspi: *const SPI_HandleTypeDef,
        pData: *mut u8,
        size: u16,
        timeout: u32,
    ) -> cty::c_uint;

    pub fn HAL_SPI_TransmitReceive_DMA(
        hspi: *const SPI_HandleTypeDef,
        pTxData: *const u8,
        pRxData: *mut u8,
        size: u16,
    ) -> cty::c_uint;

    pub fn HAL_SPI_Transmit_DMA(
        hspi: *const SPI_HandleTypeDef,
        pData: *const u8,
        size: u16,
    ) -> cty::c_uint;

    pub fn HAL_SPI_Receive_DMA(
        hspi: *const SPI_HandleTypeDef,
        pData: *mut u8,
        size: u16,
    ) -> cty::c_uint;

    pub fn HAL_SPI_Abort(hspi: *const SPI_HandleTypeDef);

    pub fn HAL_SPI_Abort_IT(hspi: *const SPI_HandleTypeDef);

    pub static huart1: UART_HandleTypeDef;
    pub static huart2: UART_HandleTypeDef;
    pub static hspi1: SPI_HandleTypeDef;
    pub static hspi2: SPI_HandleTypeDef;
}

pub struct Stm32UART {
    pub handle: *const UART_HandleTypeDef,
}

use core::fmt;

use rscore::io::Write;

impl rscore::io::Write for Stm32UART {
    fn write(&mut self, buf: &[u8]) -> rscore::io::Result<usize> {
        unsafe {
            if HAL_UART_Transmit(self.handle, buf.as_ptr(), buf.len() as u16, 100) == 0 {
                Ok(buf.len())
            } else {
                Err(rscore::io::Error::from(rscore::io::ErrorKind::Unknown))
            }
        }
    }
}

impl core::fmt::Write for Stm32UART {
    fn write_str(&mut self, s: &str) -> fmt::Result {
        let _ = self.write_all(s.as_bytes());
        Ok(())
    }
}
