// Copyright (C) NLighten Systems, LLC (Paul Soucy) 2022
//
// This file is part of Kompressor Firmware.
//
// Kompressor Firmware is free software: you can redistribute it and/or modify it under the terms of
// the GNU General Public License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any later version.
//
// Kompressor Firmware is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with Foobar. If not, see
// <https://www.gnu.org/licenses/>.

use core::fmt::Display;

use ufmt::{uDisplay, uWrite, uwrite};

use crate::io;

pub trait UTCTime {
    fn year(&self) -> u16;
    fn month(&self) -> u8;
    fn day(&self) -> u8;
    fn hour(&self) -> u8;
    fn minute(&self) -> u8;
    fn second(&self) -> u8;
    fn nano(&self) -> i32;
}

impl uDisplay for &'_ dyn UTCTime {
    fn fmt<W>(&self, formatter: &mut ufmt::Formatter<'_, W>) -> Result<(), W::Error>
    where
        W: ufmt::uWrite + ?Sized,
    {
        uwrite!(
            formatter,
            "{}-{}-{}T{}:{}:{}",
            self.year(),
            NumberFormat::decimal(self.month().into(), 2),
            NumberFormat::decimal(self.day().into(), 2),
            NumberFormat::decimal(self.hour().into(), 2),
            NumberFormat::decimal(self.minute().into(), 2),
            NumberFormat::decimal(self.second().into(), 2)
        )
    }
}

impl Display for &dyn UTCTime {
    fn fmt(&self, f: &mut core::fmt::Formatter<'_>) -> core::fmt::Result {
        write!(
            f,
            "{}-{:02}-{:02}T{:02}:{:02}:{:02}",
            self.year(),
            self.month(),
            self.day(),
            self.hour(),
            self.minute(),
            self.second()
        )
    }
}

#[cfg(feature = "serde")]
impl serde::Serialize for dyn UTCTime {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: serde::Serializer,
    {
        let mut buf = [0_u8; 24];
        let mut fmt = FixedString::new(&mut buf);
        let _ = uwrite!(&mut fmt, "{}", self);
        serializer.serialize_str(fmt.to_str())
    }
}

#[derive(Default, Clone)]
pub struct Time {
    pub year: u16,
    pub month: u8,
    pub day: u8,
    pub hour: u8,
    pub minute: u8,
    pub second: u8,
}

impl Time {
    pub fn from_utc_time(t: &impl UTCTime) -> Self {
        Self {
            year: t.year(),
            month: t.month(),
            day: t.day(),
            hour: t.hour(),
            minute: t.minute(),
            second: t.second(),
        }
    }
}

impl UTCTime for Time {
    fn year(&self) -> u16 {
        self.year
    }

    fn month(&self) -> u8 {
        self.month
    }

    fn day(&self) -> u8 {
        self.day
    }

    fn hour(&self) -> u8 {
        self.hour
    }

    fn minute(&self) -> u8 {
        self.minute
    }

    fn second(&self) -> u8 {
        self.second
    }

    fn nano(&self) -> i32 {
        0
    }
}

pub struct FixedString<'a> {
    buf: &'a mut [u8],
    i: usize,
}

impl<'a> FixedString<'a> {
    pub fn new(buf: &'a mut [u8]) -> Self {
        Self { buf: buf, i: 0 }
    }

    pub fn to_str(self) -> &'a str {
        unsafe { core::str::from_utf8_unchecked(&self.buf[..self.i]) }
    }
}

impl<'a> uWrite for FixedString<'a> {
    type Error = io::Error;

    fn write_str(&mut self, s: &str) -> Result<(), Self::Error> {
        let src = s.as_bytes();
        for i in 0..src.len() {
            self.buf[self.i] = src[i];
            self.i += 1;
        }
        Ok(())
    }
}

struct NumberFormat {
    num: i32,
    base: u8,
    padding: u8,
}

impl NumberFormat {
    pub fn decimal(num: i32, pad: u8) -> Self {
        NumberFormat {
            num: num,
            base: 10,
            padding: pad,
        }
    }
}

impl uDisplay for NumberFormat {
    fn fmt<W>(&self, formatter: &mut ufmt::Formatter<'_, W>) -> Result<(), W::Error>
    where
        W: ufmt::uWrite + ?Sized,
    {
        const REP: &[u8; 16] = b"0123456789ABCDEF";
        const N: usize = 10;
        let mut buf = [b'0'; N];

        let mut i = N as i32 - 1;
        let mut num: u32 = if self.num.is_negative() {
            formatter.write_char('-')?;
            (-self.num) as u32
        } else {
            self.num as u32
        };

        loop {
            let r: usize = num as usize % self.base as usize;
            buf[i as usize] = REP[r];
            let num_digits = N as u8 - i as u8;

            num = num / self.base as u32;

            if num_digits == self.padding || i == 0 {
                break;
            } else {
                i -= 1;
            }
        }

        unsafe { formatter.write_str(core::str::from_utf8_unchecked(&buf[i as usize..])) }
    }
}

#[cfg(test)]
mod test {
    use ufmt::Formatter;

    use super::*;

    #[test]
    fn test_utc() {}

    #[test]
    fn test_numberformat() {
        let num = NumberFormat {
            num: 26,
            base: 10,
            padding: 4,
        };

        let mut buf = [0_u8; 10];
        let mut str = FixedString::new(&mut buf);

        let mut fmt = Formatter::new(&mut str);

        assert!(uwrite!(&mut fmt, "{}", &num).is_ok());

        assert_eq!(str.to_str(), "0026");
    }

    #[test]
    fn test_numberformat2() {
        let num = NumberFormat {
            num: 13,
            base: 10,
            padding: 2,
        };

        let mut buf = [0_u8; 10];
        let mut str = FixedString::new(&mut buf);

        let mut fmt = Formatter::new(&mut str);

        assert!(uwrite!(&mut fmt, "{}", &num).is_ok());

        assert_eq!(str.to_str(), "13");
    }
}
