// Copyright (C) NLighten Systems, LLC (Paul Soucy) 2022
//
// This file is part of Kompressor Firmware.
//
// Kompressor Firmware is free software: you can redistribute it and/or modify it under the terms of
// the GNU General Public License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any later version.
//
// Kompressor Firmware is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with Foobar. If not, see
// <https://www.gnu.org/licenses/>.

use rand::prelude::*;

use rscore::prelude::*;
use rscore::serialnum::SerialNum;
use rscore::{ed25519::*, AppMetadata};

use clap::{AppSettings, Parser, Subcommand};

use std::fs::File;
use std::io::prelude::*;
use std::path::{Path, PathBuf};

use bytes::BufMut;

const META_SIZE: usize = 256;
const BOOTLOAD_SIZE: usize = 16 * 1024;

/// Utility for generating keypairs and signing app image
#[derive(Parser)]
#[clap(author, version, about, long_about = None)]
#[clap(global_setting(AppSettings::PropagateVersion))]
#[clap(global_setting(AppSettings::UseLongFormatForHelpSubcommand))]
struct Cli {
    #[clap(subcommand)]
    command: Commands,
}

#[derive(Subcommand)]
enum Commands {
    /// Generate a new random EC25519 keypair
    GenKey {
        /// Name of the keypair
        #[clap(short, long)]
        name: String,
    },

    /// Sign the app image
    SignApp {
        /// path to app image
        #[clap(short, long, parse(from_os_str), value_name = "app.bin")]
        image: PathBuf,

        /// private key file
        #[clap(short, long, parse(from_os_str), value_name = "key.priv")]
        key: PathBuf,
    },

    /// Add metadata to bootloader
    Meta {
        /// path to bootloader
        #[clap(short, long, parse(from_os_str), value_name = "bootloader.bin")]
        bootloader: PathBuf,

        /// keypair name
        #[clap(short, long)]
        name: String,

        #[clap(long)]
        serial: u32,
    },
}

fn generate_random_keypair(name: &String) -> Result<(), std::io::Error> {
    use std::ops::Deref;

    let mut rng = StdRng::from_entropy();
    let seed: [u8; 32] = rng.gen();
    let keypair = ed25519_keygen(seed);

    let mut pub_key_file = File::create(format!("{}.pub", name))?;
    pub_key_file.write_all(&keypair.public.deref()[..])?;

    let mut priv_key_file = File::create(format!("{}.priv", name))?;
    priv_key_file.write_all(&keypair.private.deref()[..])?;

    Ok(())
}

fn sign_app_image(app_image_path: &Path, priv_key_path: &Path) -> Result<(), std::io::Error> {
    let mut priv_key_file = File::open(priv_key_path)?;
    let mut private_key = [0_u8; ED25519_PRIVATE_KEY_SIZE];
    priv_key_file.read_exact(&mut private_key)?;

    let mut app_image = Vec::<u8>::new();
    let mut app_image_file = File::open(app_image_path)?;
    app_image_file.read_to_end(&mut app_image)?;

    let app_size = app_image.len() as u32;
    let hardware_id = 0_u16;

    let mut hasher = Sha512::new();
    hasher.write(b"firmware");
    hasher.write(&app_size.to_be_bytes());
    hasher.write(&hardware_id.to_be_bytes());
    hasher.write(&app_image);
    let hash = hasher.finish();

    let private_key = PrivateKey::new(&private_key);
    let sig = private_key.sign(&hash);

    let mut out = File::create("app_image_signed.bin")?;

    let metadata = AppMetadata {
        size: app_size,
        hardware: hardware_id,
        signature: &sig,
    };

    metadata.write(&mut out)?;
    out.write_all(&app_image)?;

    Ok(())
}

fn embed_meta(
    bootloader_path: &Path,
    key_name: &String,
    serial: SerialNum,
) -> Result<(), std::io::Error> {
    const IMAGE_SIZE: usize = BOOTLOAD_SIZE + META_SIZE;
    let mut bootloader_file = File::open(bootloader_path)?;
    let mut bootloader_image = bytes::BytesMut::with_capacity(IMAGE_SIZE);
    for _ in 0..IMAGE_SIZE {
        bootloader_image.put_u8(0xFF);
    }

    let mut i = 0;
    while i < bootloader_image.capacity() {
        match bootloader_file.read(&mut bootloader_image[i..]) {
            Ok(n) => {
                if n == 0 {
                    break;
                }
                i += n;
            }
            Err(e) => {
                return Err(e);
            }
        }
    }

    let mut meta_ptr = &mut bootloader_image[BOOTLOAD_SIZE..];
    meta_ptr.put_slice(&create_metadata(key_name, serial)?);

    let mut out = File::create(format!("{}_bootloader.bin", key_name))?;
    out.write_all(&bootloader_image)?;

    Ok(())
}

fn create_metadata<S: AsRef<str>>(
    key_name: S,
    serial: SerialNum,
) -> Result<bytes::Bytes, std::io::Error> {
    let mut priv_data = [0_u8; ED25519_PRIVATE_KEY_SIZE];
    let mut priv_file = File::open(format!("{}.priv", key_name.as_ref()))?;
    priv_file.read_exact(&mut priv_data)?;

    let mut pub_data = [0_u8; ED25519_PUBLIC_KEY_SIZE];
    let mut pub_file = File::open(format!("{}.pub", key_name.as_ref()))?;
    pub_file.read_exact(&mut pub_data)?;

    let mut meta_image = bytes::BytesMut::with_capacity(META_SIZE);
    for _ in 0..META_SIZE {
        meta_image.put_u8(0xFF);
    }

    let mut cursor = meta_image.as_mut();
    cursor.put_slice(&pub_data);
    cursor.put_slice(&priv_data);
    cursor.put(&serial.to_bytes()[..]);

    Ok(meta_image.freeze())
}

fn main() -> Result<(), std::io::Error> {
    let cli = Cli::parse();

    // You can check for the existence of subcommands, and if found use their
    // matches just as you would the top level app
    match &cli.command {
        Commands::GenKey { name } => {
            generate_random_keypair(name)?;
        }
        Commands::SignApp { image, key } => {
            sign_app_image(image, key)?;
        }
        Commands::Meta {
            bootloader,
            name,
            serial,
        } => {
            embed_meta(bootloader, name, serial.clone().into())?;
        }
    }

    Ok(())
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_sign() {
        let mut rng = StdRng::seed_from_u64(5);
        let seed: [u8; 32] = rng.gen();
        let keypair = ed25519_keygen(seed);
        let sig = ed25519_sign(b"hello world", &keypair.private);
        assert!(ed25519_verify(&sig, &keypair.public, b"hello world"));
        assert!(!ed25519_verify(&sig, &keypair.public, b"cool"));
    }

    #[test]
    fn test_gen_keypair() {
        //generate_random_keypair();
    }

    #[test]
    fn test_sign_app_image() {
        //let _ = sign_app_image(Path::new("build/app/app.bin"));
    }

    #[test]
    fn test_meta() {
        let _ = embed_meta(
            Path::new("build/kompressor_cm3/bootloader/bootloader.bin"),
            &String::from("hardware_key"),
        );
    }

    #[test]
    fn test_create_metadata() {
        create_metadata("hardware_key");
    }
}
