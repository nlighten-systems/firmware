// Copyright (C) NLighten Systems, LLC (Paul Soucy) 2022
//
// This file is part of Kompressor Firmware.
//
// Kompressor Firmware is free software: you can redistribute it and/or modify it under the terms of
// the GNU General Public License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any later version.
//
// Kompressor Firmware is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with Foobar. If not, see
// <https://www.gnu.org/licenses/>.

#![no_std]

include!(concat!(env!("OUT_DIR"), "/bindings.rs"));

include!(concat!(env!("OUT_DIR"), "/git_version.rs"));

mod download;
mod hw;

use hw::*;

use rscore::ed25519::ed25519_verify;
use rscore::meta::Meta;
use rscore::prelude::{Sha512, SmallString};
use rscore::AppMetadata;

use core::fmt::Write;

#[cfg(not(test))]
use core::panic::PanicInfo;

#[cfg(not(test))]
#[panic_handler]
fn panic(_panic: &PanicInfo<'_>) -> ! {
    loop {}
}

extern "C" {
    fn HAL_NVIC_SystemReset() -> !;
    fn app_start() -> !;
    fn hw_is_bootloader_pin_assert() -> u8;
    static _app_image: u8;
    static _estack: u8;
}

fn verify_app() -> bool {
    match AppMetadata::load() {
        Some(app_metadata) => {
            let mut hasher = Sha512::new();
            hasher.write(b"firmware");
            hasher.write(&app_metadata.size.to_be_bytes());
            hasher.write(&app_metadata.hardware.to_be_bytes());
            hasher.write(app_metadata.app_image());
            let hash = hasher.finish();

            unsafe { ed25519_verify(&app_metadata.signature, &app_key, &hash) }
        }
        None => false,
    }
}

/*

#![feature(asm)]
use core::arch::asm;

const VECTOR_TABLE_OFFSET: *mut u32 = 0xE000ED08 as *mut u32;

fn start_app() -> ! {

    unsafe {
    let start_appimage = core::ptr::addr_of!(_app_image) as u32 + 80;
    core::ptr::write_volatile(VECTOR_TABLE_OFFSET, start_appimage & 0xFFFFFFF8);

    let vsp = core::ptr::read(start_appimage as *const u32);
    let app_reset_handler = core::ptr::read((start_appimage + 4) as *const u32);

    asm!(
        "msr MSP, {0}",
        "bx {1}",
        in(reg) vsp,
        in(reg) app_reset_handler
    );

    loop{}
    }
}
*/

const NLIGHTEN_LOGO_BANNER: &str = include_str!("nlighten_banner.txt");

fn print_banner() {
    SERIAL.println();
    SERIAL.write_str(NLIGHTEN_LOGO_BANNER);
    SERIAL.println();
    SERIAL.write_str("NLighten bootloader v");
    SERIAL.write_str(GIT_VERSION);
    SERIAL.println();
    SERIAL.write_str("using dev_key");
    SERIAL.println();
    SERIAL.write_str("Serial: ");
    write!(SERIAL, "{}", Meta::get().serial().raw());
    SERIAL.println();
    SERIAL.println();
}

fn enter_download_mode() -> ! {
    download::download_app();
    unsafe {
        HAL_NVIC_SystemReset();
    }
}

#[no_mangle]
pub extern "C" fn main() -> ! {
    unsafe {
        hw::hw_init();
    }
    print_banner();

    let download = unsafe { hw_is_bootloader_pin_assert() > 0 };

    if download {
        SERIAL.write_str("download pin asserted, entering download mode...\r\n");
        enter_download_mode();
    } else {
        SERIAL.write_str("app valid...");
        let app_valid = verify_app();
        match app_valid {
            true => {
                SERIAL.write_str("true");
                SERIAL.println();
                unsafe {
                    app_start();
                }
            }
            false => {
                SERIAL.write_str("false");
                SERIAL.println();
                enter_download_mode();
            }
        }
    }
}
