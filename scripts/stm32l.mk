
stm32l1-y := src/ST/STM32L1xx_HAL_Driver/Src/stm32l1xx_hal.c \
src/ST/STM32L1xx_HAL_Driver/Src/stm32l1xx_hal_cortex.c \
src/ST/STM32L1xx_HAL_Driver/Src/stm32l1xx_hal_dma.c \
src/ST/STM32L1xx_HAL_Driver/Src/stm32l1xx_hal_exti.c \
src/ST/STM32L1xx_HAL_Driver/Src/stm32l1xx_hal_flash.c \
src/ST/STM32L1xx_HAL_Driver/Src/stm32l1xx_hal_flash_ex.c \
src/ST/STM32L1xx_HAL_Driver/Src/stm32l1xx_hal_flash_ramfunc.c \
src/ST/STM32L1xx_HAL_Driver/Src/stm32l1xx_hal_gpio.c \
src/ST/STM32L1xx_HAL_Driver/Src/stm32l1xx_hal_pwr.c \
src/ST/STM32L1xx_HAL_Driver/Src/stm32l1xx_hal_pwr_ex.c \
src/ST/STM32L1xx_HAL_Driver/Src/stm32l1xx_hal_rcc.c \
src/ST/STM32L1xx_HAL_Driver/Src/stm32l1xx_hal_rcc_ex.c \
src/ST/STM32L1xx_HAL_Driver/Src/stm32l1xx_hal_spi.c \
src/ST/STM32L1xx_HAL_Driver/Src/stm32l1xx_hal_tim.c \
src/ST/STM32L1xx_HAL_Driver/Src/stm32l1xx_hal_tim_ex.c \
src/ST/STM32L1xx_HAL_Driver/Src/stm32l1xx_hal_uart.c

stm32l1-$(CONFIG_COM_USB) += src/ST/STM32L1xx_HAL_Driver/Src/stm32l1xx_hal_pcd.c \
src/ST/STM32L1xx_HAL_Driver/Src/stm32l1xx_hal_pcd_ex.c \
src/ST/STM32L1xx_HAL_Driver/Src/stm32l1xx_ll_usb.c \
src/ST/STM32_USB_Device_Library/Core/Src/usbd_core.c \
src/ST/STM32_USB_Device_Library/Core/Src/usbd_ctlreq.c \
src/ST/STM32_USB_Device_Library/Core/Src/usbd_ioreq.c \
src/ST/STM32_USB_Device_Library/Class/CDC/Src/usbd_cdc.c \
src/app/USB_DEVICE/App/usb_device.c \
src/app/USB_DEVICE/App/usbd_cdc_if.c \
src/app/USB_DEVICE/App/usbd_desc.c \
src/app/USB_DEVICE/Target/usbd_conf.c

stm32-include-y := src/CMSIS/Include \
src/CMSIS/Device/ST/STM32L1xx/Include \
src/ST/STM32L1xx_HAL_Driver/Inc

stm32-include-$(CONFIG_COM_USB) += src/app/USB_DEVICE/Target \
src/app/USB_DEVICE/App \
src/ST/STM32_USB_Device_Library/Core/Inc \
src/ST/STM32_USB_Device_Library/Class/CDC/Inc

stm32-y := $(stm32l1-y)