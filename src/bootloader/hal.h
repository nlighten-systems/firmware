#pragma once

#ifdef __cplusplus
extern "C" {
#endif

#define HAL_MAX_DELAY      0xFFFFFFFFU

void hw_init();
int hw_uart_tx(uint8_t* data, uint16_t len, uint32_t timeout);
int hw_uart_rx(uint8_t* data, uint16_t len, uint32_t timeout);

#ifdef __cplusplus
}
#endif