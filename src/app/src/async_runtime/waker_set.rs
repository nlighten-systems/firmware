// Copyright (C) NLighten Systems, LLC (Paul Soucy) 2022
//
// This file is part of Kompressor Firmware.
//
// Kompressor Firmware is free software: you can redistribute it and/or modify it under the terms of
// the GNU General Public License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any later version.
//
// Kompressor Firmware is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with Foobar. If not, see
// <https://www.gnu.org/licenses/>.

use heapless::Vec;

use core::{
    cell::UnsafeCell,
    task::{Context, Waker},
};

use super::NTASKS;

pub struct WakerSet {
    inner: UnsafeCell<Inner>,
}

impl WakerSet {
    pub const fn new() -> Self {
        Self {
            inner: UnsafeCell::new(Inner::new()),
        }
    }

    pub fn cancel(&self, key: usize) {
        // NOTE(unsafe) single-threaded context; OK as long as no references are returned
        unsafe { (*self.inner.get()).cancel(key) }
    }

    pub fn notify_all(&self) -> bool {
        // NOTE(unsafe) single-threaded context; OK as long as no references are returned
        unsafe { (*self.inner.get()).notify_all() }
    }

    pub fn notify_one(&self) -> bool {
        // NOTE(unsafe) single-threaded context; OK as long as no references are returned
        unsafe { (*self.inner.get()).notify_one() }
    }

    pub fn insert(&self, cx: &Context<'_>) -> usize {
        // NOTE(unsafe) single-threaded context; OK as long as no references are returned
        unsafe { (*self.inner.get()).insert(cx) }
    }

    pub fn remove(&self, key: usize) {
        // NOTE(unsafe) single-threaded context; OK as long as no references are returned
        unsafe { (*self.inner.get()).remove(key) }
    }
}

#[derive(Clone, Copy, Eq, PartialEq)]
enum Notify {
    /// Notify one additional entry.
    One,
    // Notify all entries.
    All,
}

struct Inner {
    entries: Vec<Option<Waker>, NTASKS>,
}

impl Inner {
    const fn new() -> Self {
        Self {
            entries: Vec::new(),
        }
    }

    fn cancel(&mut self, key: usize) {
        self.remove(key);
        self.notify_one();
    }

    /// Notifies all blocked operations if none have been notified already.
    ///
    /// Returns `true` if an operation was notified.
    fn notify_all(&mut self) -> bool {
        self.notify(Notify::All)
    }

    /// Notifies one additional blocked operation.
    ///
    /// Returns `true` if an operation was notified.
    fn notify_one(&mut self) -> bool {
        self.notify(Notify::One)
    }

    /// Notifies blocked operations, either one or all of them.
    ///
    /// Returns `true` if at least one operation was notified.
    fn notify(&mut self, n: Notify) -> bool {
        let mut notified = false;

        for opt_waker in self.entries.iter_mut() {
            // If there is no waker in this entry, that means it was already woken.
            if let Some(w) = opt_waker.take() {
                w.wake();
                notified = true;

                if n == Notify::One {
                    break;
                }
            }
        }

        notified
    }

    fn insert(&mut self, cx: &Context<'_>) -> usize {
        let w = cx.waker().clone();
        for (i, opt_waker) in self.entries.iter_mut().enumerate() {
            if opt_waker.is_none() {
                *opt_waker = Some(w);
                return i;
            }
        }
        panic!("OOM");
    }

    /// Removes the waker of an operation.
    fn remove(&mut self, key: usize) {
        self.entries[key] = None;
    }
}
