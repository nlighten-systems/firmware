#![no_std]
#![allow(non_upper_case_globals)]
#![allow(non_camel_case_types)]
#![allow(non_snake_case)]

extern crate alloc;

mod base;
mod critical;
mod delays;
mod isr;
mod mutex;
mod prelude;
mod queue;
mod semaphore;
mod shim;
mod task;
mod units;
mod utils;

pub use crate::base::FreeRtosError;
pub use prelude::v1::*;

#[cfg(feature = "allocator")]
mod allocator;

#[cfg(feature = "allocator")]
pub use crate::allocator::*;

pub use crate::critical::*;
pub use crate::delays::*;
pub use crate::isr::*;
pub use crate::mutex::*;
pub use crate::queue::*;
pub use crate::semaphore::*;
pub use crate::task::*;
pub use crate::units::*;
