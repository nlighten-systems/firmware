// Copyright (C) NLighten Systems, LLC (Paul Soucy) 2022
//
// This file is part of Kompressor Firmware.
//
// Kompressor Firmware is free software: you can redistribute it and/or modify it under the terms of
// the GNU General Public License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any later version.
//
// Kompressor Firmware is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with Foobar. If not, see
// <https://www.gnu.org/licenses/>.

pub mod spi;
mod stm32_hal;
pub mod uart;

use core::{
    future::Future,
    pin::Pin,
    task::{Context, Poll},
};

pub use stm32_hal::*;

pub type StreamBufferHandle_t = *const cty::c_void;
pub type TickType_t = u32;

extern "C" {
    pub fn resetSX130x();
    pub fn resetGPS();
    pub fn reset_host_spi();

    pub fn xStreamBufferReceive(
        xStreamBuffer: StreamBufferHandle_t,
        RxData: *mut u8,
        xBufferLengthBytes: cty::size_t,
        xTicksToWait: TickType_t,
    ) -> cty::size_t;
}

pub struct Sleep {
    wakeup_at: u32,
}

impl Sleep {
    pub fn new(ms: u32) -> Self {
        let now = freertos_rust::FreeRtosUtils::get_tick_count();
        Self {
            wakeup_at: now.wrapping_add(ms),
        }
    }
}

impl Future for Sleep {
    type Output = ();

    fn poll(self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<Self::Output> {
        let now = freertos_rust::FreeRtosUtils::get_tick_count();
        if self.wakeup_at.wrapping_sub(now) <= 0 {
            Poll::Ready(())
        } else {
            cx.waker().wake_by_ref();
            Poll::Pending
        }
    }
}

// struct TickTimer {
//     start: TickType_t,
//     period: TickType_t,
// }

// impl TickTimer {
//     fn new(period: TickType_t) -> Self {
//         TickTimer {
//             start: reertos_rust::FreeRtosUtils::get_tick_count(),
//             period: period,
//         }
//     }

//     fn tick(&mut self) -> bool {
//         let now = reertos_rust::FreeRtosUtils::get_tick_count();
//         let diff = now.wrapping_sub(self.start);
//         if diff >= self.period {
//             self.start = now;
//             return true;
//         } else {
//             return false;
//         }
//     }
// }

pub struct TickTimer {
    start: TickType_t,
    period: TickType_t,
}

impl TickTimer {
    pub fn new(period: TickType_t) -> Self {
        Self {
            start: freertos_rust::FreeRtosUtils::get_tick_count(),
            period: period,
        }
    }

    pub fn tick(&mut self) -> impl Future<Output = ()> + '_ {
        futures::future::poll_fn(|cx| {
            if self.is_expired() {
                self.reset();
                Poll::Ready(())
            } else {
                cx.waker().wake_by_ref();
                Poll::Pending
            }
        })
    }

    pub fn is_expired(&self) -> bool {
        let now = freertos_rust::FreeRtosUtils::get_tick_count();
        let diff = now.wrapping_sub(self.start);
        diff >= self.period
    }

    pub fn reset(&mut self) {
        self.start = freertos_rust::FreeRtosUtils::get_tick_count();
    }
}
