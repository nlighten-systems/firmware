// Copyright (C) NLighten Systems, LLC (Paul Soucy) 2022
//
// This file is part of Kompressor Firmware.
//
// Kompressor Firmware is free software: you can redistribute it and/or modify it under the terms of
// the GNU General Public License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any later version.
//
// Kompressor Firmware is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with Foobar. If not, see
// <https://www.gnu.org/licenses/>.

use log::{Level, Metadata, Record};

use crate::hal::{huart1, Stm32UART};

pub struct UARTLogger;

impl log::Log for UARTLogger {
    fn enabled(&self, metadata: &Metadata) -> bool {
        metadata.level() <= Level::Trace
    }

    fn log(&self, record: &Record) {
        if self.enabled(record.metadata()) {
            use core::fmt::Write;

            //let ptr = 0x20000a70 as *const UART_HandleTypeDef as *const _;
            let mut uart = unsafe { Stm32UART { handle: &huart1 } };
            let _ = write!(uart, "{}", record.level());
            let _ = write!(uart, " ");
            let _ = write!(uart, "{}", record.args());
            let _ = write!(uart, "\r\n");
        }
    }

    fn flush(&self) {}
}
