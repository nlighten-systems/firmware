// Copyright (C) NLighten Systems, LLC (Paul Soucy) 2022
//
// This file is part of Kompressor Firmware.
//
// Kompressor Firmware is free software: you can redistribute it and/or modify it under the terms of
// the GNU General Public License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any later version.
//
// Kompressor Firmware is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with Foobar. If not, see
// <https://www.gnu.org/licenses/>.

use crate::prelude::*;

pub const APP_MAGIC: [u8; 2] = [0x4a, 0xa3];

#[repr(C)]
#[repr(align(4))]
pub struct AppMetadata<'a> {
    pub size: u32,
    pub hardware: u16,
    pub signature: &'a Signature,
}

impl<'a> AppMetadata<'a> {
    pub fn load() -> Option<Self> {
        let d = unsafe { core::slice::from_raw_parts(crate::APP_IMAGE_HEADER as *const u8, 80) };
        if d[..2] != APP_MAGIC {
            return None;
        }

        Some(AppMetadata {
            size: u32::from_be_bytes(d[2..6].try_into().unwrap()),
            hardware: u16::from_be_bytes(d[6..8].try_into().unwrap()),
            signature: d[8..72].try_into().unwrap(),
        })
    }

    pub fn app_image(&self) -> &[u8] {
        unsafe {
            let start_appimage = crate::APP_IMAGE_START;
            core::slice::from_raw_parts(
                start_appimage as *const u8,
                self.size.min(crate::APP_MAX_SIZE) as usize,
            )
        }
    }

    #[cfg(feature = "std")]
    pub fn write(&self, out: &mut impl std::io::Write) -> std::io::Result<()> {
        use std::io::prelude::*;
        use std::io::Cursor;

        let mut buf = Cursor::new(vec![0_u8; 256]);

        buf.write_all(&APP_MAGIC)?;
        buf.write_all(&self.size.to_be_bytes())?;
        buf.write_all(&self.hardware.to_be_bytes())?;
        buf.write_all(self.signature)?;

        out.write_all(buf.get_ref())?;
        Ok(())
    }
}

#[cfg(test)]
mod test {
    use super::*;
}
