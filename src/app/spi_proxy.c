#include "main.h"
#include "cmsis_os.h"
#include <string.h>
#include <stdio.h>

#include "spi_proxy.h"

// #define DEBUG


/////// Rust Functions ///////
void ctx_sx1250_write(spi_cmd_w_reg_1250_t* cmd);
void ctx_sx130x_write(spi_cmd_w_reg_130x_t* cmd);
void ctx_sx130x_read(spi_cmd_r_reg_130x_t* cmd);
void ext_cfg_done();


#define CMD_MASK				  0x1F
#define CMD_DEST_MASK     0x07
#define CMD_W_BIT         0x08
#define CMD_R_BIT         0x10
#define CMD_ACK_BIT       0x20

#define CMD_DEST_130X     0x00
#define CMD_DEST_R1       0x01
#define CMD_DEST_R2       0x02
#define CMD_DEST_MICRO    0x03
#define CMD_DEST_HOST     0x04

#define CMD_W_REG_130X 		(CMD_W_BIT | CMD_DEST_130X)
#define CMD_WB_130X				0x90
#define CMD_R_REG_130X 		(CMD_R_BIT | CMD_DEST_130X)
#define CMD_RB_130X				0xA0
#define CMD_RMW_REG_130X 	(CMD_R_BIT | CMD_W_BIT | CMD_DEST_130X)
#define CMD_W_REG_1250		(CMD_W_BIT | CMD_DEST_R1)
#define CMD_R_REG_1250		0x60
#define CMD_W_REG_MICRO		(CMD_W_BIT | CMD_DEST_MICRO)

#define SPI_MUX_130X	0
#define WRITE_ACCESS	0x80
#define READ_ACCESS		0x00

extern osThreadId spiProxyTaskHandle;
extern SPI_HandleTypeDef hspi1;
extern SPI_HandleTypeDef hspi2;
extern EventGroupHandle_t eventsHandle;
extern UART_HandleTypeDef huart1;
extern QueueHandle_t spiSlaveRxQueue;
extern QueueHandle_t spiSlaveTxQueue;

uint32_t ulTaskNotifyValueClear(TaskHandle_t xTask, uint32_t ulBitsToClear);


// RMA bufs for SPI slave tx/rx
static uint8_t gSpiSlaveTxBuf[SPI_PKT_SIZE];
static uint8_t gSpiSlaveRxBuf[SPI_PKT_SIZE];

static void spi_master_send_w_reg_130x(spi_cmd_w_reg_130x_t* cmd);
void spi_master_read_reg_130x(spi_cmd_r_reg_130x_t* cmd);
static void spi_master_send_rmw_reg_130x(spi_cmd_rmw_reg_130x_t* cmd);
static void spi_master_read_reg_1250(spi_cmd_r_reg_1250_t* cmd);
static void spi_master_write_reg_1250(spi_cmd_w_reg_1250_t* cmd);

static
void write_header(uint8_t* buf, uint8_t bits, uint8_t dest, uint16_t address, uint8_t buf_size)
{
  buf_size -= 1;
  buf[0] = bits | (0xC0 & (buf_size << 6)) | (0x07 & dest);
  buf[1] = 0x00FF & address;
  buf[2] = (0x7F & (address >> 8)) | ((0x04 & buf_size) << 5);
}

static
void header_get_address(uint8_t* buf, uint16_t* address)
{
  *address = (0x7F00 & (buf[2] << 8)) |
	            (0x00FF & buf[1]);
}

static
void header_get_buf_size(uint8_t* buf, uint8_t* buf_size)
{
  *buf_size = ((0x04 & (buf[2] >> 5)) |
              (0x03 & (buf[0] >> 6))) + 1;
}

static
void write_reg_130x(uint8_t* cmd_pkt)
{
	spi_cmd_w_reg_130x_t cmd;
	header_get_address(cmd_pkt, &cmd.address);
	header_get_buf_size(cmd_pkt, &cmd.buf_size);
	memcpy(cmd.buf, &cmd_pkt[3], cmd.buf_size);

	spi_master_send_w_reg_130x(&cmd);

	cmd_pkt[0] |= CMD_ACK_BIT;
	xQueueSend(spiSlaveTxQueue, cmd_pkt, 200);
}

static
void read_reg_130x(uint8_t* cmd_pkt)
{
	spi_cmd_r_reg_130x_t cmd;
	header_get_address(cmd_pkt, &cmd.address);
	header_get_buf_size(cmd_pkt, &cmd.buf_size);

	spi_master_read_reg_130x(&cmd);

	cmd_pkt[0] |= CMD_ACK_BIT;
	memcpy(&cmd_pkt[3], cmd.buf, cmd.buf_size);
	xQueueSend(spiSlaveTxQueue, cmd_pkt, 200);
}

static
void rmw_reg_130x(uint8_t* cmd_pkt)
{
	spi_cmd_rmw_reg_130x_t cmd;
	header_get_address(cmd_pkt, &cmd.address);
	cmd.value = cmd_pkt[3];
	cmd.mask = cmd_pkt[4];

	spi_master_send_rmw_reg_130x(&cmd);

	cmd_pkt[0] |= CMD_ACK_BIT;
	xQueueSend(spiSlaveTxQueue, cmd_pkt, 200);
}

static
void read_sx1250(uint8_t* cmd_pkt)
{
	spi_cmd_r_reg_1250_t cmd;
	cmd.radio = 0x07 & cmd_pkt[0];

	{
	uint16_t address;
	header_get_address(cmd_pkt, &address);
	cmd.op_code = 0x00FF & address;
	}

	header_get_buf_size(cmd_pkt, &cmd.buf_size);

	spi_master_read_reg_1250(&cmd);

	cmd_pkt[0] |= CMD_ACK_BIT;
	memcpy(&cmd_pkt[3], cmd.buf, cmd.buf_size);
	xQueueSend(spiSlaveTxQueue, cmd_pkt, 200);
}


static
void write_sx1250(uint8_t* cmd_pkt)
{
	spi_cmd_w_reg_1250_t cmd;
	cmd.radio = 0x07 & cmd_pkt[0];

	{
	uint16_t address;
	header_get_address(cmd_pkt, &address);
	cmd.op_code = 0x00FF & address;
	}

	header_get_buf_size(cmd_pkt, &cmd.buf_size);
  	memcpy(cmd.buf, &cmd_pkt[3], cmd.buf_size);

	spi_master_write_reg_1250(&cmd);

	cmd_pkt[0] |= CMD_ACK_BIT;
	xQueueSend(spiSlaveTxQueue, cmd_pkt, 200);
}

static
void write_reg_micro(uint8_t* cmd_pkt)
{
	ext_cfg_done();
	cmd_pkt[0] |= CMD_ACK_BIT;
	xQueueSend(spiSlaveTxQueue, cmd_pkt, 200);
}

/////////////////////// SPI MASTER /////////////////////////////

static
void spi_master_send_w_reg_130x(spi_cmd_w_reg_130x_t* cmd)
{
	#ifdef DEBUG
	{
		uint8_t buf[32];
		int len = snprintf((char*)buf, 32, "w_reg_130x: 0x%x 0x%02x\r\n", cmd->address, cmd->buf[0]);
		HAL_UART_Transmit(&huart1, buf, len, 500);
	}
	#endif
	uint8_t buf[3];

	buf[0] = SPI_MUX_130X;
	buf[1] = WRITE_ACCESS | (0x7F & (cmd->address >> 8));
	buf[2] = cmd->address;

	HAL_GPIO_WritePin(SPI1_NSS_GPIO_Port, SPI1_NSS_Pin, RESET);
	HAL_SPI_Transmit(&hspi1, buf, 3, 200);
  	HAL_SPI_Transmit(&hspi1, cmd->buf, cmd->buf_size, 200);
	HAL_GPIO_WritePin(SPI1_NSS_GPIO_Port, SPI1_NSS_Pin, SET);

  	ctx_sx130x_write(cmd);
}

void spi_master_read_reg_130x(spi_cmd_r_reg_130x_t* cmd)
{
	#ifdef DEBUG
	{
		uint8_t buf[32];
		int len = snprintf((char*)buf, 32, "r_reg_130x: 0x%x ", cmd->address);
		HAL_UART_Transmit(&huart1, buf, len, 500);
	}
	#endif
	uint8_t txData[4];

	txData[0] = SPI_MUX_130X;
	txData[1] = READ_ACCESS | ((cmd->address >> 8) & 0x7F);
	txData[2] = cmd->address;
	txData[3] = 0;

	HAL_GPIO_WritePin(SPI1_NSS_GPIO_Port, SPI1_NSS_Pin, RESET);
	HAL_SPI_Transmit(&hspi1, txData, 4, 200);
  	HAL_SPI_Receive(&hspi1, cmd->buf, cmd->buf_size, 200);
	HAL_GPIO_WritePin(SPI1_NSS_GPIO_Port, SPI1_NSS_Pin, SET);

	ctx_sx130x_read(cmd);

	#ifdef DEBUG
	{
		uint8_t buf[32];
		int len = snprintf((char*)buf, 32, "0x%02x\r\n", cmd->buf[0]);
		HAL_UART_Transmit(&huart1, buf, len, 500);
	}
	#endif
}

static
void spi_master_send_rmw_reg_130x(spi_cmd_rmw_reg_130x_t* cmd)
{
	#ifdef DEBUG
	{
		uint8_t buf[32];
		int len = snprintf((char*)buf, 32, "rmw_reg_130x: 0x%x\r\n", cmd->address);
		HAL_UART_Transmit(&huart1, buf, len, 500);
	}
	#endif
	spi_cmd_r_reg_130x_t read_cmd;
	spi_cmd_w_reg_130x_t write_cmd;

	read_cmd.address = cmd->address;
  	read_cmd.buf_size = 1;

	write_cmd.address = cmd->address;
  	write_cmd.buf_size = 1;

	spi_master_read_reg_130x(&read_cmd);
	write_cmd.buf[0] = read_cmd.buf[0];
	write_cmd.buf[0] &= ~cmd->mask;
	write_cmd.buf[0] |= cmd->value;
	//write_cmd.value |= cmd->mask & cmd->value;

	//write_cmd.value = (~cmd->mask & read_cmd.value) | (cmd->mask & cmd->value);
	spi_master_send_w_reg_130x(&write_cmd);
}

static
void spi_master_read_reg_1250(spi_cmd_r_reg_1250_t* cmd)
{
	#ifdef DEBUG
	{
		uint8_t buf[32];
		int len = snprintf((char*)buf, 32, "r_reg_1250: %d 0x%02x ", cmd->radio, cmd->op_code);
		HAL_UART_Transmit(&huart1, buf, len, 500);
	}
	#endif
	
	uint8_t txData[2];
	txData[0] = cmd->radio;
	txData[1] = cmd->op_code;

	HAL_GPIO_WritePin(SPI1_NSS_GPIO_Port, SPI1_NSS_Pin, RESET);
	HAL_SPI_Transmit(&hspi1, txData, 2, 200);
  	HAL_SPI_Receive(&hspi1, cmd->buf, cmd->buf_size, 200);
	HAL_GPIO_WritePin(SPI1_NSS_GPIO_Port, SPI1_NSS_Pin, SET);

	#ifdef DEBUG
	{
		uint8_t buf[32];
		int len;
		len = snprintf((char*)buf, 32, "[ 0x%02x", cmd->buf[0]);
		HAL_UART_Transmit(&huart1, buf, len, 500);

		for(int i=1;i<cmd->buf_size;i++) {
			len = snprintf((char*)buf, 32, ", 0x%02x", cmd->buf[i]);
			HAL_UART_Transmit(&huart1, buf, len, 500);
		}
		
		len = snprintf((char*)buf, 32, " ]\r\n");
		HAL_UART_Transmit(&huart1, buf, len, 500);
	}
	#endif
}

static
void spi_master_write_reg_1250(spi_cmd_w_reg_1250_t* cmd)
{
	#ifdef DEBUG
	{
		uint8_t buf[32];
		int len = snprintf((char*)buf, 32, "w_reg_1250: %d 0x%02x 0x%02x\r\n", cmd->radio, cmd->op_code, cmd->buf[0]);
		HAL_UART_Transmit(&huart1, buf, len, 500);
	}
	#endif
	uint8_t txData[2];
	
	txData[0] = cmd->radio;
	txData[1] = cmd->op_code;

	HAL_GPIO_WritePin(SPI1_NSS_GPIO_Port, SPI1_NSS_Pin, RESET);
	HAL_SPI_Transmit(&hspi1, txData, 2, 200);
  	HAL_SPI_Transmit(&hspi1, cmd->buf, cmd->buf_size, 200);
	HAL_GPIO_WritePin(SPI1_NSS_GPIO_Port, SPI1_NSS_Pin, SET);

  ctx_sx1250_write(cmd);
}

static
void readRxQueue()
{
	uint8_t cmd_pkt[SPI_PKT_SIZE];
	if (xQueueReceive(spiSlaveRxQueue, cmd_pkt, portMAX_DELAY) == pdTRUE) {

      switch (0x1F & cmd_pkt[0]) {
		case 0:
			break;
        case CMD_W_REG_130X:
          write_reg_130x(cmd_pkt);
          break;

        case CMD_R_REG_130X:
          read_reg_130x(cmd_pkt);
          break;

        case CMD_RMW_REG_130X:
          rmw_reg_130x(cmd_pkt);
          break;

        case (CMD_W_BIT | CMD_DEST_R1):
        case (CMD_W_BIT | CMD_DEST_R2):
          write_sx1250(cmd_pkt);
          break;

        case (CMD_R_BIT | CMD_DEST_R1):
        case (CMD_R_BIT | CMD_DEST_R2):
          read_sx1250(cmd_pkt);
          break;

		case CMD_W_REG_MICRO:
			write_reg_micro(cmd_pkt);
			break;

        default:
          #ifdef DEBUG
          {
            uint8_t buf[32];
            int len = snprintf((char*)buf, 32, "unknown command: 0x%02x\r\n", cmd_pkt[0]);
            HAL_UART_Transmit(&huart1, buf, len, 500);
          }
          #endif
		  break;

      }
    }
}

void spiProxyFn(void const * argument)
{
  HAL_SPI_TransmitReceive_DMA(&hspi2, gSpiSlaveTxBuf, gSpiSlaveRxBuf, SPI_PKT_SIZE);
  
  for(;;) {
    readRxQueue();

	//if(ulTaskNotifyTake(pdTRUE, portMAX_DELAY)) {
	HAL_SPI_TransmitReceive_DMA(&hspi2, gSpiSlaveTxBuf, gSpiSlaveRxBuf, SPI_PKT_SIZE);
	//}

  }
}

/*
void spiMasterFn(void const* argument)
{
	for(;;)
	{
		if (xSemaphoreTake(spiMasterCmdHandle, portMAX_DELAY) == pdTRUE) {
			switch (gMasterCmd->type) {
				case SPI_CMD_W_REG_130x: 
					spi_master_send_w_reg_130x(&gMasterCmd->w_reg_130x);
					break;

				case SPI_CMD_R_REG_130x: 
					spi_master_read_reg_130x(&gMasterCmd->r_reg_130x);
					break;

				case SPI_CMD_RMW_REG_130x: 
					spi_master_send_rmw_reg_130x(&gMasterCmd->rmw_reg_130x);  
					break;

				case SPI_CMD_W_REG_1250:
					spi_master_write_reg_1250(&gMasterCmd->w_reg_1250);
					break;

				case SPI_CMD_R_REG_1250:
					spi_master_read_reg_1250(&gMasterCmd->r_reg_1250);
					break;
			}
		}
		
	}
}
*/

/*
void HAL_SPI_RxCpltCallback(SPI_HandleTypeDef* hspi)
{
	BaseType_t xHigherPriorityTaskWoken = pdFALSE;

	if(hspi == &hspi2) {
		vTaskNotifyGiveFromISR(spiSlaveTaskHandle, &xHigherPriorityTaskWoken );
	}

	if(hspi == &hspi1) {
		vTaskNotifyGiveFromISR(spiMasterTaskHandle, &xHigherPriorityTaskWoken);
	}
	portYIELD_FROM_ISR( xHigherPriorityTaskWoken );
}
*/

void HAL_SPI_TxRxCpltCallback(SPI_HandleTypeDef* hspi)
{
	BaseType_t xHigherPriorityTaskWoken = pdFALSE;

	if(hspi == &hspi2) {
		//if (gSpiSlaveRxBuf[0] != 0) {
			xQueueSendFromISR(spiSlaveRxQueue, gSpiSlaveRxBuf, &xHigherPriorityTaskWoken);
			memset(gSpiSlaveRxBuf, 0, SPI_PKT_SIZE);
		//}
		if (!xQueueReceiveFromISR(spiSlaveTxQueue, gSpiSlaveTxBuf, &xHigherPriorityTaskWoken)) {
			memset(gSpiSlaveTxBuf, 0, SPI_PKT_SIZE);
		}

		//vTaskNotifyGiveFromISR(spiProxyTaskHandle, &xHigherPriorityTaskWoken);

		/*
		int code;
		while((code = HAL_SPI_TransmitReceive_DMA(&hspi2, gSpiSlaveTxBuf, gSpiSlaveRxBuf, SPI_PKT_SIZE)) != HAL_OK) {
			HAL_SPI_Abort(&hspi2);
			#ifdef DEBUG
			{
				uint8_t buf[32];
				int len = snprintf((char*)buf, 32, "WTF\r\n");
				HAL_UART_Transmit(&huart1, buf, len, 500);
				
			}
			#endif
		}
		*/
		
    	portYIELD_FROM_ISR( xHigherPriorityTaskWoken );
	} 
	
}

/*
void HAL_SPI_TxCpltCallback(SPI_HandleTypeDef* hspi)
{
	BaseType_t xHigherPriorityTaskWoken = pdFALSE;

	if(hspi == &hspi2) {
		vTaskNotifyGiveFromISR(spiSlaveTaskHandle, &xHigherPriorityTaskWoken );
	}

	if(hspi == &hspi1) {
		vTaskNotifyGiveFromISR(spiMasterTaskHandle, &xHigherPriorityTaskWoken);
	}
	portYIELD_FROM_ISR( xHigherPriorityTaskWoken );
}
*/





