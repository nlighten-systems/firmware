// Copyright (C) NLighten Systems, LLC (Paul Soucy) 2022
//
// This file is part of Kompressor Firmware.
//
// Kompressor Firmware is free software: you can redistribute it and/or modify it under the terms of
// the GNU General Public License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any later version.
//
// Kompressor Firmware is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with Foobar. If not, see
// <https://www.gnu.org/licenses/>.

use core::{alloc::AllocError, future::Future};

use freertos_rust::DurationTicks;
use kompressor_common::{
    concentrator::{
        stream::SPIFrame, Frame, SX1250Data, SX1250Read, SX1250Write, SX130xMemData, SX130xRegRead,
        SX130xRegValue, SX130xRegWrite, StreamData, CHUNK_SIZE_MAX,
    },
    lora::RxPktRep,
};
use rscore::{io::AsyncWriter, lorapkt::LoraPacketRep};

use crate::concentrator::signpkt::write_cbor;
use crate::{
    async_runtime::{self, Mutex},
    concentrator::StreamAsyncWriter,
    hal::{spi, Sleep, TickTimer},
};

use log::{debug, error, info, LevelFilter};

use super::signpkt::{LoRaPacketOwned, SignPktReq};

use alloc::boxed::Box;

const SPI_MUX_130X: u8 = 0;
const WRITE_ACCESS: u8 = 0x80;
const READ_ACCESS: u8 = 0x00;

const SX130X_FIFO: u16 = 0x4000;
const SX130X_OTP_RD_DATA_RD_DATA: u16 = 0x6181;

const SMCU_REG_STATUS: u8 = 0x00;
const SMCU_STATUS_RUN: u8 = 0x01;

pub async fn run() {
    let static_data = unsafe { crate::DATA.assume_init_mut() };

    let mut last_pps_timer = TickTimer::new(300);
    let mut fifo_timer = TickTimer::new(10);

    loop {
        if let Some(frame) = static_data.host_rx.try_take() {
            handle_cmds(&frame).await;
        }

        if (static_data.smcu_reg[SMCU_REG_STATUS as usize] & SMCU_STATUS_RUN) > 0 {
            if fifo_timer.is_expired() {
                read_fifo().await;
                fifo_timer.reset();
            }

            if last_pps_timer.is_expired() {
                read_last_pps().await;
                last_pps_timer.reset();
            }

            if let Ok(v) = static_data
                .sign_pkt
                .receive(freertos_rust::Duration::zero())
            {
                let _ = crate::concentrator::signpkt::write_cbor_signpkt(v.id, &v.sig).await;
            }
        }

        async_runtime::r#yield().await;
    }
}

async fn read_last_pps() {
    let static_data = unsafe { crate::DATA.assume_init_mut() };

    let address = 0x6101;

    let buf_header = [
        SPI_MUX_130X,
        READ_ACCESS | (0x7F & (address >> 8) as u8),
        address as u8,
        0,
    ];

    let mut buf = [0_u8; 8];

    spi::spi1_cs_set();
    spi::spi1_write(&buf_header);
    spi::spi1_read(&mut buf);
    spi::spi1_cs_clear();

    let pps_time = u32::from_be_bytes([buf[0], buf[1], buf[2], buf[3]]);
    static_data.sx130x_pps.last_pps(pps_time);
}

async fn read_fifo() {
    let static_data = unsafe { crate::DATA.assume_init_mut() };

    let address = 0x58c8;

    let buf_header = [
        SPI_MUX_130X,
        READ_ACCESS | (0x7F & (address >> 8) as u8),
        address as u8,
        0,
    ];

    let mut value = [0, 0];

    spi::spi1_cs_set();
    spi::spi1_write(&buf_header);
    spi::spi1_read(&mut value);
    spi::spi1_cs_clear();

    let mut num_bytes = u16::from_be_bytes(value);

    /* Workaround for multi-byte read issue: read again and ensure new read is not lower than the previous one */
    spi::spi1_cs_set();
    spi::spi1_write(&buf_header);
    spi::spi1_read(&mut value);
    spi::spi1_cs_clear();

    num_bytes = num_bytes.max(u16::from_be_bytes(value));

    if num_bytes > 0 {
        info!("fifo bytes: {}", num_bytes);
    }

    while num_bytes > 0 {
        let address = SX130X_FIFO;
        let buf_header = [
            SPI_MUX_130X,
            READ_ACCESS | (0x7F & (address >> 8) as u8),
            address as u8,
            0,
        ];

        let mut buf = [0_u8; 16];

        let bytes_read = num_bytes.min(16);
        let buf_slice = &mut buf[..bytes_read as usize];

        spi::spi1_cs_set();
        spi::spi1_write(&buf_header);
        spi::spi1_read(buf_slice);
        spi::spi1_cs_clear();

        sx130x_mem_read_hook(SX130X_FIFO, buf_slice).await;

        num_bytes -= bytes_read as u16;
    }
}

async fn handle_cmds(frame: &SPIFrame) {
    let data = unsafe { crate::DATA.assume_init_mut() };

    match Frame::try_from(&frame) {
        Ok(Frame::SX130xRegWrite(d)) => sx130x_write(d).await,

        Ok(Frame::SX130xRegRead(d)) => {
            let value = sx130x_read(d).await;
            spi::host_send(
                data,
                &Frame::SX130xRegValue(SX130xRegValue {
                    address: d.address,
                    value: value,
                }),
            )
            .await;
        }

        Ok(Frame::SX130xRegRMW(d)) => {
            let mut value = sx130x_read(SX130xRegRead { address: d.address }).await;
            value &= !d.mask;
            value |= d.value;
            sx130x_write(SX130xRegWrite {
                address: d.address,
                value: value,
            })
            .await;
        }

        Ok(Frame::SX130xMemWrite(d)) => {
            #[cfg(feature = "debug")]
            debug!("SX130xMemWrite 0x{:04x} {}", d.address, d.len);
            data.sx130x_write_op = Some(d);
        }

        Ok(Frame::SX130xMemRead(d)) => {
            #[cfg(feature = "debug")]
            debug!("SX130xMemRead 0x{:04x} {}", d.address, d.len);

            let buf_header = [
                SPI_MUX_130X,
                READ_ACCESS | (0x7F & (d.address >> 8) as u8),
                d.address as u8,
                0,
            ];

            let mut buf = [0_u8; CHUNK_SIZE_MAX];

            let mut remaining = d.len as usize;

            spi::spi1_cs_set();
            spi::spi1_write(&buf_header);

            while remaining > 0 {
                let n = remaining.min(CHUNK_SIZE_MAX);
                spi::spi1_read(&mut buf[..n as usize]);

                spi::host_send(
                    data,
                    &Frame::SX130xMemData(SX130xMemData { data: &buf[..n] }),
                )
                .await;
                remaining -= n;

                #[cfg(feature = "debug")]
                if (remaining % 1024) == 0 {
                    debug!("remaining {}", remaining);
                }

                sx130x_mem_read_hook(d.address, &buf).await;
            }

            spi::spi1_cs_clear();
        }

        Ok(Frame::SX130xMemData(d)) => {
            let mut is_finished = false;
            match data.sx130x_write_op.as_mut() {
                Some(op) => {
                    let buf_header = [
                        SPI_MUX_130X,
                        WRITE_ACCESS | (0x7F & (op.address >> 8) as u8),
                        op.address as u8,
                    ];

                    spi::spi1_cs_set();
                    spi::spi1_write(&buf_header);
                    spi::spi1_write(d.data);
                    spi::spi1_cs_clear();

                    op.address += d.data.len() as u16;
                    op.len -= d.data.len() as u16;

                    #[cfg(feature = "debug")]
                    if (op.len % 1024) == 0 {
                        debug!("remaining {}", op.len);
                    }
                    if op.len == 0 {
                        is_finished = true;
                    }
                }

                None => {
                    error!("no pending write operation");
                }
            }

            if is_finished {
                data.sx130x_write_op = None;
            }
        }

        Ok(Frame::SX1250Read(d)) => {
            let rx_buf = sx1250_read(d).await;
            spi::host_send(
                data,
                &Frame::SX1250Data(SX1250Data {
                    radio: d.radio,
                    opcode: d.opcode,
                    buf: rx_buf,
                }),
            )
            .await;
        }

        Ok(Frame::SX1250Write(d)) => sx1250_write(d).await,

        Ok(Frame::SMCUWrite(d)) => smcu_write(d).await,

        Ok(o) => {
            error!("unhandled frame: {:?}", o);
        }

        Err(e) => {
            error!("parsing frame: {:?}", e);
        }
    }
}

async fn sx130x_write(d: SX130xRegWrite) {
    let buf = [
        SPI_MUX_130X,
        WRITE_ACCESS | (0x7F & (d.address >> 8) as u8),
        d.address as u8,
        d.value,
    ];

    spi::spi1_cs_set();
    spi::spi1_write(&buf);
    spi::spi1_cs_clear();

    let data = unsafe { crate::DATA.assume_init_mut() };
    data.radio_ctx.sx130x_write_reg(d.address, &[d.value]);

    #[cfg(feature = "debug")]
    debug!("SX130xRegWrite 0x{:04x} <= 0x{:02x}", d.address, d.value);
}

async fn sx130x_read(d: SX130xRegRead) -> u8 {
    let buf = [
        SPI_MUX_130X,
        READ_ACCESS | (0x7F & (d.address >> 8) as u8),
        d.address as u8,
        0,
    ];

    let mut value = [0_u8];

    spi::spi1_cs_set();
    spi::spi1_write(&buf);
    spi::spi1_read(&mut value);
    spi::spi1_cs_clear();

    sx130x_mem_read_hook(d.address, &value).await;

    #[cfg(feature = "debug")]
    debug!("SX130xRegRead 0x{:04x} => 0x{:02x}", d.address, value[0]);
    value[0]
}

async fn sx1250_write(d: SX1250Write) {
    //todo: assert value of radio > 0

    let data_buf = &d.buf[..d.buf_size as usize];

    spi::spi1_cs_set();
    spi::spi1_write(&[d.radio, d.opcode]);
    spi::spi1_write(data_buf);
    spi::spi1_cs_clear();

    const OPCODE_SET_RF_FREQUENCY: u8 = 0x86;

    match d.opcode {
        OPCODE_SET_RF_FREQUENCY => {
            let data = unsafe { crate::DATA.assume_init_mut() };
            data.radio_ctx.rf_chain[(d.radio - 1) as usize]
                .freq
                .write(0, data_buf);
        }

        _ => {}
    }

    #[cfg(feature = "debug")]
    debug!(
        "SX1250Write {} 0x{:02x} {:?}",
        d.radio,
        d.opcode,
        &d.buf[..d.buf_size as usize]
    );
}

async fn sx1250_read(d: SX1250Read) -> [u8; 5] {
    //todo: assert value of radio > 0
    let buf = [d.radio, d.opcode];

    let mut rx_buf = [0_u8; 5];

    spi::spi1_cs_set();
    spi::spi1_write(&buf);
    spi::spi1_read(&mut rx_buf[..d.buf_size as usize]);
    spi::spi1_cs_clear();

    #[cfg(feature = "debug")]
    debug!(
        "SX1250Read {} 0x{:02x} => {:?}",
        d.radio,
        d.opcode,
        &rx_buf[..d.buf_size as usize]
    );
    rx_buf
}

async fn smcu_write(d: SX130xRegWrite) {
    let static_data = unsafe { crate::DATA.assume_init_mut() };

    match static_data.smcu_reg.get_mut(d.address as usize) {
        Some(v) => *v = d.value,
        None => error!("SMCUWrite out of range: 0x{:04x}", d.address),
    }

    //#[cfg(feature = "debug")]
    debug!("SMCUWrite 0x{:04x} <= 0x{:02x}", d.address, d.value);
}

/// Called whenever data is read from the SX130X
async fn sx130x_mem_read_hook(addr: u16, buf: &[u8]) {
    match addr {
        SX130X_FIFO => handle_read_fifo(buf).await,
        SX130X_OTP_RD_DATA_RD_DATA => {
            let data = unsafe { crate::DATA.assume_init_mut() };
            data.radio_ctx
                .eui
                .set(data.radio_ctx.sx130x_mem_1[0], buf[0]);
        }
        _ => {}
    }
}

async fn handle_read_fifo(buf: &[u8]) {
    let data = unsafe { crate::DATA.assume_init_mut() };
    for b in buf {
        if let Some(pkt) = data.lora_pkt_builder.push(*b) {
            use kompressor_common::utils::Tracer;

            //#[cfg(feature = "debug")]
            let tracer = Tracer::new("pkt received");

            data.radio_ctx.gps_time = None;
            if let Some(ref gps_time) = data.gps_time {
                match data.sx130x_pps.to_ns(pkt.timestamp()) {
                    Ok(mut nano_diff) => {
                        if let Ok(sub_clocks) = data.radio_ctx.fine_timestamp_sub_clocks(&pkt) {
                            // convert 32 Mhz clocks to nanoseconds
                            nano_diff +=
                                unsafe { core::intrinsics::roundf32(sub_clocks * 31.25) } as i32;
                        }

                        data.radio_ctx.gps_time =
                            Some(gps_time.clone().saturating_add_nanos(nano_diff));
                    }

                    Err(_) => {
                        data.radio_ctx.gps_time = Some(*gps_time);
                    }
                }
            }

            let ctx = data.radio_ctx.clone();

            let pkt_rep = LoraPacketRep {
                pkt: pkt,
                ctx: &ctx,
            };

            // send the RxPkt back to the host
            write_cbor(&pkt_rep).await;

            let r = match LoRaPacketOwned::try_new_box(&pkt_rep.pkt, &ctx) {
                Ok(pkt_owned) => {
                    let ptr = Box::into_raw(pkt_owned);
                    match data.sign_queue.send(ptr, freertos_rust::Duration::ticks(0)) {
                        Ok(_) => Ok(()),
                        Err(_) => Err(AllocError),
                    }
                }

                Err(e) => Err(e),
            };

            if let Err(_) = r {
                error!("sign queue full. Dropping sig");
            }
        }
    }
}
