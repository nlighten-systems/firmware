// Copyright (C) NLighten Systems, LLC (Paul Soucy) 2022
//
// This file is part of Kompressor Firmware.
//
// Kompressor Firmware is free software: you can redistribute it and/or modify it under the terms of
// the GNU General Public License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any later version.
//
// Kompressor Firmware is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with Foobar. If not, see
// <https://www.gnu.org/licenses/>.

use core::future::Future;

use crate::hal::uart::uart_write;
use crate::hal::*;
use kompressor_common::gps::GPSTime;
use log::{debug, info};
use rscore::ublox::GnassTimebase;

use rscore::{
    io::AsyncWriter,
    ublox::{self, UbxPkt},
};

extern "C" {
    static mut gGPSRxBuf: [u8; 1];
    static gpsRxQueue: StreamBufferHandle_t;
}

struct UartAsyncWriter {}

impl AsyncWriter for UartAsyncWriter {
    type Output<'a> = impl Future<Output = rscore::io::Result<()>> + 'a;

    fn write<'a>(&mut self, buf: &'a [u8]) -> Self::Output<'a> {
        async {
            unsafe { uart_write(&huart2, buf).await };
            Ok(())
        }
    }
}

const UBX_CLASS_CFG: u8 = 0x06;
const UBX_CFG_PRT: u8 = 0x00;

const COM_PORT_UART1: u8 = 1;

pub async fn run() {
    unsafe {
        resetGPS();
    }
    debug!("gps reset");

    unsafe { HAL_UART_Receive_IT(&huart2, gGPSRxBuf.as_mut_ptr(), 1) };

    Sleep::new(1000).await;
    send_ubx_cfg().await;

    let mut ubx_builder = ublox::Builder::<128>::new();

    loop {
        let b = read_gps().await;
        if let Some(pkt) = ubx_builder.push(b) {
            //info!("time now: {:?}", unsafe { crate::DATA.assume_init_ref().gps_time } );

            if let Ok(timtp) = ublox::UbxTimTp::try_from(&pkt) {
                let data = unsafe { crate::DATA.assume_init_mut() };
                if let Ok(GnassTimebase::Gps) = timtp.ref_timebase_gnss() {
                    data.next_gps_time =
                        Some(GPSTime::new(timtp.week(), timtp.tow_ms() as u64 / 1000, 0));
                } else {
                    data.next_gps_time = None;
                }
            } else if let Ok(pvt) = ublox::UbxNavPVT::try_from(&pkt) {
                let data = unsafe { crate::DATA.assume_init_mut() };
                data.radio_ctx.position = pvt.position();
                info!("{}", pvt);
            }
        }
    }
}

async fn read_gps() -> u8 {
    use core::{
        pin::Pin,
        task::{Context, Poll},
    };

    //unsafe { uart_read(&huart2, &mut data).await };

    struct ReadQueue {}

    impl Future for ReadQueue {
        type Output = u8;

        fn poll(self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<Self::Output> {
            let mut b = 0u8;
            let num_bytes = unsafe { xStreamBufferReceive(gpsRxQueue, &mut b, 1, 0) };
            if num_bytes > 0 {
                Poll::Ready(b)
            } else {
                cx.waker().wake_by_ref();
                Poll::Pending
            }
        }
    }

    ReadQueue {}.await
}

async fn send_ubx_cfg() {
    let mode: u32 = (1 << 6) | (1 << 7) |   // 8-bit
                    (1 << 11)               // no parity
                    ; // 1 stop bit

    let baudrate: u32 = 9600;
    let inProtoMask: u16 = 1 << 0;
    let outProtoMask: u16 = 1 << 0;
    let flags: u16 = 0;

    let mut payload = [0_u8; 20];
    payload[0] = COM_PORT_UART1; // portID
    payload[1] = 0; // reserved
    payload[2..4].clone_from_slice(&[0_u8, 2]); // txReady
    payload[4..8].clone_from_slice(&mode.to_le_bytes()); //mode
    payload[8..12].clone_from_slice(&baudrate.to_le_bytes());
    payload[12..14].clone_from_slice(&inProtoMask.to_le_bytes());
    payload[14..16].clone_from_slice(&outProtoMask.to_le_bytes());
    payload[16..18].copy_from_slice(&flags.to_be_bytes());

    let pkt = UbxPkt::new(UBX_CLASS_CFG, UBX_CFG_PRT, &payload);

    let mut gps_serial = UartAsyncWriter {};

    let _ = pkt.write_async(&mut gps_serial).await;

    Sleep::new(100).await;

    config_pps().await;

    //let pkt = UbxPkt::new(0x0A, 0x04, &[]);
    //let _ = pkt.write(gps_serial);

    //CurrentTask::delay(Duration::ms(100));

    //let pkt = UbxPkt::new(0x06, 0x01, &[0x01, 0x03, 0x01]);     //UBX-NAV-STATUS
    //let _ = pkt.write(gps_serial);

    //unsafe { osDelay(100); }

    //let pkt = UbxPkt::new(0x06, 0x01, &[0x01, 0x35, 0x05]);     //UBX-NAV-SAT
    //let _ = pkt.write(gps_serial);

    //unsafe { osDelay(100); }

    // send the UBX-TIM-TP every second
    let pkt = UbxPkt::new(0x06, 0x01, &[0x0D, 0x01, 0x01]); //UBX-TIM-TP
    let _ = pkt.write_async(&mut gps_serial).await;

    Sleep::new(100).await;

    //let pkt = UbxPkt::new(0x06, 0x31, &[]);
    //let _ = pkt.write(gps_serial);
    //Sleep::new(100).await;

    let pkt = UbxPkt::new(0x06, 0x01, &[0x01, 0x07, 0x05]); //UBX-NAV-PVT
    let _ = pkt.write_async(&mut gps_serial).await;
}

// fn send_ubx_cfg(gps_serial: &mut impl io::Write) {
//     let mode: u32 = (1 << 6) | (1 << 7) |   // 8-bit
//                     (1 << 11)               // no parity
//                     ; // 1 stop bit

//     let baudrate: u32 = 9600;
//     let inProtoMask: u16 = 1 << 0;
//     let outProtoMask: u16 = 1 << 0;
//     let flags: u16 = 0;

//     let mut payload = [0_u8; 20];
//     payload[0] = COM_PORT_UART1; // portID
//     payload[1] = 0; // reserved
//     payload[2..4].clone_from_slice(&[0_u8, 2]); // txReady
//     payload[4..8].clone_from_slice(&mode.to_le_bytes()); //mode
//     payload[8..12].clone_from_slice(&baudrate.to_le_bytes());
//     payload[12..14].clone_from_slice(&inProtoMask.to_le_bytes());
//     payload[14..16].clone_from_slice(&outProtoMask.to_le_bytes());
//     payload[16..18].copy_from_slice(&flags.to_be_bytes());

//     let pkt = UbxPkt::new(UBX_CLASS_CFG, UBX_CFG_PRT, &payload);
//     let _ = pkt.write(gps_serial);

//     CurrentTask::delay(Duration::ms(100));

//     config_pps(gps_serial);

//     //let pkt = UbxPkt::new(0x0A, 0x04, &[]);
//     //let _ = pkt.write(gps_serial);

//     //CurrentTask::delay(Duration::ms(100));

//     //let pkt = UbxPkt::new(0x06, 0x01, &[0x01, 0x03, 0x01]);     //UBX-NAV-STATUS
//     //let _ = pkt.write(gps_serial);

//     //unsafe { osDelay(100); }

//     //let pkt = UbxPkt::new(0x06, 0x01, &[0x01, 0x35, 0x05]);     //UBX-NAV-SAT
//     //let _ = pkt.write(gps_serial);

//     //unsafe { osDelay(100); }

//     // send the UBX-TIM-TP every second
//     let pkt = UbxPkt::new(0x06, 0x01, &[0x0D, 0x01, 0x01]); //UBX-TIM-TP
//     let _ = pkt.write(gps_serial);

//     unsafe {
//         osDelay(100);
//     }

//     //let pkt = UbxPkt::new(0x06, 0x31, &[]);
//     //let _ = pkt.write(gps_serial);

//     CurrentTask::delay(Duration::ms(100));

//     let pkt = UbxPkt::new(0x06, 0x01, &[0x01, 0x07, 0x05]); //UBX-NAV-PVT
//     let _ = pkt.write(gps_serial);
// }

async fn config_pps() {
    let mut gps_serial = UartAsyncWriter {};

    // Set the PPS configuration
    // let ant_cable_delay_ns: i16 = 50;
    // let rf_group_delay_ns = 0;

    // let mut payload = [0_u8; 32];
    // payload[0] = 0; // TIMEPULSE 0
    // payload[1] = 1; // Version 1
    // payload[4..6].clone_from_slice(&ant_cable_delay_ns.to_le_bytes());

    let payload = [
        0, 1, 0, 0, 50, 0, 0, 0, 64, 66, 15, 0, 64, 66, 15, 0, 0, 0, 0, 0, 160, 134, 1, 0, 0, 0, 0,
        0, 247, 0, 0, 0,
    ];

    let pkt = UbxPkt::new(UBX_CLASS_CFG, 0x31, &payload);
    let _ = pkt.write_async(&mut gps_serial).await;

    Sleep::new(100).await;
}

// fn gps_fn(task: Task) {
//     unsafe {
//         let next_gps_time = &mut STATIC_DATA.assume_init_mut().next_gps_time;

//         resetGPS();

//         HAL_UART_Receive_IT(&huart2, gGPSRxBuf.as_mut_ptr(), 1);
//         CurrentTask::delay(Duration::ms(1000));

//         let mut dbg_serial = UART { handle: &huart1 };
//         let mut gps_serial = UART { handle: &huart2 };

//         //let pkt = UbxPkt::new(UBX_CLASS_CFG, UBX_CFG_PRT, &[COM_PORT_UART1]);
//         //let _ = pkt.send(&mut gps_serial);

//         //osDelay(1000);

//         //let pkt = UbxPkt::new(0x0A, 0x04, &payload);

//         /*
//         let mode: u32 = (1 << 6) | (1 << 7) |   // 8-bit
//                         (1 << 11)               // no parity
//                         ;                       // 1 stop bit

//         let baudrate: u32 = 9600;
//         let inProtoMask: u16 = 1 << 0;
//         let outProtoMask: u16 = 1 << 0;
//         let flags: u16 = 0;

//         let mut payload = [0_u8 ; 20];
//         payload[0] = COM_PORT_UART1;                        // portID
//         payload[1] = 0;                                     // reserved
//         payload[2..4].clone_from_slice(&[0_u8, 2]);     // txReady
//         payload[4..8].clone_from_slice(&mode.to_le_bytes()); //mode
//         payload[8..12].clone_from_slice(&baudrate.to_le_bytes());
//         payload[12..14].clone_from_slice(&inProtoMask.to_le_bytes());
//         payload[14..16].clone_from_slice(&outProtoMask.to_le_bytes());
//         payload[16..18].copy_from_slice(&flags.to_be_bytes());
//         let pkt = UbxPkt::new(UBX_CLASS_CFG, UBX_CFG_PRT, &payload);
//         let _ = pkt.send(&mut gps_serial);
//         */
//         //let _ = dbg_serial.write_all(b"kewl\r\n");

//         send_ubx_cfg(&mut gps_serial);

//         let mut ubx_builder = ublox::Builder::<128>::new();
//         let mut b = 0u8;

//         let mut one_second = TickTimer::new(1000);

//         loop {
//             let num_bytes = xStreamBufferReceive(gpsRxQueue, &mut b, 1, 100);
//             if num_bytes > 0 {
//                 //HAL_UART_Transmit(&huart1, &b, 1 as u16, 1000);
//                 if let Some(pkt) = ubx_builder.push(b) {
//                     let mut formatter = Formatter::new(&mut dbg_serial);
//                     //let _ = uwrite!(formatter, "gps => {}\r\n", pkt);

//                     //#[cfg(feature = "debug")]
//                     //info!("gps => {}", pkt);

//                     if let Ok(timtp) = ublox::UbxTimTp::try_from(&pkt) {
//                         if let Ok(GnassTimebase::Gps) = timtp.ref_timebase_gnss() {
//                             *next_gps_time =
//                                 Some(GPSTime::new(timtp.week(), timtp.tow_ms() as u64 / 1000, 0));
//                         } else {
//                             *next_gps_time = None;
//                         }
//                     } else if let Ok(pvt) = ublox::UbxNavPVT::try_from(&pkt) {
//                         let ctx = &mut STATIC_DATA.assume_init_mut().radio_ctx;
//                         //ctx.time = Time::from_utc_time(&pvt);
//                         ctx.position = pvt.position();

//                         let _ = uwrite!(formatter, "gps => {}\r\n", pvt);
//                     } else if let Ok(tp5) = ublox::UbxCfgTP5v1::try_from(&pkt) {
//                         #[cfg(feature = "debug")]
//                         info!("gps => {}", tp5);
//                     }
//                 }
//             }

//             if one_second.tick() {
//                 // let pkt = UbxPkt::new(0x01, 0x03, &[]);
//                 // let _ = pkt.write(&mut gps_serial);

//                 //let pkt = UbxPkt::new(0x06, 0x31, &[]);
//                 //let _ = pkt.write(&mut gps_serial);
//             }

//             //info!("gps_time: {:?}", next_gps_time);

//             //let pkt = UbxPkt::new(UBX_CLASS_CFG, UBX_CFG_PRT, &[COM_PORT_UART1]);
//             //let _ = pkt.send(&mut gps_serial);
//         }
//     }
// }

#[no_mangle]
extern "C" fn GPS_PPS() {
    let data = unsafe { crate::DATA.assume_init_mut() };
    data.gps_time = data.next_gps_time.take();
    //info!("gps_time: {:?}", data.gps_time);
}
