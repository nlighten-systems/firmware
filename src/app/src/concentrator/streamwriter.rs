// Copyright (C) NLighten Systems, LLC (Paul Soucy) 2022
//
// This file is part of Kompressor Firmware.
//
// Kompressor Firmware is free software: you can redistribute it and/or modify it under the terms of
// the GNU General Public License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any later version.
//
// Kompressor Firmware is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with Foobar. If not, see
// <https://www.gnu.org/licenses/>.

use kompressor_common::concentrator::{stream::SPIFrame, Frame, StreamData};

pub struct StreamAsyncWriter {
    stream_idx: u8,
    buf: [u8; (SPIFrame::PAYLOAD_SIZE - 1)],
    i: usize,
}

impl StreamAsyncWriter {
    pub fn new(stream_idx: u8) -> Self {
        StreamAsyncWriter {
            stream_idx,
            buf: [0_u8; (SPIFrame::PAYLOAD_SIZE - 1)],
            i: 0,
        }
    }

    pub async fn write<'a>(&'a mut self, mut buf: &'a [u8]) {
        loop {
            if buf.is_empty() {
                break;
            }

            if self.i == (SPIFrame::PAYLOAD_SIZE - 1) {
                self.flush().await;
            }

            self.buf[self.i] = buf[0];
            self.i += 1;
            buf = &buf[1..];
        }
    }

    pub async fn flush(&mut self) {
        if self.i > 0 {
            let data = unsafe { crate::DATA.assume_init_mut() };
            crate::spi::host_send(
                data,
                &Frame::StreamData(StreamData {
                    idx: self.stream_idx,
                    data: &self.buf[..self.i],
                }),
            )
            .await;
            self.i = 0;
        }
    }
}
