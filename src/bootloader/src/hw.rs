// Copyright (C) NLighten Systems, LLC (Paul Soucy) 2022
//
// This file is part of Kompressor Firmware.
//
// Kompressor Firmware is free software: you can redistribute it and/or modify it under the terms of
// the GNU General Public License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any later version.
//
// Kompressor Firmware is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with Foobar. If not, see
// <https://www.gnu.org/licenses/>.

use rscore::ed25519::ED25519_PUBLIC_KEY_SIZE;

pub const HAL_MAX_DELAY: u32 = 0xFFFFFFFF;

extern "C" {
    pub fn hw_init();
    pub fn hw_uart_tx(data: *const u8, len: usize, timeout: u32) -> i32;
    pub fn hw_uart_rx(data: *const u8, len: usize, timeout: u32) -> i32;
    pub static app_key: [u8; ED25519_PUBLIC_KEY_SIZE];
}

#[inline]
fn write_uart(data: &[u8], timeout: u32) -> i32 {
    unsafe { hw_uart_tx(data.as_ptr(), data.len(), timeout) }
}

#[inline]
fn read_uart(data: &mut [u8], timeout: u32) -> i32 {
    unsafe { hw_uart_rx(data.as_mut_ptr(), data.len(), timeout) }
}

pub struct UART;

impl core::fmt::Write for UART {
    fn write_str(&mut self, text: &str) -> Result<(), core::fmt::Error> {
        write_uart(text.as_bytes(), HAL_MAX_DELAY);
        Ok(())
    }
}

impl rscore::io::Write for UART {
    fn write(&mut self, buf: &[u8]) -> rscore::io::Result<usize> {
        write_uart(buf, HAL_MAX_DELAY);
        Ok(buf.len())
    }
}

impl rscore::io::Read for UART {
    fn read(&mut self, buf: &mut [u8]) -> rscore::io::Result<usize> {
        match read_uart(buf, 1000) {
            0 => Ok(buf.len()),
            _ => Err(rscore::io::ErrorKind::TimedOut.into()),
        }
    }
}

impl UART {
    pub fn println(&mut self) {
        use core::fmt::Write;
        self.write_str("\r\n");
    }
}

pub const SERIAL: UART = UART {};
