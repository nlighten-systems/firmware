// Copyright (C) NLighten Systems, LLC (Paul Soucy) 2022
//
// This file is part of Kompressor Firmware.
//
// Kompressor Firmware is free software: you can redistribute it and/or modify it under the terms of
// the GNU General Public License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any later version.
//
// Kompressor Firmware is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with Foobar. If not, see
// <https://www.gnu.org/licenses/>.

use core::{
    future::Future,
    pin::Pin,
    sync::atomic::{AtomicBool, Ordering},
    task::{Context, Poll},
};

use log::error;

use crate::hal::*;

static mut UART2_TX: UartTransaction = UartTransaction::new();
static mut UART2_RX: UartTransaction = UartTransaction::new();

pub struct UARTWriter {}

impl rscore::io::Write for UARTWriter {
    fn write(&mut self, _buf: &[u8]) -> rscore::io::Result<usize> {
        todo!()
    }
}

struct UartTransaction {
    complete: AtomicBool,
}

impl UartTransaction {
    const fn new() -> Self {
        UartTransaction {
            complete: AtomicBool::new(false),
        }
    }

    fn reset(&self) {
        self.complete.store(false, Ordering::Release);
    }
}

struct Transaction<'a> {
    complete: &'a AtomicBool,
}

impl<'a> Transaction<'a> {
    fn new(complete: &'a AtomicBool) -> Self {
        Transaction { complete }
    }
}

impl<'a> Future for Transaction<'a> {
    type Output = ();

    fn poll(self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<Self::Output> {
        match self.complete.load(Ordering::Acquire) {
            true => Poll::Ready(()),

            false => {
                cx.waker().wake_by_ref();
                Poll::Pending
            }
        }
    }
}

pub async fn uart_write(handle: *const UART_HandleTypeDef, data: &[u8]) {
    unsafe {
        let op: Transaction;

        if handle == &huart2 {
            UART2_TX.reset();
            op = Transaction::new(&UART2_TX.complete);
        } else {
            panic!();
        }

        HAL_UART_Transmit_IT(handle, data.as_ptr(), data.len() as u16);
        op.await
    }
}

pub async fn uart_read(handle: *const UART_HandleTypeDef, data: &mut [u8]) {
    unsafe {
        let op: Transaction;

        if handle == &huart2 {
            UART2_RX.reset();
            op = Transaction::new(&UART2_RX.complete);
        } else {
            panic!();
        }

        HAL_UART_Receive_IT(handle, data.as_mut_ptr(), data.len() as u16);
        op.await
    }
}

#[no_mangle]
extern "C" fn HAL_UART_RxCpltCallback(handle: *const UART_HandleTypeDef) {
    unsafe {
        if handle == &huart2 {
            {
                unsafe {
                    UART2_RX.complete.store(true, Ordering::Release);
                }
            }
        }
    }
}

#[no_mangle]
extern "C" fn HAL_UART_TxCpltCallback(handle: *const UART_HandleTypeDef) {
    unsafe {
        if handle == &huart2 {
            {
                unsafe {
                    UART2_TX.complete.store(true, Ordering::Release);
                }
            }
        }
    }
}

#[no_mangle]
extern "C" fn HAL_UART_ErrorCallback(_handle: *const UART_HandleTypeDef) {
    error!("UART error");
}
