#include "stdint.h"
#include "main.h"
#include "config_app_image.h"

extern UART_HandleTypeDef huart1;

void __attribute__( ( optimize( "O0" ) ) ) app_start() {

    void (*app_reset_handler)(void);

    //shutdown everything
    HAL_UART_DeInit(&huart1);
    HAL_GPIO_DeInit(BOOTLOAD_GPIO_Port, BOOTLOAD_Pin);
    HAL_RCC_DeInit();
    HAL_DeInit();


    SysTick->CTRL = 0;
    SysTick->LOAD = 0;
    SysTick->VAL = 0;

     SCB->VTOR = APP_IMAGE_START;

    // 1. configure the MSP by reading the value from the base address of the sector 2
    uint32_t msp_value = *(__IO uint32_t *)APP_IMAGE_START;

    __set_MSP(msp_value);

    uint32_t resethandler_address = *(__IO uint32_t *) (APP_IMAGE_START + 4);

    app_reset_handler = (void*) resethandler_address;

    //3. jump to reset handler of the user application
    app_reset_handler();


}